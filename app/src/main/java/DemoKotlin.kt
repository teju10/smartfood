import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import java.lang.Integer.parseInt

/**
 * Created by and-04 on 27/12/17.
 */


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//    setContentView(R.layout.exm)
//        PrintProdct("12", "10")
    }

    fun PrintProdct(args1: String, args2: String) {
        val x = parseInt(args1)
        val y = parseInt(args2)

        if (x != null && y != null) {
            println(x * y)
        } else {
            println("Either 'args1' or 'args2' is not a number")
        }
    }

    fun main(Args:Array<String>){
        class AirForce(val name:String)
        val usa = AirForce("USA")
        val india = AirForce("INDIA")

        println("${usa.name} and ${india.name}")
    }
}