package com.yallaseller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.yallaseller.R;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by and-04 on 5/12/17.
 */

public class SalesFragment extends Fragment implements View.OnClickListener {

    View rv;
    Context mContext;
    Button day_btn, week_btn, month_btn, year_btn;
    private BarChart mChart;
    String[] days, weekdays;
    XAxis xAxis;
    int flag,max_yaxis = 0;
    ArrayList<JSONObject> daylist, monthlist,weeklist;
    ResponseTask rt;
    Utils utils;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constant.LANG));

        rv = inflater.inflate(R.layout.frag_mywallet_sales, container, false);
        mContext = getActivity();
        utils = new Utils(mContext);
        mChart = (BarChart) rv.findViewById(R.id.chart_bar);

        FIND();

      /*  mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        // mChart.setMaxVisibleValueCount(60);
        mChart.getAxisLeft().setAxisMaximum(20);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawBarShadow(false);
        mChart.setDrawGridBackground(false);

        final String[] weekdays = {mContext.getResources().getString(R.string.sun),
                mContext.getResources().getString(R.string.mon),
                mContext.getResources().getString(R.string.tue),
                mContext.getResources().getString(R.string.wed),
                mContext.getResources().getString(R.string.thu),
                mContext.getResources().getString(R.string.fri),
                mContext.getResources().getString(R.string.sat)};

        xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(weekdays));

        mChart.getAxisLeft().setDrawGridLines(false);

        //setYAxis();

        mChart.animateY(2500);

        mChart.getLegend().setEnabled(false);*/

       // GraphChartDays();

        if (utils.isNetConnected()) {
            GetWalletDataTask();
        } else {
            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.check_internet));
        }
        return rv;
    }

    public void FIND() {

        day_btn = (Button) rv.findViewById(R.id.day_btn);
        week_btn = (Button) rv.findViewById(R.id.week_btn);
        month_btn = (Button) rv.findViewById(R.id.month_btn);
        year_btn = (Button) rv.findViewById(R.id.year_btn);

        day_btn.setOnClickListener(this);
        week_btn.setOnClickListener(this);
        month_btn.setOnClickListener(this);
        year_btn.setOnClickListener(this);

        SetBTN1BG(day_btn, week_btn, month_btn, year_btn);
    }

    private void setYAxis() {

        /*ArrayList<Integer> list = new ArrayList<>();

        for (int i = 1; i < 8; i++) {
            list.add(i);
        }

        for (int i = 0; i < list.size(); i++) {
            yVals1.add(new BarEntry(i, list.get(i)));
        }*/

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        if (flag == 0) {
            for (int i = 0; i < daylist.size(); i++) {
                try {
                    yVals1.add(new BarEntry(i,
                            Integer.valueOf(daylist.get(i).getString("day" + String.valueOf(i)))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (flag == 1) {
            for (int i = 0; i < weeklist.size(); i++) {
                try {
                    yVals1.add(new BarEntry(i,
                            Integer.valueOf(daylist.get(i).getString("day" + String.valueOf(i)))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else if (flag == 2) {
            for (int i = 0; i < monthlist.size(); i++) {
                try {
                    yVals1.add(new BarEntry(i, Integer.parseInt(monthlist.get(i).getString("delivery_charge"))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(yVals1, "Data Set");
            set1.setColors(ColorTemplate.LIBERTY_COLORS);
            set1.setDrawValues(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            mChart.setData(data);
            mChart.setFitBars(true);
        }

        mChart.invalidate();
    }

    public void SetBTN1BG(Button btn1, Button btn2, Button btn3, Button btn4) {

        btn1.setBackground(mContext.getResources().getDrawable(R.drawable.edt_orange));
        btn2.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn3.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn4.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));

        btn1.setTextColor(mContext.getResources().getColor(R.color.white));
        btn2.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn3.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn4.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

    }

    public void SetBTN2BG(Button btn1, Button btn2, Button btn3, Button btn4) {

        btn1.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn2.setBackground(mContext.getResources().getDrawable(R.drawable.edt_orange));
        btn3.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn4.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));

        btn1.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn2.setTextColor(mContext.getResources().getColor(R.color.white));
        btn3.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn4.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

    }

    public void SetBTN3BG(Button btn1, Button btn2, Button btn3, Button btn4) {

        btn1.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn2.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn3.setBackground(mContext.getResources().getDrawable(R.drawable.edt_orange));
        btn4.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));

        btn1.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn2.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn3.setTextColor(mContext.getResources().getColor(R.color.white));
        btn4.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

    }

    public void SetBTN4BG(Button btn1, Button btn2, Button btn3, Button btn4) {

        btn1.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn2.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn3.setBackground(mContext.getResources().getDrawable(R.drawable.white_orange));
        btn4.setBackground(mContext.getResources().getDrawable(R.drawable.edt_orange));

        btn1.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn2.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn3.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        btn4.setTextColor(mContext.getResources().getColor(R.color.white));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.day_btn:
                flag = 0;
                SetBTN1BG(day_btn, week_btn, month_btn, year_btn);
                GetWalletDataTask();
                break;

            case R.id.week_btn:
                flag = 1;
                SetBTN2BG(day_btn, week_btn, month_btn, year_btn);
                GetWalletDataTask();
                break;

            case R.id.month_btn:
                flag = 2;
                SetBTN3BG(day_btn, week_btn, month_btn, year_btn);
                GetWalletDataTask();
                break;

            case R.id.year_btn:
                flag = 3;
                SetBTN4BG(day_btn, week_btn, month_btn, year_btn);
                break;
        }
    }

    public void GraphChartDays(int max) {
        days = new String[]{
                "day1", "day2", "day3", "day4", "day5", "day6"};

        mChart.getDescription().setEnabled(false);

        mChart.getAxisLeft().setAxisMaximum(max);
        mChart.setPinchZoom(false);

        mChart.setDrawBarShadow(false);
        mChart.setDrawGridBackground(false);

        xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));

        mChart.getAxisLeft().setDrawGridLines(false);
        setYAxis();
        mChart.animateY(2500);

        mChart.getLegend().setEnabled(false);
    }

    public void GraphChartWeek(int max) {
        weekdays = new String[]{mContext.getResources().getString(R.string.sun),
                mContext.getResources().getString(R.string.mon),
                mContext.getResources().getString(R.string.tue),
                mContext.getResources().getString(R.string.wed),
                mContext.getResources().getString(R.string.thu),
                mContext.getResources().getString(R.string.fri),
                mContext.getResources().getString(R.string.sat)};


        mChart.getDescription().setEnabled(false);

        mChart.getAxisLeft().setAxisMaximum(max);
        mChart.setPinchZoom(false);

        mChart.setDrawBarShadow(false);
        mChart.setDrawGridBackground(false);


        xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(weekdays));

        mChart.getAxisLeft().setDrawGridLines(false);
        setYAxis();
        mChart.animateY(2500);

        mChart.getLegend().setEnabled(false);
    }

    private void GetWalletDataTask() {
        try {

            JSONObject j = new JSONObject();
            j.put(Constant.ACTION, Constant.GETRESTAURANTWALLET);
            j.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));

            utils.startProgress();
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    try {
                        if (result == null) {
                            utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                        } else {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {

                                JSONObject jobj = jo.getJSONObject("post_data");

                                if (flag == 0) {
                                    JSONArray jaar = jobj.getJSONArray("todays_total");
                                    daylist = new ArrayList<JSONObject>();

                                    for (int i = 0; i < jaar.length(); i++) {
                                        daylist.add(jaar.getJSONObject(i));
                                    }
                                    GraphChartDays(max_yaxis);

                                   // GraphChartWeek(max_yaxis);

                                }if (flag == 1) {
                                    weeklist = new ArrayList<>();
                                    JSONArray jaar = jobj.getJSONArray("todays_total");

                                    for (int i = 0; i < jaar.length(); i++) {
                                        weeklist.add(jaar.getJSONObject(i));
                                    }
                                    GraphChartWeek(max_yaxis);

                                   // GraphChartWeek(max_yaxis);

                                } else if (flag == 2) {
                                    monthlist = new ArrayList<>();
                                    JSONArray jaarmonth = jobj.getJSONArray("cur_month");

                                    for (int i = 0; i < jaarmonth.length(); i++) {
                                        monthlist.add(jaarmonth.getJSONObject(i));
                                    }
                                } else if (flag == 3) {

                                }

                                ((TextView) rv.findViewById(R.id.amnt_order)).
                                        setText(jobj.getString("today_total_earning"));

                                ((TextView) rv.findViewById(R.id.total_sales)).
                                        setText(jobj.getString("application_fees"));


                            }
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

}
