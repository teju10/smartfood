package com.yallaseller.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.adapter.RateReviewAdap;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 21/11/17.
 */

public class MyReviewFragment extends Fragment {
    View rv;
    Context mContext;
    ResponseTask rt;
    Utils utils;
    ImageView star1_rat, star2_rat, star3_rat, star4_rat, star5_rat;
    ArrayList<JSONObject> mlist;
    RateReviewAdap adap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constant.LANG));

        rv = inflater.inflate(R.layout.frag_my_review, container, false);
        mContext = getActivity();
        utils = new Utils(mContext);

        FIND();

        if (utils.isNetConnected()) {
            GetRattingReviewTask();
        } else {
            utils.Toast(mContext.getResources().getString(R.string.check_internet), null);
        }

        return rv;
    }

    public void FIND() {
        star1_rat = (ImageView) rv.findViewById(R.id.star1_rat);
        star2_rat = (ImageView) rv.findViewById(R.id.star2_rat);
        star3_rat = (ImageView) rv.findViewById(R.id.star3_rat);
        star4_rat = (ImageView) rv.findViewById(R.id.star4_rat);
        star5_rat = (ImageView) rv.findViewById(R.id.star5_rat);
    }

    public void GetRattingReviewTask() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.GETREVIEWRATE);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                JSONObject jobj = jo.getJSONObject("object");
                                ((TextView) rv.findViewById(R.id.overol_rate)).setText(jobj.getString("rating"));
                                Ratting(Float.valueOf(jobj.getString("rating")));
//                                Ratting(Float.valueOf("1.5"));
                                mlist = new ArrayList<>();
                                JSONArray jara = jobj.getJSONArray("review");
                                for (int i = 0; i < jara.length(); i++) {
                                    mlist.add(jara.getJSONObject(i));
                                }
                                SetAdap();
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }

    }

    public void SetAdap() {
        adap = new RateReviewAdap(mContext, mlist);
        ((ListView) rv.findViewById(R.id.rate_review_lst)).setAdapter(adap);
    }

    public void Ratting(float rate) {
        if (rate >= 0 && rate < 0.5) {
            star1_rat.setImageResource(R.drawable.star_orange_empty);
            star2_rat.setImageResource(R.drawable.star_orange_empty);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);

        } else if (rate >= 0.5 && rate < 1) {
            star1_rat.setImageResource(R.drawable.star_half_orange);
            star2_rat.setImageResource(R.drawable.star_orange_empty);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);

        } else if (rate >= 1 && rate < 1.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange_empty);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 1.5 && rate < 2) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_half_orange);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 2 && rate < 2.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 2.5 && rate < 3) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_half_orange);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 3 && rate < 3.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 3.5 && rate < 4) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_half_orange);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 4 && rate < 4.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 4.5 && rate < 5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange);
            star5_rat.setImageResource(R.drawable.star_half_orange);
        } else if (rate == 5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange);
            star5_rat.setImageResource(R.drawable.star_orange);
        }
    }

}
