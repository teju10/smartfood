package com.yallaseller.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yallaseller.R;
import com.yallaseller.activity.My_RestaurantActivity;
import com.yallaseller.adapter.MyMealsAdapter;
import com.yallaseller.customwidget.CircleImageView;
import com.yallaseller.model.MyMealsChild;
import com.yallaseller.model.MyMealsGroup;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by and-04 on 21/11/17.
 */

public class MyMealsFragment extends Fragment implements View.OnClickListener {
    Context mContext;
    View rv;
    ResponseTask rt;
    List<MyMealsGroup> groupList = new ArrayList<>();
    List<MyMealsChild> childList;
    MyMealsAdapter adapter;
    String dataString = "";
    ExpandableListView my_meals_lst;
    Dialog cat_dialog;
    ArrayList<String> catogrylist;
    ArrayList<String> catogryid;
    int[] to;
    ArrayList<HashMap<String, String>> cards;
    String cid = "", Category = "";
    String[] from;
    Spinner newcat_spinner;
    private Utils utils;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constant.LANG));

        rv = inflater.inflate(R.layout.mymealsfragment, container, false);
        mContext = getActivity();
        utils = new Utils(mContext);
        my_meals_lst = (ExpandableListView) rv.findViewById(R.id.my_meals_lst);
        dataString = this.getArguments().getString(Constant.MYMEALSOBJ, "");

      /*  my_meals_lst.setGroupIndicator(null);
        my_meals_lst.setChildIndicator(null);
        my_meals_lst.setChildDivider(getResources().getDrawable(R.color.grey_500));
        my_meals_lst.setDivider(getResources().getDrawable(R.color.white));
        my_meals_lst.setDividerHeight(10);*/

        rv.findViewById(R.id.add_cat_btn).setOnClickListener(this);
        return rv;
    }

    @Override
    public void onResume() {
        if (utils.isNetConnected()) {
            GetMyMeals();
        } else {
            utils.Toast(mContext.getResources().getString(R.string.check_internet), null);
        }
        super.onResume();
    }

    public void SetAdap() {
        adapter = new MyMealsAdapter(mContext, groupList, childList);
        my_meals_lst.setAdapter(adapter);
    }

    public void AddCatgoryDialog() {
        cat_dialog = new Dialog(mContext, R.style.MyDialog);
        cat_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cat_dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        cat_dialog.setContentView(R.layout.dialog_addnew_catgry);
        cat_dialog.setCancelable(false);
        cat_dialog.show();
        final EditText edt_category, child_item_name, child_sub_item, child_price;
        newcat_spinner = (Spinner) cat_dialog.findViewById(R.id.newcat_spinner);
        edt_category = (EditText) cat_dialog.findViewById(R.id.edt_category);
        child_item_name = (EditText) cat_dialog.findViewById(R.id.child_item_name);
        child_sub_item = (EditText) cat_dialog.findViewById(R.id.child_sub_item);
        child_price = (EditText) cat_dialog.findViewById(R.id.child_price);

        child_item_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        child_sub_item.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        if (utils.getPreference(Constant.isLangSet).equals("en")) {
            newcat_spinner.setBackground(mContext.getResources().getDrawable(R.drawable.spinner_downarrow_white));
        }else{
            newcat_spinner.setBackground(mContext.getResources().getDrawable(R.drawable.spinner_downarrow_white_ar));
        }

        GetCatogoryTask();

        newcat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedText = (TextView) parent.getChildAt(0);
                if (position == 0) {
                    cid = "0";
                } else {
                    cid = catogryid.get(position).toString();
                    Category = catogrylist.get(position).toString();
                }
                if (selectedText != null) {
                    selectedText.setTextColor(Color.WHITE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Category = "";
                cid = "";
            }
        });

        cat_dialog.findViewById(R.id.submit_cat_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cid.equals("0") || Integer.valueOf(cid) <= 0) {
                    Utility.showToast(mContext, getString(R.string.select_cat_msg));

                } else if (child_item_name.getText().toString().equals("")) {
                    Utility.showToast(mContext, getString(R.string.main_item_msg));

                } else if (child_sub_item.getText().toString().equals("")) {
                    Utility.showToast(mContext, getString(R.string.sub_item_msg));

                } else if (child_price.getText().toString().equals("") || Integer.valueOf(child_price.getText().toString()) <= 0) {
                    Utility.showToast(mContext, getString(R.string.price_item_msg));
                } else {
                    SendMyMealsCat(/*edt_category.getText().toString()*/cid, child_item_name.getText().toString(),
                            child_sub_item.getText().toString(), child_price.getText().toString());
                }
            }
        });

        cat_dialog.findViewById(R.id.close_dia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cat_dialog != null && cat_dialog.isShowing()) {
                    cat_dialog.cancel();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_cat_btn:
                AddCatgoryDialog();
                break;
        }
    }

    public void SendMyMealsCat(String _cat, String _subcat, String _subtype, String price) {
        try {
            /* AddMenuItems */
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ADDMYMEALSITEM);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.CATAGORYNAME, _cat);
            jo.put(Constant.CAT_TYPE, _subcat);
            jo.put(Constant.CAT_SUBTYPE, _subtype);
            jo.put(Constant.PRICE, price);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.Toast(mContext.getResources().getString(R.string.cat_send), null);
                                if (cat_dialog.isShowing()) {
                                    cat_dialog.dismiss();
                                }
                                GetMyMeals();
                            } else {
                                utils.Toast(j.getString("msg"), null);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void GetMyMeals() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.GETMYMEALS);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                JSONObject joo = jobj.getJSONObject("object");
                                groupList.clear();
                                // childList.clear();
                                ((TextView) getActivity().findViewById(R.id.toolbar_title)).setText(joo.getString("restaurant_name"));


                                Picasso.with(mContext).load(joo.getString("logo_image")).into((CircleImageView) getActivity().findViewById(R.id.res_img));
                                ((My_RestaurantActivity) mContext).Ratting(Float.parseFloat(joo.getString("avg_rating")));
                                JSONArray menuarra = joo.getJSONArray("menu_list");
                                if (menuarra.length() == 0) {
                                    Utility.showToast(mContext, "no menue list available");
                                } else {
                                    for (int i = 0; i < menuarra.length(); i++) {
                                        JSONObject c = menuarra.getJSONObject(i);
                                        childList = new ArrayList<>();
                                        JSONArray childarra = c.getJSONArray("menu_item");
                                        for (int j = 0; j < childarra.length(); j++) {
                                            JSONObject object = childarra.getJSONObject(j);
                                            childList.add(new MyMealsChild(object.getInt("menu_id"), object.getString("menu_name"),
                                                    object.getString("menu_title"), object.getString("menu_price"),
                                                    object.getString("menu_image"),object.getString("menu_discount_price"),
                                                    object.getString("discount_status"),object.getString("discount_percentage")));
                                        }

                                        groupList.add(new MyMealsGroup(c.getInt("cat_id"), c.getString("cat_name"), childList));
                                    }
                                }
                                SetAdap();
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void GetCatogoryTask() {

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.GETCATORYLIST);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                JSONArray ja = j.getJSONArray("object");
                                catogrylist = new ArrayList<>();
                                catogryid = new ArrayList<>();

                                catogryid.clear();
                                catogrylist.clear();

                                catogryid.add("0");
                                catogrylist.add(getResources().getString(R.string.sel_cat));
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jobj = ja.getJSONObject(i);
                                    String catname = ja.getJSONObject(i).getString("kitchen_name");
                                    String catid = ja.getJSONObject(i).getString("id");
                                    catogryid.add(catid);
                                    catogrylist.add(catname);
                                }
                                ArrayAdapter adapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, catogrylist);
                                newcat_spinner.setAdapter(adapter);

                            } else {

                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

}
