package com.yallaseller.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yallaseller.R;
import com.yallaseller.utils.Utils;

/**
 * Created by vivek on 6/2/2017.
 */

public class AboutFragment extends Fragment {

    private View rootView;
    private Utils utils;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_about, container, false);
        init();
        return rootView;
    }

    private void init() {
        utils = new Utils(getActivity());
    }


}
