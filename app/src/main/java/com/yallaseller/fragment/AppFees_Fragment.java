package com.yallaseller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.adapter.AppFeesHistory;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 5/12/17.
 */

public class AppFees_Fragment extends Fragment {

    View rv;
    ResponseTask rt;
    Utils utils;
    AppFeesHistory adap;
    Context mContext;
    ArrayList<JSONObject> listarray;
    RecyclerView fees_history_list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constant.LANG));

        rv = inflater.inflate(R.layout.frag_mywallet_appfees, container, false);

        mContext = getActivity();

        utils = new Utils(mContext);
        fees_history_list = (RecyclerView) rv.findViewById(R.id.fees_history_list);

        if (utils.isNetConnected()) {
            GetAppFeesTask();
        } else {
            utils.Toast(mContext.getResources().getString(R.string.check_internet), null);
        }

        return rv;
    }

    public void GetAppFeesTask() {
        try {

            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.APPFEES);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            utils.startProgress();

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    try {
                        if (result == null) {
                            utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                        } else {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                JSONObject jobj = j.getJSONObject("object");
                                ((TextView) rv.findViewById(R.id.fees_rate)).setText(jobj.getString("fees_rate"));
                                ((TextView) rv.findViewById(R.id.fees_blnce)).setText(jobj.getString("fees_balance"));
                                listarray = new ArrayList<>();
                                JSONArray ja = jobj.getJSONArray("fees_history");

                                for (int i = 0; i < ja.length(); i++) {
                                    listarray.add(ja.getJSONObject(i));
                                }
                                SetAdap();
                            }
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public void SetAdap() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        fees_history_list.setLayoutManager(mLayoutManager);
        fees_history_list.setItemAnimator(new DefaultItemAnimator());

        adap = new AppFeesHistory(mContext, listarray);
        ((RecyclerView) rv.findViewById(R.id.fees_history_list)).setAdapter(adap);
    }

}
