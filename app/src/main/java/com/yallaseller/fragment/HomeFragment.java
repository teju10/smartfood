package com.yallaseller.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.activity.ConfirmOrder;
import com.yallaseller.activity.DestinationActivity;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.model.SingleDO;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yallaseller.SellerDrawer.fusedLatitude;
import static com.yallaseller.SellerDrawer.fusedLongitude;

/**
 * Created by vivek on 6/2/2017.
 */

public class HomeFragment extends Fragment {
    private static final int LOCATION_PERMISSION_CODE = 300;
    public static boolean isOrdrededOne = false;
    public static boolean isOrdrededTwo = false;
    public static boolean isOrdrededThree = false;
    public static boolean isOrdrededOneGO = false;
    public static boolean isOrdrededTwoGO = false;
    public static boolean isOrdrededThreeGO = false;
    public static String strCurAdd = "";
    public static int counter;
    final Handler handler = new Handler();
    Dialog dialog_rate;
    MapView mMapView;
    Timer timer;
    TimerTask timerTask;
    float currentZoomLevel = 8;
    int rateCount = 0;
    Dialog dia_show_ondemand_req, dia_send_deli_time, dia_reason_cancel;
    Bundle bundle = this.getArguments();
    Context mContext;
    String Time_Str = "";
    ResponseTask rt;
    private GoogleMap googleMap;
    private View rootView;
    private Utils utils;
    private Button btnBusy;
    private String isBusy = "0";
    private String strKeyId, NotiType = "0", Cust_call_no;
    private String strLatitude, strLongitude, Orderid = "",
            Notitype, DriverID = "", NewUser = "", phoneno;
    private LatLng currentLatLong, prevLatLng, fromLatLng;
    private Location curLocation, previousLocation;
    private float currentDegree = 0.0f;
    private TextView textAdd;
    private Button btnOrderNow;
    private int fillStar1, fillStar2, fillStar3, fillStar4, fillStar5;
    private ArrayList<RegistrationInfo> listOfOrder2 = new ArrayList<>();
    private ArrayList<RegistrationInfo> listOfOrder1 = new ArrayList<>();
    private ArrayList<RegistrationInfo> listOfOrder3 = new ArrayList<>();
    private boolean isRate = false;
    private Marker sellerMarker;
    private Marker markerOrder1, markerOrder2, markerOrder3;
    private Marker[] markerList, tempList;
    private LinearLayout layoutHomeGIF;
    private TextView driverOnWay;


    static public void rotateMarker(final Marker marker, final float toRotation) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

//    public void animateMarker(final LatLng toPosition, final Marker carMoveMarker, final float degree) {
//        final Handler handler = new Handler();
//        final long start = SystemClock.uptimeMillis();
//        Projection proj = googleMap.getProjection();
//        Point startPoint = proj.toScreenLocation(carMoveMarker.getPosition());
//        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
//        final long duration = 1000;
//
//        final Interpolator interpolator = new LinearInterpolator();
//        final float startRotation = carMoveMarker.getRotation();
//
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                long elapsed = SystemClock.uptimeMillis() - start;
//                float t = interpolator.getInterpolation((float) elapsed
//                        / duration);
//                double lng = t * toPosition.longitude + (1 - t)
//                        * startLatLng.longitude;
//                double lat = t * toPosition.latitude + (1 - t)
//                        * startLatLng.latitude;
//                carMoveMarker.setPosition(new LatLng(lat, lng));
//
//                float rot = t * degree + (1 - t) * startRotation;
//                carMoveMarker.setRotation(-rot > 180 ? rot / 2 : rot);
//
//                if (t < 1.0) {
//                    // Post again 16ms later.
//                    handler.postDelayed(this, 16);
//                }
//            }
//        });
//    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            permissionLocation();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.please_loc_label))
                .setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void permissionLocation() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == LOCATION_PERMISSION_CODE) {
            //Checking location permissions
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionLocation();
            } else {
            }
        }

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            utils.Toast(mContext.getResources().getString(R.string.permission_grant), null);
            CallCus();
        } else {
            utils.Toast(mContext.getResources().getString(R.string.permission_denied), null);

        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();
        return output;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constant.LANG));

        rootView = inflater.inflate(R.layout.frag_home, container, false);

        mContext = getActivity();
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        init();
        markerList = new Marker[0];
        tempList = new Marker[0];
        markerOrder1 = markerOrder2 = markerOrder3 = null;

        try {
            Orderid = getArguments().getString(Constant.ORDER_ID);
            Notitype = getArguments().getString(Constant.NOTITYPE);

            if (Notitype.equals("0")) {
                NewUser = getArguments().getString(Constant.NEWUSER);
            } else if (Notitype.equals("5")) {
                DriverID = getArguments().getString(Constant.DRIVER_ID);
            }
            if (Notitype.equals("0")) {
                ShowOnDemandReqDia(mContext, Orderid);
            } else if (Notitype.equals("5")) {
                GetDriverDetailTask(Orderid, DriverID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        statusCheck();

       /* try {
            NotiType = utils.getPreference(Constant.NOTITYPE);
            if (utils.getPreference(Constant.NOTITYPE).equals("0")) {
                ShowOnDemandReqDia(mContext);
            } else if (utils.getPreference(Constant.NOTITYPE).equals("5")) {
                GetDriverDetailTask(utils.getPreference(Constant.ORDER_ID), utils.getPreference(Constant.DRIVER_ID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/


       /* if(!utils.isNetConnected()){
             utils.Toast(getActivity().getString(R.string.strConnection), null);
        }else{
            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
                mMapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap mMap) {
                        googleMap = mMap;
                        mMap.getUiSettings().setZoomControlsEnabled(true);
                        mMap.getUiSettings().setZoomGesturesEnabled(true);
                        mMap.getUiSettings().setCompassEnabled(true);
                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
//                        googleMap.getUiSettings().setCompassEnabled(true);
//                        googleMap.getUiSettings().setTiltGesturesEnabled(true);
//                        googleMap.getUiSettings().setRotateGesturesEnabled(true);
                        try {
                            // Customise the styling of the base map using a JSON object defined
                            // in a raw resource file.
                            boolean success = googleMap.setMapStyle(
                                    MapStyleOptions.loadRawResourceStyle(
                                            getActivity(), R.raw.style_json));
                        } catch (Exception e) {
                        }
                        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                            @Override
                            public void onCameraIdle() {
                                currentZoomLevel = googleMap.getCameraPosition().zoom;
                                if (currentZoomLevel < 5) {
                                    currentZoomLevel = 18;
                                }
                            }
                        });

                        if (currentZoomLevel == -1) {
                            currentZoomLevel = 18;
                        }
                        startTimer();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        btnOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ConfirmOrder.class));
            }
        });

        if (!utils.isNetConnected()) {
            utils.Toast(getResources().getString(R.string.strConnection), null);
        } else {
            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
                mMapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap mMap) {
                        googleMap = mMap;
                        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                        try {
                            boolean success = googleMap.setMapStyle(
                                    MapStyleOptions.loadRawResourceStyle(
                                            getActivity(), R.raw.style_json));
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(fromLatLng)
                                    .zoom(18).bearing(0).tilt(30).build();
                        } catch (Exception e) {
                        }
                        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                            @Override
                            public void onCameraIdle() {
                                currentZoomLevel = googleMap.getCameraPosition().zoom;
                                if (currentZoomLevel < 5) {
                                    currentZoomLevel = 12;
                                }
                            }
                        });

                        if (currentZoomLevel == -1) {
                            currentZoomLevel = 12;
                        }
                        startTimer();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (isOrdrededThree) {
            btnOrderNow.setVisibility(View.GONE);
        } else {
            if (isOrdrededOne) {
                btnOrderNow.setText(getResources().getString(R.string.order_extra_driver_now));
            }
            btnOrderNow.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    private void showSellerLocation() {
        Bitmap image = null;
        Drawable myDrawable = mContext.getResources().getDrawable(R.drawable.ic_marker);
        image = ((BitmapDrawable) myDrawable).getBitmap();

        try {
            Double lat, longi;

            try {
                if (!utils.getPreference(Constant.PREF_SAVED_LAT).equals("")) {
                    lat = Double.parseDouble(utils.getPreference(Constant.PREF_SAVED_LAT));
                    longi = Double.parseDouble(utils.getPreference(Constant.PREF_SAVED_LONG));
                } else {
                    lat = fusedLatitude;
                    longi = fusedLongitude;
                }
            } catch (Exception e) {
                lat = fusedLatitude;
                longi = fusedLongitude;
            }

            try {
                strCurAdd = getCompleteAddressString(lat, longi);
                if (!strCurAdd.equals("")) {
                    textAdd.setText(strCurAdd);
                }
            } catch (Exception e) {

            }

            if (sellerMarker == null) {
                fromLatLng = new LatLng(lat, longi);

                CameraPosition cameraPosition = new CameraPosition.Builder().target(fromLatLng)
                        .zoom(18).bearing(0).tilt(30).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                sellerMarker = googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, longi))
                        .icon(BitmapDescriptorFactory.fromBitmap(image))
                        .title(strCurAdd)
                        .anchor(0.5f, 0.5f).
                                rotation((float) Math.toDegrees(currentDegree)).
                                flat(true));
            } else {
                sellerMarker.setIcon(BitmapDescriptorFactory.fromBitmap(image));
                sellerMarker.setPosition(new LatLng(lat, longi));
//                rotateMarker(sellerMarker, currentDegree);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.setTitle(strCurAdd);
                return true;
            }
        });*/
    }

    private void callCancelOrder(String orderId) {
        utils.startProgress();
        ApiUtils.getAPIService().requestCancelOrder(orderId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {

                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void SendRattingTej(String OrderId, int rate, String DID) {
        //http://infograins.com/INFO01/yalla/restaurant_api.php?action=
        // RestaurantRatingToDriver&order_id=19666&user_id=11&driver_id=59&rate=4.5
        try {
            JSONObject j = new JSONObject();
            j.put(Constant.ACTION, "RestaurantRatingToDriver");
            j.put(Constant.ORDER_ID, OrderId);
            j.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            j.put(Constant.DRIVER_ID, DID);
            j.put("rate", rate);
            utils.startProgress();
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {

                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                utils.Toast(mContext.getResources().getString(R.string.rating_submited), null);
                                if (dialog_rate.isShowing()) {
                                    dialog_rate.dismiss();
                                }
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    private void callRate(String orderId) {
        utils.startProgress();
        ApiUtils.getAPIService().requestRating(orderId, String.valueOf(rateCount)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        if (dialog_rate.isShowing()) {
                            dialog_rate.dismiss();
                        }
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                //utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void callCurrentWork() {
        Double lat, longi;
        try {
            if (!utils.getPreference(Constant.PREF_SAVED_LAT).equals("")) {
                longi = Double.parseDouble(utils.getPreference(Constant.PREF_SAVED_LONG));
                lat = Double.parseDouble(utils.getPreference(Constant.PREF_SAVED_LAT));
            } else {
                lat = fusedLatitude;
                longi = fusedLongitude;
            }
        } catch (Exception e) {
            lat = fusedLatitude;
            longi = fusedLongitude;
        }

        try {
            if (strCurAdd.equals("")) {
                strCurAdd = getCompleteAddressString(lat, longi);
                textAdd.setText(strCurAdd);
            }
        } catch (Exception e) {

        }

        strKeyId = utils.getPreference(Constant.PREF_ID);
        String curLat = String.valueOf(lat);
        String curLong = String.valueOf(longi);
        final String curDegree = String.valueOf(currentDegree);
        final String country = utils.getPreference(Constant.PREF_COUNTRY);
//        new MyFirebaseInstanceIDService().onTokenRefresh();
        String timeZone = utils.timeZoneId();
//        String token = utils.getPreference(Constant.FCMID);

        showSellerLocation();

        ApiUtils.getAPIService().requestLocationAPI(strKeyId, country, curLat, curLong, curDegree, Utility.getSharedPreferences(mContext, Constant.FCMID), "android", timeZone).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        HomeFragment.isOrdrededOne = false;
                        HomeFragment.isOrdrededTwo = false;
                        HomeFragment.isOrdrededThree = false;
                        markerOrder1 = markerOrder2 = markerOrder3 = null;

                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        tempList = new Marker[listOfPostData.size()];

                        if (tempList.length > markerList.length) {
                            googleMap.clear();
                            sellerMarker = null;
                            markerList = new Marker[listOfPostData.size()];
                        }

                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo registrationInfo = listOfPostData.get(i);
                            String driverId = registrationInfo.getId();
                            String driverLat = registrationInfo.getStrLat();
                            String driverLong = registrationInfo.getStrLongi();
                            String isBusy = registrationInfo.getIsBusy();
                            String driverDegree = registrationInfo.getStrDegree();

                            Bitmap image;
                            if (isBusy.equals("0")) {
                                image = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_red_car);
                            } else {
                                image = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_green_car);
                            }

                            try {
                                LatLng DriverlatLng = new LatLng(Float.parseFloat(driverLat), Float.parseFloat(driverLong));
                                if (markerList[i] == null) {
                                    markerList[i] = googleMap.addMarker(new MarkerOptions().position(DriverlatLng).icon(BitmapDescriptorFactory.fromBitmap(image)).anchor(0.5f, 0.5f).rotation(Float.parseFloat(driverDegree)).flat(true));
                                } else {
                                    markerList[i].setIcon(BitmapDescriptorFactory.fromBitmap(image));
                                    markerList[i].setPosition(DriverlatLng);
                                    //  animateMarker(DriverlatLng, markerList[i], Float.parseFloat(driverDegree));
                                    rotateMarker(markerList[i], Float.parseFloat(driverDegree));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        showSellerLocation();
                    } else if (success.equals("2")) {
                        counter++;
                        tempList = new Marker[0];
                        markerList = new Marker[0];
                        listOfOrder1 = response.body().getPost_order1();
                        listOfOrder2 = response.body().getPost_order2();
                        listOfOrder3 = response.body().getPost_order3();

                        String checkOrder1 = listOfOrder1.get(0).getStrKeyOrder1();
                        String checkOrder2 = listOfOrder2.get(0).getStrKeyOrder2();
                        String checkOrder3 = listOfOrder3.get(0).getStrKeyOrder3();

                        if (HomeFragment.isOrdrededOneGO || HomeFragment.isOrdrededTwoGO || HomeFragment.isOrdrededThreeGO) {
                            layoutHomeGIF.setVisibility(View.VISIBLE);
                            if (counter == 60) {
                                HomeFragment.isOrdrededOneGO = false;
                                HomeFragment.isOrdrededTwoGO = false;
                                HomeFragment.isOrdrededThreeGO = false;
                                layoutHomeGIF.setVisibility(View.GONE);
                            }
                        } else {
                            layoutHomeGIF.setVisibility(View.GONE);
                        }

                        if (checkOrder1.equals("0") && checkOrder2.equals("0") && checkOrder3.equals("0")) {
                            markerOrder1 = null;
                            markerOrder2 = null;
                            markerOrder3 = null;
                            sellerMarker = null;
                            googleMap.clear();
                        } else {

                            if (markerOrder1 == null && checkOrder1.equals("1")) {
                                sellerMarker = null;
                                googleMap.clear();
                            }
                        }

                        try {
                            if (checkOrder1.equals("1")) {
                                HomeFragment.isOrdrededOne = true;
                                HomeFragment.isOrdrededOneGO = false;
                                Bitmap image = null;
                                if (listOfOrder1.get(0).getIsBusy().equals("0") || listOfOrder1.get(0).getIsBusy().equals("-1")) {
                                    image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_red_car);
                                } else {
                                    image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_green_car);
                                }


                                try {
                                    currentLatLong = new LatLng(Double.valueOf(listOfOrder1.get(0).getStrLat()), Double.valueOf(listOfOrder1.get(0).getStrLongi()));
                                    if (markerOrder1 == null) {
                                        Matrix matrix = new Matrix();
                                        image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(),
                                                matrix, true);
                                        markerOrder1 = googleMap.addMarker(new MarkerOptions().position(currentLatLong).icon(BitmapDescriptorFactory.fromBitmap(image)).anchor(0.5f, 0.5f).rotation(Float.parseFloat(listOfOrder1.get(0).getStrDegree())).flat(true));
                                        markerOrder1.setSnippet("1");
                                    } else {
                                        markerOrder1.setIcon(BitmapDescriptorFactory.fromBitmap(image));
                                        markerOrder1.setPosition(currentLatLong);
                                        rotateMarker(markerOrder1, Float.parseFloat(listOfOrder1.get(0).getStrDegree()));
//                                        animateMarker(currentLatLong, markerOrder1, Float.parseFloat(listOfOrder1.get(0).getStrDegree()));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (markerOrder1 != null)
                                    markerOrder1.remove();
                                HomeFragment.isOrdrededOne = false;
                            }
                        } catch (Exception e) {

                        }

                        try {
                            if (checkOrder2.equals("1")) {
                                HomeFragment.isOrdrededTwo = true;
                                HomeFragment.isOrdrededTwoGO = false;
                                Bitmap image = null;

                                if (listOfOrder2.get(0).getIsBusy().equals("0") || listOfOrder2.get(0).getIsBusy().equals("-1")) {
                                    image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_red_car);
                                } else {
                                    image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_green_car);
                                }


                                try {
                                    currentLatLong = new LatLng(Double.valueOf(listOfOrder2.get(0).getStrLat()), Double.valueOf(listOfOrder2.get(0).getStrLongi()));
                                    if (markerOrder2 == null) {
                                        Matrix matrix = new Matrix();
                                        image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(),
                                                matrix, true);
                                        markerOrder2 = googleMap.addMarker(new MarkerOptions().position(currentLatLong).icon(BitmapDescriptorFactory.fromBitmap(image)).anchor(0.5f, 0.5f).rotation(Float.parseFloat(listOfOrder2.get(0).getStrDegree())).flat(true));
                                        markerOrder2.setSnippet("2");
                                    } else {
                                        markerOrder2.setIcon(BitmapDescriptorFactory.fromBitmap(image));
//                                        animateMarker(currentLatLong, markerOrder2, Float.parseFloat(listOfOrder2.get(0).getStrDegree()));
                                        markerOrder2.setPosition(currentLatLong);
                                        rotateMarker(markerOrder2, Float.parseFloat(listOfOrder2.get(0).getStrDegree()));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (markerOrder2 != null)
                                    markerOrder2.remove();
                                HomeFragment.isOrdrededTwo = false;
                            }
                        } catch (Exception e) {

                        }

                        try {
                            if (checkOrder3.equals("1")) {
                                HomeFragment.isOrdrededThree = true;
                                HomeFragment.isOrdrededThreeGO = false;
                                Bitmap image = null;

                                if (listOfOrder3.get(0).getIsBusy().equals("0") || listOfOrder3.get(0).getIsBusy().equals("-1")) {
                                    image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_red_car);
                                } else {
                                    image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_green_car);
                                }

                                try {
                                    currentLatLong = new LatLng(Double.valueOf(listOfOrder3.get(0).getStrLat()), Double.valueOf(listOfOrder3.get(0).getStrLongi()));
                                    if (markerOrder3 == null) {
                                        Matrix matrix = new Matrix();
                                        image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(),
                                                matrix, true);
                                        markerOrder3 = googleMap.addMarker(new MarkerOptions().position(currentLatLong).icon(BitmapDescriptorFactory.fromBitmap(image)).anchor(0.5f, 0.5f).rotation(Float.parseFloat(listOfOrder3.get(0).getStrDegree())).flat(true));
                                        markerOrder3.setSnippet("3");
                                    } else {
                                        markerOrder3.setIcon(BitmapDescriptorFactory.fromBitmap(image));
                                        //animateMarker(currentLatLong, markerOrder3, Float.parseFloat(listOfOrder3.get(0).getStrDegree()));
                                        markerOrder3.setPosition(currentLatLong);
                                        rotateMarker(markerOrder3, Float.parseFloat(listOfOrder3.get(0).getStrDegree()));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                if (markerOrder3 != null)
                                    markerOrder3.remove();
                                HomeFragment.isOrdrededThree = false;
                            }
                        } catch (Exception e) {

                        }
                        showSellerLocation();

                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                try {
                                    if (marker.getSnippet().equals("1")) {
                                        driverDetail(listOfOrder1, 1);
                                    } else if (marker.getSnippet().equals("2")) {
                                        driverDetail(listOfOrder2, 2);
                                    } else if (marker.getSnippet().equals("3")) {
                                        driverDetail(listOfOrder3, 3);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }
                        });
                    } else if (success.equals("3")) {
                        stopTimerTask();
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        if (isRate == false) {
                            isRate = true;
//                            rateDriver(registrationInfo.getOrderId(), registrationInfo.getPhone(), registrationInfo.getName(), registrationInfo.getImageLink(), registrationInfo.getStrCarModel());
                        }
                    }
                    /**
                     * To check three orders are on/off
                     */
                    if (isOrdrededThree) {
                        btnOrderNow.setVisibility(View.GONE);
                    } else {
                        if (isOrdrededOne) {
                            btnOrderNow.setText(getResources().getString(R.string.order_extra_driver_now));
                        } else {
                            btnOrderNow.setText(getResources().getString(R.string.order_driver_now));
                        }
                        btnOrderNow.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                // utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    public void rateDriver(final String orderId, String phone, String driverName, String driverImage, String carModel) {

        try {
            dialog_rate = new Dialog(getActivity());
            dialog_rate.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_rate.setContentView(R.layout.dialog_rating);
            dialog_rate.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog_rate.setCancelable(true);
            dialog_rate.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog_rate.show();

            TextView textDriverName = (TextView) dialog_rate.findViewById(R.id.rating_driver_name);
            TextView textDriverCar = (TextView) dialog_rate.findViewById(R.id.rating_car_model);
            TextView textDriverPhone = (TextView) dialog_rate.findViewById(R.id.rating_driver_num);
            CircleImageView driImg = (CircleImageView) dialog_rate.findViewById(R.id.rating_img);
            Button btnConfirm = (Button) dialog_rate.findViewById(R.id.rating_btn_confirm);
            Button rating_btn_reject = (Button) dialog_rate.findViewById(R.id.rating_btn_reject);

            final ImageView star1 = (ImageView) dialog_rate.findViewById(R.id.rating_star1);
            final ImageView star2 = (ImageView) dialog_rate.findViewById(R.id.rating_star2);
            final ImageView star3 = (ImageView) dialog_rate.findViewById(R.id.rating_star3);
            final ImageView star4 = (ImageView) dialog_rate.findViewById(R.id.rating_star4);
            final ImageView star5 = (ImageView) dialog_rate.findViewById(R.id.rating_star5);

            fillStar1 = fillStar2 = fillStar3 = fillStar4 = fillStar5 = 0;
            final Drawable drawableEmpty = getActivity().getResources().getDrawable(R.drawable.star_empty);
            final Drawable drawableStar = getActivity().getResources().getDrawable(R.drawable.star);

            Picasso.with(getActivity()).load(driverImage).into(driImg);

            textDriverCar.setText(carModel);
            textDriverPhone.setText(phone);
            textDriverName.setText(driverName);

            rating_btn_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog_rate.isShowing()) {
                        dialog_rate.dismiss();
                    }
                }
            });

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startWorkTimer();
                    if (fillStar1 == 1) {
                        rateCount = 1;
                    } else if (fillStar2 == 1) {
                        rateCount = 2;
                    } else if (fillStar3 == 1) {
                        rateCount = 3;
                    } else if (fillStar4 == 1) {
                        rateCount = 4;
                    } else if (fillStar5 == 1) {
                        rateCount = 5;
                    }

                    if (NotiType.equals("5")) {
                        if (rateCount != 0) {
                            SendRattingTej(utils.getPreference(Constant.ORDER_ID), rateCount, utils.getPreference(Constant.DRIVER_ID));
                        } else {
                            utils.Toast(mContext.getResources().getString(R.string.fill_rate), null);
                        }
                    } else if (NotiType.equals("0")) {
                        callRate(orderId);
                    }

                }
            });

            star1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fillStar1 == 0) {
                        fillStar1 = 1;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableEmpty);
                        star3.setImageDrawable(drawableEmpty);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    } else {
                        fillStar1 = 0;
                        star1.setImageDrawable(drawableEmpty);
                        star2.setImageDrawable(drawableEmpty);
                        star3.setImageDrawable(drawableEmpty);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    }
                }
            });

            star2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fillStar2 == 0) {
                        fillStar2 = 1;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableEmpty);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    } else {
                        fillStar2 = 0;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableEmpty);
                        star3.setImageDrawable(drawableEmpty);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    }
                }
            });

            star3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fillStar3 == 0) {
                        fillStar3 = 1;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableStar);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    } else {
                        fillStar3 = 0;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableEmpty);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    }
                }
            });

            star4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fillStar4 == 0) {
                        fillStar4 = 1;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableStar);
                        star4.setImageDrawable(drawableStar);
                        star5.setImageDrawable(drawableEmpty);
                    } else {
                        fillStar4 = 0;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableStar);
                        star4.setImageDrawable(drawableEmpty);
                        star5.setImageDrawable(drawableEmpty);
                    }
                }
            });

            star5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fillStar5 == 0) {
                        fillStar5 = 1;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableStar);
                        star4.setImageDrawable(drawableStar);
                        star5.setImageDrawable(drawableStar);
                    } else {
                        fillStar5 = 0;
                        star1.setImageDrawable(drawableStar);
                        star2.setImageDrawable(drawableStar);
                        star3.setImageDrawable(drawableStar);
                        star4.setImageDrawable(drawableStar);
                        star5.setImageDrawable(drawableEmpty);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void driverDetail(ArrayList<RegistrationInfo> listOfReg, int driverId) {
        {
            try {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_driver_detail);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(true);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();

                CircleImageView circleImageView = (CircleImageView) dialog.findViewById(R.id.dialog_driver_img);
                TextView textNumber = (TextView) dialog.findViewById(R.id.dialog_driver_num);
                TextView textName = (TextView) dialog.findViewById(R.id.dialog_driverName);
                TextView textPhone = (TextView) dialog.findViewById(R.id.dialog_driverPhone);
                TextView textCarModel = (TextView) dialog.findViewById(R.id.dialog_driverCarModel);
                TextView textArrival = (TextView) dialog.findViewById(R.id.dialog_driverArrival);
                TextView textDistance = (TextView) dialog.findViewById(R.id.dialog_driverDistance);
                TextView textOK = (TextView) dialog.findViewById(R.id.dialog_driverOK);
                LinearLayout layoutCancel = (LinearLayout) dialog.findViewById(R.id.layout_request_cancel);

                final RegistrationInfo listDo = listOfReg.get(0);
                Picasso.with(getActivity()).load(listDo.getImageLink()).into(circleImageView);
                textName.setText(listDo.getName());
                textPhone.setText(listDo.getPhone());
                textCarModel.setText(listDo.getStrCarModel());
                textArrival.setText(listDo.getStrDuration());
                textDistance.setText(listDo.getStrDistance());
                textOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                layoutCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                        callCancelOrder(listDo.getOrderId());
                    }
                });

                if (driverId == 1) {
                    textNumber.setText("Driver 1 is on the way");
                } else if (driverId == 2) {
                    textNumber.setText("Driver 2 is on the way");
                } else if (driverId == 3) {
                    textNumber.setText("Driver 3 is on the way");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void updateWork() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            fusedLatitude = ((SellerDrawer) getActivity()).getFusedLatitude();
                            fusedLongitude = ((SellerDrawer) getActivity()).getFusedLongitude();
                            if (fusedLatitude == 0 && fusedLongitude == 0) {
                                strLatitude = utils.getPreference(Constant.PREF_CURRENT_LAT);
                                strLongitude = utils.getPreference(Constant.PREF_CURRENT_LONG);
                                if (!strLatitude.equals("")) {
                                    fusedLatitude = Double.parseDouble(strLatitude);
                                    fusedLongitude = Double.parseDouble(strLongitude);
                                }
                            } else {
                                if (fusedLatitude != 0) {
                                    callCurrentWork();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    private void init() {
        utils = new Utils(getActivity());
        textAdd = (TextView) rootView.findViewById(R.id.map_cur_tv_add);
        btnOrderNow = (Button) rootView.findViewById(R.id.btn_order_now);
        layoutHomeGIF = (LinearLayout) rootView.findViewById(R.id.home_gif);
        driverOnWay = (TextView) rootView.findViewById(R.id.dialog_driver_num);
        mMapView.onResume();

        if (utils.getPreference(Constant.PREF_ID).equals("")) {
            getActivity().finish();
        }

        textAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), DestinationActivity.class));
            }
        });
    }

    public void startTimer() {
        try {
            timer = new Timer();
            initializeTimerTask();
            timer.schedule(timerTask, 1000, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startWorkTimer() {
        try {
            timer = new Timer();
            updateWork();
            timer.schedule(timerTask, 1000, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopTimerTask() {
        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            fusedLatitude = SellerDrawer.getFusedLatitude();
                            fusedLongitude = SellerDrawer.getFusedLongitude();
                            if (fusedLatitude == 0 && fusedLongitude == 0) {
                                strLatitude = utils.getPreference(Constant.PREF_CURRENT_LAT);
                                strLongitude = utils.getPreference(Constant.PREF_CURRENT_LONG);
                                if (!strLatitude.equals("")) {
                                    fusedLatitude = Double.parseDouble(strLatitude);
                                    fusedLongitude = Double.parseDouble(strLongitude);
                                }
                            } else {
                                if (fusedLatitude != 0) {
                                    stopTimerTask();
                                    currentLatLong = new LatLng(fusedLatitude, fusedLongitude);
                                    if (prevLatLng == null) {
                                        prevLatLng = currentLatLong;
                                    }

                                    googleMap.clear();
                                    // showSellerLocation();
                                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLong));
                                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoomLevel));
                                    startWorkTimer();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public String getCompleteAddressString(double lati, double longi) {
        Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
        String value = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(lati, longi, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                strReturnedAddress.append(returnedAddress.getAddressLine(0));
                value = strReturnedAddress.toString();

            } else {
               /* if (!mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    Utility.AlertNoGPS(mContext);
                }
                mGPS = new GPSTracker(mContext);
                lati = String.valueOf(mGPS.getLatitude());
                longi = String.valueOf(mGPS.getLongitude());*/
                Utility.showToast(mContext, "No address return");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private Bitmap addBorder(Bitmap bmp, int borderSize) {
        Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());
        Canvas canvas = new Canvas(bmpWithBorder);
        canvas.drawColor(getResources().getColor(R.color.colorPrimaryDark));
        canvas.drawBitmap(bmp, borderSize, borderSize, null);
        return bmpWithBorder;
    }

    public void DialogCancelReq(final String oid) {

        dia_reason_cancel = new Dialog(mContext, R.style.MyDialog);
        dia_reason_cancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_reason_cancel.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_reason_cancel.setContentView(R.layout.dialog_cancel_request_reason);
        dia_reason_cancel.setCancelable(false);
        dia_reason_cancel.show();

        final EditText edt_txt = ((EditText) dia_reason_cancel.findViewById(R.id.edt_txt));

        dia_reason_cancel.findViewById(R.id.btn_sbmt_cancelreq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edt_txt.getText().toString().equals("")) {
                    AcceptReject_OrderTask(oid, "1", "1", edt_txt.getText().toString());
                } else {
                    utils.Toast(mContext.getResources().getString(R.string.fill_reason), null);
                }
            }
        });
        dia_reason_cancel.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dia_reason_cancel.dismiss();
            }
        });
    }

    public void ShowOnDemandReqDia(final Context con, String OID) {

        dia_show_ondemand_req = new Dialog(con, R.style.MyDialog);
        dia_show_ondemand_req.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_show_ondemand_req.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_show_ondemand_req.setContentView(R.layout.dialog_get_order_food);
        dia_show_ondemand_req.setCancelable(false);
        dia_show_ondemand_req.show();

        final TextView alert, orderid, time, c_name, order, price, c_phone, order_deli_time;
        final LinearLayout accept_anyway_one, acceptany_two, accpt_reject_ll;
        final ImageView cus_img;

        alert = (TextView) dia_show_ondemand_req.findViewById(R.id.alert);
        orderid = (TextView) dia_show_ondemand_req.findViewById(R.id.orderid);
        time = (TextView) dia_show_ondemand_req.findViewById(R.id.time);
        c_name = (TextView) dia_show_ondemand_req.findViewById(R.id.c_name);
        order = (TextView) dia_show_ondemand_req.findViewById(R.id.order);
        order_deli_time = (TextView) dia_show_ondemand_req.findViewById(R.id.order_deli_time);
        price = (TextView) dia_show_ondemand_req.findViewById(R.id.price);
        cus_img = (ImageView) dia_show_ondemand_req.findViewById(R.id.cus_img);
        c_phone = (TextView) dia_show_ondemand_req.findViewById(R.id.c_phone);

        accept_anyway_one = (LinearLayout) dia_show_ondemand_req.findViewById(R.id.accept_anyway_one);
        acceptany_two = (LinearLayout) dia_show_ondemand_req.findViewById(R.id.acceptany_two);
        accpt_reject_ll = (LinearLayout) dia_show_ondemand_req.findViewById(R.id.accpt_reject_ll);

        dia_show_ondemand_req.findViewById(R.id.accept_anyway_one).setVisibility(View.VISIBLE);
        dia_show_ondemand_req.findViewById(R.id.acceptany_two).setVisibility(View.GONE);

        GetCustomerTask(OID, orderid, time, c_name, order,
                price, cus_img, c_phone, order_deli_time);

        if (NewUser.equals("1")) {
            alert.setText(con.getResources().getString(R.string.accept_call_warn));

            dia_show_ondemand_req.findViewById(R.id.acceptany_one).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.setText(con.getResources().getString(R.string.accept_call_warn2));
                    dia_show_ondemand_req.findViewById(R.id.cus_det_ll).setVisibility(View.GONE);
                    dia_show_ondemand_req.findViewById(R.id.accept_anyway_one).setVisibility(View.GONE);
                    dia_show_ondemand_req.findViewById(R.id.acceptany_two).setVisibility(View.VISIBLE);
                    alert.setText(con.getResources().getString(R.string.accept_call_warn2));
                }
            });

            dia_show_ondemand_req.findViewById(R.id.accept_anyway_two).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.setText(con.getResources().getString(R.string.accept_call_warn2));
                    AcceptReject_OrderTask(Orderid, "0", "0", "");
                }
            });

            dia_show_ondemand_req.findViewById(R.id.accept).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.setText(con.getResources().getString(R.string.accept_call_warn2));
                    AcceptReject_OrderTask(Orderid, "0", "0", "");
                }
            });

            dia_show_ondemand_req.findViewById(R.id.reject).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogCancelReq(Orderid);
                }
            });

            dia_show_ondemand_req.findViewById(R.id.call_one).setOnClickListener(new MyDialogClick(accept_anyway_one,
                    acceptany_two, accpt_reject_ll));

            dia_show_ondemand_req.findViewById(R.id.call_two).setOnClickListener(new MyDialogClick(accept_anyway_one,
                    acceptany_two, accpt_reject_ll));
        } else if (NewUser.equals("0")) {

            dia_show_ondemand_req.findViewById(R.id.accept_anyway_one).setVisibility(View.GONE);
            dia_show_ondemand_req.findViewById(R.id.acceptany_two).setVisibility(View.GONE);
            dia_show_ondemand_req.findViewById(R.id.accpt_reject_ll).setVisibility(View.VISIBLE);

            alert.setText(con.getResources().getString(R.string.call_confirm_msg));

            dia_show_ondemand_req.findViewById(R.id.accept).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AcceptReject_OrderTask(Orderid, "0", "0", "");
                }
            });

            dia_show_ondemand_req.findViewById(R.id.reject).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogCancelReq(Orderid);
                }
            });
        }
    }

    public void GetCustomerTask(String oid, final TextView oid_tv, final TextView time,
                                final TextView cname, final TextView order, final TextView price,
                                final ImageView imv, final TextView phone, final TextView deli_date) {
        // http://infograins.com/INFO01/yalla/restaurant_api.php?action=push_order&user_id=11
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.GETCUSTOMERDETAIL);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ORDER_ID, oid);

            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                JSONObject j = jobj.getJSONObject("object");

                                oid_tv.setText(j.getString("order_id"));
                                time.setText(j.getString("date_time"));
                                cname.setText(j.getString("customer_name"));
                                order.setText(j.getString("product_subitems"));
                                price.setText(j.getString("subtotal") + " JD");
                                phone.setText(j.getString("contact"));
                                deli_date.setText(j.getString("delivery_time"));
                                phoneno = j.getString("contact");
                                Glide.with(mContext).load(j.getString("customer_image")).into(imv);
                                Cust_call_no = j.getString("contact");
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void AcceptReject_OrderTask(String oid, final String status, final String which_status, String reason) {

//infograins.com/INFO01/yalla/restaurant_api.php?action=AcceptRejectOrder&
        // user_id=11&order_id=5a20017b337d9&verify_status=1&reject_why=driver%20not%20available%20for%20this%20location
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ACCPET_REJECT_ORDER);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ORDER_ID, oid);
            jo.put(Constant.STATUS, status);
            if (status.equals("0")) {
                jo.put(Constant.REJECTWHY, "");
            } else {
                jo.put(Constant.REJECTWHY, reason);
            }

            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                utils.Toast(jobj.getString("msg"), null);

                                if (status.equals("0")) {
                                    if (dia_show_ondemand_req.isShowing()) {
                                        dia_show_ondemand_req.dismiss();
                                    }
                                } else if (status.equals("1")) {
                                    dia_reason_cancel.dismiss();
                                    dia_show_ondemand_req.dismiss();

                                }
                               /* utils.setPreference(Constant.ORDER_ID, "");
                                utils.setPreference(Constant.NEWUSER, "");*/
                                if (which_status.equals("0")) {
                                    DialogSendPreparedTime();
                                }
                            }else{
                                if (status.equals("0")) {
                                    if (dia_show_ondemand_req.isShowing()) {
                                        dia_show_ondemand_req.dismiss();
                                    }
                                } else if (status.equals("1")) {
                                    dia_reason_cancel.dismiss();
                                    dia_show_ondemand_req.dismiss();

                                }
                                utils.Toast(jobj.getString("msg"),null);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void CallCus() {
        if (!phoneno.equals("")) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phoneno));
                mContext.startActivity(intent);

                return;
            }
        } else {
            utils.Toast(mContext.getResources().getString(R.string.customer_no_phoneno), null);
        }
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    public void DialogSendPreparedTime() {

        dia_send_deli_time = new Dialog(mContext, R.style.MyDialog);
        dia_send_deli_time.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_send_deli_time.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_send_deli_time.setContentView(R.layout.dialog_time_prepare);
        dia_send_deli_time.setCancelable(false);
        dia_send_deli_time.show();
        RecyclerView time_lst = (RecyclerView) dia_send_deli_time.findViewById(R.id.time_lst);
        ArrayList<SingleDO> timelst = new ArrayList<>();

        time_lst.setHasFixedSize(true);
        time_lst.setLayoutManager(new LinearLayoutManager(mContext));

        for (int i = 1; i < 31; i++) {
            timelst.add(new SingleDO(i, false));
        }

        Single_txt_Adap adapter = new Single_txt_Adap(mContext, timelst);
        time_lst.setAdapter(adapter);

        dia_send_deli_time.findViewById(R.id.dia_confirm_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Time_Str.equals("")) {
                    SendTimeTask(Orderid, Time_Str);
                } else {
                    utils.Toast(mContext.getResources().getString(R.string.plz_select_time), null);
                }
            }
        });
    }

    public void SendTimeTask(String OID, String time) {
        // http://infograins.com/INFO01/yalla/restaurant_api.php?action=TimeToPrepare&
        // order_id=5a20017b337d9&time=20&user_id=11
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, "TimeToPrepare");
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ORDER_ID, OID);
            jo.put("time", time);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.Toast(j.getString("msg"), null);
                                dia_send_deli_time.dismiss();
                            } else {
                                dia_send_deli_time.dismiss();
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }

    }

    /*-------------------GET DRIVER DETAIL------------------------------------*/

    public void GetDriverDetailTask(String Oid, String Driver_id) {
        try {
            JSONObject j = new JSONObject();
            j.put(Constant.ACTION, "GetDriverDetails");
            j.put("driver_id", Driver_id);
            j.put(Constant.ORDER_ID, Oid);
            utils.startProgress();

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                JSONObject jobj = jo.getJSONObject("object");
                                rateDriver(jobj.getString("order_id"), jobj.getString("number"),
                                        jobj.getString("name"), jobj.getString("image"),
                                        jobj.getString("car_model"));

                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }


    }

    public class MyDialogClick implements View.OnClickListener {
        LinearLayout ll1, ll2, ll3;

        public MyDialogClick(LinearLayout ll1, LinearLayout ll2, LinearLayout ll3) {
            this.ll1 = ll1;
            this.ll2 = ll2;
            this.ll3 = ll3;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.call_one:
                    if (isPermissionGranted()) {
                        CallCus();
                    }

                    ll1.setVisibility(View.GONE);
                    ll2.setVisibility(View.GONE);
                    ll3.setVisibility(View.VISIBLE);

                case R.id.call_two:
                    if (isPermissionGranted()) {
                        CallCus();
                    }

                    ll1.setVisibility(View.GONE);
                    ll2.setVisibility(View.GONE);
                    ll3.setVisibility(View.VISIBLE);
                    break;

            }
        }
    }

    public class Single_txt_Adap extends RecyclerView.Adapter<Single_txt_Adap.ViewHolder> {

        Context context;
        ArrayList<SingleDO> mlist;
        int selectedPosition = -1;
        ResponseTask rt;
        Utils utils;

        public Single_txt_Adap(Context con, ArrayList<SingleDO> mlist) {
            this.context = con;
            this.mlist = mlist;

            utils = new Utils(context);
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new ViewHolder(LayoutInflater.from(context).inflate(
                    R.layout.text_single_add, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.card.setText(String.valueOf(mlist.get(position).getTime()));

            holder.ll.setTag(position);

            if (selectedPosition == position) {
                holder.card.setBackground(context.getResources().getDrawable(R.drawable.circle_white_boarder));
                holder.min_tv.setVisibility(View.VISIBLE);
            } else {
                holder.card.setBackground(context.getResources().getDrawable(R.drawable.draw_trans));
                holder.min_tv.setVisibility(View.GONE);
            }

            holder.ll.setOnClickListener(new OnItemClick(position));

        }


        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public int getItemCount() {
            return mlist.size();
        }


        public class OnItemClick implements View.OnClickListener {
            int pos;

            public OnItemClick(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                selectedPosition = (Integer) v.getTag();
                notifyDataSetChanged();
                Time_Str = String.valueOf(mlist.get(pos).getTime());

            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView card, min_tv;
            RelativeLayout ll;

            public ViewHolder(View v) {
                super(v);
                card = (TextView) v.findViewById(R.id.card);
                min_tv = (TextView) v.findViewById(R.id.min_tv);
                ll = (RelativeLayout) v.findViewById(R.id.ll);
            }
        }

    }
}
