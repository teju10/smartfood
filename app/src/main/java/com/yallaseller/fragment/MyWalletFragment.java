package com.yallaseller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.yallaseller.R;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;


/**
 * Created by and-04 on 21/11/17.
 */

public class MyWalletFragment extends Fragment implements View.OnClickListener {

    View rv;
    ResponseTask rt;
    ViewPager viewPager;
    Fragment fragment;
    Button sales_btn, appfees_btn;
    Context mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constant.LANG));

        rv = inflater.inflate(R.layout.frag_mywallet, container, false);
        mContext = getActivity();

        sales_btn = (Button) rv.findViewById(R.id.sales_btn);
        appfees_btn = (Button) rv.findViewById(R.id.appfees_btn);

        changeFragment(0);

        rv.findViewById(R.id.sales_btn).setOnClickListener(this);
        rv.findViewById(R.id.appfees_btn).setOnClickListener(this);

        return rv;
    }

    public void changeFragment(int pos) {
        switch (pos) {
            case 0:
                fragment = new SalesFragment();
                sales_btn.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                appfees_btn.setBackgroundColor(mContext.getResources().getColor(R.color.light_orange));
                sales_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                appfees_btn.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                System.out.println("");
                break;
            case 1:
                fragment = new AppFees_Fragment();
                sales_btn.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                sales_btn.setBackgroundColor(mContext.getResources().getColor(R.color.light_orange));
                appfees_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                appfees_btn.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body_home, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sales_btn:
                changeFragment(0);
                break;

            case R.id.appfees_btn:
                changeFragment(1);
                break;
        }
    }

}
