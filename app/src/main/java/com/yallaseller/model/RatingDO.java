package com.yallaseller.model;

/**
 * Created by vivek on 5/8/2017.
 */

public class RatingDO {

    private String strRating;
    private String strName;
    private String strImage;
    private String strReview;
    private String strDate;

    public String getStrRating() {
        return strRating;
    }

    public void setStrRating(String strRating) {
        this.strRating = strRating;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String getStrReview() {
        return strReview;
    }

    public void setStrReview(String strReview) {
        this.strReview = strReview;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }
}
