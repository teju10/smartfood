package com.yallaseller.model;

/**
 * Created by and-04 on 11/12/17.
 */

public class SingleDO {

    int time;
    String OrderId;
    boolean selected;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public SingleDO(int time, boolean selected) {
        this.time = time;
        this.selected = selected;
    }

}
