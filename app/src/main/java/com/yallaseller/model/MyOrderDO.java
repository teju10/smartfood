package com.yallaseller.model;

/**
 * Created by vivek on 6/27/2017.
 */

public class MyOrderDO {

    private String orderID;

    public String getOrder_id() {
        return order_id;
    }

    public String getStrCustomerReqDate() {
        return strCustomerReqDate;
    }

    public void setStrCustomerReqDate(String strCustomerReqDate) {
        this.strCustomerReqDate = strCustomerReqDate;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;

    }

    private String order_id;

    public String getDelivery_time() {
        return strDelivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        strDelivery_time = delivery_time;
    }

    private String strDriverName;
    private String strCustomerName;
    private String strCustomerReqDate;
    private String strDelivery_time;

    public String getStrCustomerName() {
        return strCustomerName;
    }

    public void setStrCustomerName(String strCustomerName) {
        this.strCustomerName = strCustomerName;
    }


    public String getStrCustomerCall() {
        return strCustomerCall;
    }

    public void setStrCustomerCall(String strCustomerCall) {
        this.strCustomerCall = strCustomerCall;
    }

    public String getStrCustomerMeal() {
        return strCustomerMeal;
    }

    public void setStrCustomerMeal(String strCustomerMeal) {
        this.strCustomerMeal = strCustomerMeal;
    }

    public String getStrCustomerImage() {
        return strCustomerImage;
    }

    public void setStrCustomerImage(String strCustomerImage) {
        this.strCustomerImage = strCustomerImage;
    }

    private String strCustomerCall;
    private String strCustomerMeal;
    private String strCustomerImage;
    private String strCustomerTotalPrice;
    private String strDriverCall;
    private String strStatus;
    private String strCarModel;

    public String getStrDriverStatus() {
        return strDriverStatus;
    }

    public void setStrDriverStatus(String strDriverStatus) {
        this.strDriverStatus = strDriverStatus;
    }

    private String strCarColor;
    private String strDriverImage;
    private String strDriverStatus;

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getStrDriverImage() {
        return strDriverImage;
    }

    public void setStrDriverImage(String strDriverImage) {
        this.strDriverImage = strDriverImage;
    }

    public String getStrDriverName() {
        return strDriverName;
    }

    public void setStrDriverName(String strDriverName) {
        this.strDriverName = strDriverName;
    }

    public String getStrDriverCall() {
        return strDriverCall;
    }

    public void setStrDriverCall(String strDriverCall) {
        this.strDriverCall = strDriverCall;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public String getStrCarModel() {
        return strCarModel;
    }

    public void setStrCarModel(String strCarModel) {
        this.strCarModel = strCarModel;
    }

    public String getStrCarColor() {
        return strCarColor;
    }

    public void setStrCarColor(String strCarColor) {
        this.strCarColor = strCarColor;
    }

    public String getStrCustomerTotalPrice() {
        return strCustomerTotalPrice;
    }

    public void setStrCustomerTotalPrice(String strCustomerTotalPrice) {
        this.strCustomerTotalPrice = strCustomerTotalPrice;
    }
}
