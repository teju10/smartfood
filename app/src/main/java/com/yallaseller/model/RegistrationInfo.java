package com.yallaseller.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yallaseller.api.KeyAbstract;


/**
 * Created by vivek on 3/7/2017.
 */
public class RegistrationInfo extends KeyAbstract {

    @SerializedName(KEY_USERID)
    @Expose
    private String id;

    @SerializedName(KEY_ORDER_ID)
    @Expose
    private String orderId;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    @SerializedName(KEY_EMAIL)

    @Expose
    private String email;

    @SerializedName(ORDERID)
    @Expose
    private String orderid;

    @SerializedName(DELIVERYTIME)
    @Expose
    private String delivery_time;


    @SerializedName(KEY_IMAGE)
    @Expose
    private String imageLink;

    @SerializedName(KEY_OTP)
    @Expose
    private String otp;

    @SerializedName(KEY_NOTI_MSG)
    @Expose
    private String notiMsg;

    @SerializedName(KEY_NOTI)
    @Expose
    private String notiTitle;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    @SerializedName(KEY_NOTI_TIME)
    @Expose
    private String notiTIme;

    @SerializedName(KEY_NAME)
    @Expose
    private String name;

    @SerializedName(KEY_CUSTOMER_MEAL)
    @Expose
    private String customermeal;


    @SerializedName(KEY_CUSTOMER_CALL)
    @Expose
    private String customercall;

    @SerializedName(KEY_CUSTOMER_IMAGE)
    @Expose
    private String customerimg;

    @SerializedName(KEY_CUSTOMERNAME)
    @Expose
    private String customername;

    @SerializedName(KEY_CUSTOMER_TOTALPRICE)
    @Expose
    private String customertotalprice;


    @SerializedName(KEY_PHONE)
    @Expose
    private String phone;

    @SerializedName(KEY_COUNTRY)
    @Expose
    private String country;

    public String getStrDriverStatus() {
        return strDriverStatus;
    }

    public void setStrDriverStatus(String strDriverStatus) {
        this.strDriverStatus = strDriverStatus;
    }

    @SerializedName(KEY_ISCOMPLETE)
    @Expose
    private String isOrderComplete;

    @SerializedName(KEY_DRIVERSTATUS)
    @Expose
    private String strDriverStatus;

    @SerializedName(KEY_DURATION)
    @Expose
    private String strDuration;

    @SerializedName(KEY_STORE_NAME)
    @Expose
    private String strStoreName;

    @SerializedName(KEY_DISTANCE)
    @Expose
    private String strDistance;

    @SerializedName(KEY_PRICE)
    @Expose
    private String strPrice;

    public String getStrCustomerReqDate() {
        return strCustomerReqDate;
    }

    public void setStrCustomerReqDate(String strCustomerReqDate) {
        this.strCustomerReqDate = strCustomerReqDate;
    }

    @SerializedName(KEY_CUR_ADD)
    @Expose
    private String strCurAddress;

    @SerializedName(KEY_CUSTOMER_REQDATE)
    @Expose
    private String strCustomerReqDate;


    @SerializedName(KEY_LATIYUDE)
    @Expose
    private String strLat;

    @SerializedName(KEY_LONGITUDE)
    @Expose
    private String strLongi;

    @SerializedName(KEY_DEGREE)
    @Expose
    private String strDegree;

    @SerializedName(KEY_DRIVER_ID)
    @Expose
    private String strDriverId;

    @SerializedName(KEY_REQUEST_ID)
    @Expose
    private String strReqId;

    @SerializedName(KEY_CAR_MODEL)
    @Expose
    private String strCarModel;

    @SerializedName(KEY_CAR_COLOR)
    @Expose
    private String strCarColor;


    @SerializedName(KEY_BRANCH_NAME)
    @Expose
    private String strBranchName;

    @SerializedName(KEY_CITY)
    @Expose
    private String strCity;

    public String getCustomermeal() {
        return customermeal;
    }

    public void setCustomermeal(String customermeal) {
        this.customermeal = customermeal;
    }



    public String getCustomerimg() {
        return customerimg;
    }

    public void setCustomerimg(String customerimg) {
        this.customerimg = customerimg;
    }

    @SerializedName(KEY_LANG)
    @Expose
    private String strLanguage;

    @SerializedName(KEY_IS_BUSY)
    @Expose
    private String isBusy;

    @SerializedName(KEY_ORDER1)
    @Expose
    private String strKeyOrder1;

    @SerializedName(KEY_ORDER2)
    @Expose
    private String strKeyOrder2;

    @SerializedName(KEY_ORDER3)
    @Expose
    private String strKeyOrder3;


    public RegistrationInfo() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getCustomertotalprice() {
        return customertotalprice;
    }

    public void setCustomertotalprice(String customertotalprice) {
        this.customertotalprice = customertotalprice;}

    public String getStrDuration() {
        return strDuration;
    }

    public void setStrDuration(String strDuration) {
        this.strDuration = strDuration;
    }

    public String getStrDistance() {
        return strDistance;
    }

    public void setStrDistance(String strDistance) {
        this.strDistance = strDistance;
    }

    public String getStrPrice() {
        return strPrice;
    }

    public void setStrPrice(String strPrice) {
        this.strPrice = strPrice;
    }

    public String getStrCurAddress() {
        return strCurAddress;
    }

    public void setStrCurAddress(String strCurAddress) {
        this.strCurAddress = strCurAddress;
    }

    public String getStrLongi() {
        return strLongi;
    }

    public void setStrLongi(String strLongi) {
        this.strLongi = strLongi;
    }

    public String getStrLat() {
        return strLat;
    }

    public void setStrLat(String strLat) {
        this.strLat = strLat;
    }

    public String getStrDegree() {
        return strDegree;
    }

    public void setStrDegree(String strDegree) {
        this.strDegree = strDegree;
    }

    public String getStrDriverId() {
        return strDriverId;
    }

    public void setStrDriverId(String strDriverId) {
        this.strDriverId = strDriverId;
    }

    public String getStrReqId() {
        return strReqId;
    }

    public void setStrReqId(String strReqId) {
        this.strReqId = strReqId;
    }

    public String getStrCarModel() {
        return strCarModel;
    }

    public void setStrCarModel(String strCarModel) {
        this.strCarModel = strCarModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStrStoreName() {
        return strStoreName;
    }

    public void setStrStoreName(String strStoreName) {
        this.strStoreName = strStoreName;
    }

    public String getStrBranchName() {
        return strBranchName;
    }

    public void setStrBranchName(String strBranchName) {
        this.strBranchName = strBranchName;
    }

    public String getStrCity() {
        return strCity;
    }

    public void setStrCity(String strCity) {
        this.strCity = strCity;
    }

    public String getStrLanguage() {
        return strLanguage;
    }

    public void setStrLanguage(String strLanguage) {
        this.strLanguage = strLanguage;
    }

    public String getIsBusy() {
        return isBusy;
    }

    public void setIsBusy(String isBusy) {
        this.isBusy = isBusy;
    }

    public String getStrKeyOrder1() {
        return strKeyOrder1;
    }

    public void setStrKeyOrder1(String strKeyOrder1) {
        this.strKeyOrder1 = strKeyOrder1;
    }

    public String getStrKeyOrder2() {
        return strKeyOrder2;
    }

    public void setStrKeyOrder2(String strKeyOrder2) {
        this.strKeyOrder2 = strKeyOrder2;
    }

    public String getStrKeyOrder3() {
        return strKeyOrder3;
    }

    public void setStrKeyOrder3(String strKeyOrder3) {
        this.strKeyOrder3 = strKeyOrder3;
    }

    public String getIsOrderComplete() {
        return isOrderComplete;
    }

    public void setIsOrderComplete(String isOrderComplete) {
        this.isOrderComplete = isOrderComplete;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStrCarColor() {
        return strCarColor;
    }

    public void setStrCarColor(String strCarColor) {
        this.strCarColor = strCarColor;
    }


    public String getNotiMsg() {
        return notiMsg;
    }

    public void setNotiMsg(String notiMsg) {
        this.notiMsg = notiMsg;
    }

    public String getNotiTitle() {
        return notiTitle;
    }

    public void setNotiTitle(String notiTitle) {
        this.notiTitle = notiTitle;
    }

    public String getNotiTIme() {
        return notiTIme;
    }

    public void setNotiTIme(String notiTIme) {
        this.notiTIme = notiTIme;
    }

    public String getCustomercall() {
        return customercall;
    }

    public void setCustomercall(String customercall) {
        this.customercall = customercall;
    }
}
