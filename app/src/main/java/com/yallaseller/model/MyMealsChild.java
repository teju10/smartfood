package com.yallaseller.model;

/**
 * Created by and-04 on 22/11/17.
 */

public class MyMealsChild {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private  int id;
    private String ProName,ProType,price,price_disprice,img,status,price_discount;

    public int getId() {
        return id;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProName() {
        return ProName;

    }

    public void setProName(String proName) {
        ProName = proName;
    }

    public String getProType() {
        return ProType;
    }

    public void setProType(String proType) {
        ProType = proType;
    }

    public String getPrice_disprice() {
        return price_disprice;
    }

    public void setPrice_disprice(String price_disprice) {
        this.price_disprice = price_disprice;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public MyMealsChild(int id,String proName, String proType, String price,
                        String img,String dis_price,String _status,String dis_percent) {
        this.id = id;
        this.price = price;
        ProName = proName;
        ProType = proType;
        this.price_disprice = dis_price;
        this.img = img;
        this.status = _status;
        this.price_discount = dis_percent;
    }

    public MyMealsChild(int id,String proName,String price) {
        this.id = id;
        ProName = proName;
        this.price = price;
    }
}
