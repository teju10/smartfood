package com.yallaseller.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by and-04 on 22/11/17.
 */

public class MyMealsGroup {
    private int id;
    private String HeadName;
    private List<MyMealsChild> Items;

    public MyMealsGroup(int id,String headName, List<MyMealsChild> items) {
        this.id = id;
        HeadName = headName;
        Items = items;
    }

    public String getHeadName() {
        return HeadName;
    }

    public List<MyMealsChild> getItems() {
        return Items;
    }

    public int getId() {
        return id;
    }
}
