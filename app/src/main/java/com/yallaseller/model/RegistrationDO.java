package com.yallaseller.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yallaseller.api.KeyAbstract;

import java.util.ArrayList;

/**
 * Created by vivek on 3/7/2017.
 */

public class RegistrationDO extends KeyAbstract {

    @SerializedName(KEY_POST_DATA)
    @Expose
    private ArrayList<RegistrationInfo> postdata = new ArrayList<>();

    @SerializedName(KEY_ORDER1)
    @Expose
    private ArrayList<RegistrationInfo> post_order1 = new ArrayList<>();

    @SerializedName(KEY_ORDER2)
    @Expose
    private ArrayList<RegistrationInfo> post_order2 = new ArrayList<>();

    @SerializedName(KEY_ORDER3)
    @Expose
    private ArrayList<RegistrationInfo> post_order3 = new ArrayList<>();


    @SerializedName(KEY_SUCCESS)
    @Expose
    private String success;
    @SerializedName(KEY_SUCCESS_MSG)
    @Expose
    private String message;


    @SerializedName(KEY_ONE_STAR)
    @Expose
    private String oneStar;

    @SerializedName(KEY_TWO_STAR)
    @Expose
    private String twoStar;

    @SerializedName(KEY_THREE_STAR)
    @Expose
    private String threeStar;

    @SerializedName(KEY_FOUR_STAR)
    @Expose
    private String fourStar;

    @SerializedName(KEY_FIVE_STAR)
    @Expose
    private String fivetar;

    @SerializedName(KEY_TOTAL_RATING)
    @Expose
    private String totalRating;

    @SerializedName(KEY_TOTAL_REVIEW)
    @Expose
    private String totalReview;


    @SerializedName(KEY_OLD_PWRD)
    @Expose
    private String strOldPassword;

    @SerializedName(KEY_NEW_PWRD)
    @Expose
    private String strNewPassword;

    @SerializedName(KEY_COUNT_ORDER_COMPLETE)
    @Expose
    private String strOrderComplete;


    @SerializedName(KEY_COUNT_ORDER_ONWAY)
    @Expose
    private String strOrderInComplete;

    public RegistrationDO() {        //default constructor
    }

    public ArrayList<RegistrationInfo> getPostdata() {
        return postdata;
    }

    public void setPostdata(ArrayList<RegistrationInfo> postdata) {
        this.postdata = postdata;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOneStar() {
        return oneStar;
    }

    public void setOneStar(String oneStar) {
        this.oneStar = oneStar;
    }

    public String getTwoStar() {
        return twoStar;
    }

    public void setTwoStar(String twoStar) {
        this.twoStar = twoStar;
    }

    public String getThreeStar() {
        return threeStar;
    }

    public void setThreeStar(String threeStar) {
        this.threeStar = threeStar;
    }

    public String getFourStar() {
        return fourStar;
    }

    public void setFourStar(String fourStar) {
        this.fourStar = fourStar;
    }

    public String getFivetar() {
        return fivetar;
    }

    public void setFivetar(String fivetar) {
        this.fivetar = fivetar;
    }

    public String getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public void setTotalReview(String totalReview) {
        this.totalReview = totalReview;
    }

    public ArrayList<RegistrationInfo> getPost_order1() {
        return post_order1;
    }

    public void setPost_order1(ArrayList<RegistrationInfo> post_order1) {
        this.post_order1 = post_order1;
    }

    public ArrayList<RegistrationInfo> getPost_order2() {
        return post_order2;
    }

    public void setPost_order2(ArrayList<RegistrationInfo> post_order2) {
        this.post_order2 = post_order2;
    }

    public ArrayList<RegistrationInfo> getPost_order3() {
        return post_order3;
    }

    public void setPost_order3(ArrayList<RegistrationInfo> post_order3) {
        this.post_order3 = post_order3;
    }

    public String getStrOldPassword() {
        return strOldPassword;
    }

    public void setStrOldPassword(String strOldPassword) {
        this.strOldPassword = strOldPassword;
    }

    public String getStrNewPassword() {
        return strNewPassword;
    }

    public void setStrNewPassword(String strNewPassword) {
        this.strNewPassword = strNewPassword;
    }

    public String getStrOrderComplete() {
        return strOrderComplete;
    }

    public void setStrOrderComplete(String strOrderComplete) {
        this.strOrderComplete = strOrderComplete;
    }

    public String getStrOrderInComplete() {
        return strOrderInComplete;
    }

    public void setStrOrderInComplete(String strOrderInComplete) {
        this.strOrderInComplete = strOrderInComplete;
    }
}

