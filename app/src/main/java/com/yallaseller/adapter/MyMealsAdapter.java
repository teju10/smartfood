package com.yallaseller.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.yallaseller.R;
import com.yallaseller.activity.MyMeals_SubCategoryAct;
import com.yallaseller.model.MyMealsChild;
import com.yallaseller.model.MyMealsGroup;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by and-04 on 21/11/17.
 */

public class MyMealsAdapter extends BaseExpandableListAdapter {

    private static final String TAG = MyMealsAdapter.class.getSimpleName();
    private Context mContext;
    private List<MyMealsGroup> groupList;
    private List<MyMealsChild> childdata;
    private ResponseTask rt;
    private Utils utils;
    private Dialog edit_dia, add_dis_dia;
    private EditText edit_child_item_name, edit_child_sub_item, edit_child_price;

    public MyMealsAdapter(Context context, List<MyMealsGroup> groupList, List<MyMealsChild> childdata) {
        this.mContext = context;
        this.groupList = groupList;
        this.childdata = childdata;
        utils = new Utils(mContext);
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = 0;
        if (groupList.get(groupPosition).getItems() != null)
            size = groupList.get(groupPosition).getItems().size();
        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupList.get(groupPosition).getItems().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.mymeal_header_itemtxt, null);
        }
        ((TextView) convertView.findViewById(R.id.lblListHeader)).setText(groupList.get(groupPosition).getHeadName());

        if (groupList.get(groupPosition).getItems().size()>0){
            ((ImageView) convertView.findViewById(R.id.down)).setVisibility(View.VISIBLE);
        }else{
            ((ImageView) convertView.findViewById(R.id.down)).setVisibility(View.GONE);
        }

        ((ImageView) convertView.findViewById(R.id.down))
                .setBackgroundResource(isExpanded ? R.drawable.expand_up : R.drawable.expand_down);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final MyMealsChild childText = (MyMealsChild) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.mymeals_itemlayout, null);
        }
        final TextView my_meal_title = (TextView) convertView.findViewById(R.id.my_meal_title);
        final TextView my_meal_subcat = (TextView) convertView.findViewById(R.id.my_meal_protype);
        final TextView child_price = (TextView) convertView.findViewById(R.id.child_price);
        final TextView child_dis_price = (TextView) convertView.findViewById(R.id.child_dis_price);
        final ImageView meal_image = (ImageView) convertView.findViewById(R.id.meal_img);
        final Button remove_offer = (Button) convertView.findViewById(R.id.remove_offer);
        final Button add_offer = (Button) convertView.findViewById(R.id.add_offer);

//        my_meal_title.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
//        my_meal_subcat.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        if (childText.getStatus().equals("0")) {
            child_price.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            child_price.setPaintFlags(child_price.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            add_offer.setText(mContext.getResources().getString(R.string.make_offer));
            child_dis_price.setVisibility(View.GONE);
            remove_offer.setVisibility(View.GONE);

        } else if (childText.getStatus().equals("1")) {
            child_dis_price.setVisibility(View.VISIBLE);
            child_price.setTextColor(mContext.getResources().getColor(R.color.grey_600));
            child_dis_price.setText(childText.getPrice_disprice() + " JD");
            child_price.setPaintFlags((child_price).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            add_offer.setText(mContext.getResources().getString(R.string.edit_offer));
            remove_offer.setVisibility(View.VISIBLE);
        }

        child_price.setText(childText.getPrice() + " JD");
        my_meal_title.setText(childText.getProName());
        my_meal_subcat.setText(childText.getProType());
        final View finalConvertView = convertView;

        Glide.with(mContext).load(childText.getImg()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                ((ProgressBar) finalConvertView.findViewById(R.id.pb_meals)).setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                ((ProgressBar) finalConvertView.findViewById(R.id.pb_meals)).setVisibility(View.GONE);

                return false;
            }
        }).into(meal_image);

        ((Button) convertView.findViewById(R.id.btn_edt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditCatgoryDialog(childText.getId(), my_meal_title.getText().toString(), my_meal_subcat.getText().toString(),
                        childText.getPrice(), groupPosition, childPosition);
            }
        });

        add_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddDiaDialog(childText.getId(), groupPosition, childPosition, childText.getPrice_discount());
            }
        });

        ((ImageView) convertView.findViewById(R.id.delete_cat_item)).setOnClickListener(new EditItem(childText.getId(), groupPosition, childPosition));

        ((Button) convertView.findViewById(R.id.remove_offer)).setOnClickListener(new RemoveOffer(childText.getId(), groupPosition, childPosition));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, MyMeals_SubCategoryAct.class);
                System.out.printf("--------->>>>" + childText.getId() + "--------->>>>" + childText.getProName());
                i.putExtra(Constant.MENUID, String.valueOf(childText.getId()));
                i.putExtra(Constant.MENUNAME, childText.getProName());
                mContext.startActivity(i);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void EditCatgoryDialog(final int cid, String child_item, String sub_c, String p, final int g_pos, final int c_pos) {

        edit_dia = new Dialog(mContext, R.style.MyDialog);
        edit_dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        edit_dia.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        edit_dia.setContentView(R.layout.dialog_editnew_catgry);
        edit_dia.setCancelable(false);
        edit_dia.show();

        edit_child_item_name = (EditText) edit_dia.findViewById(R.id.edit_child_item_name);
        edit_child_sub_item = (EditText) edit_dia.findViewById(R.id.edit_child_sub_item);
        edit_child_price = (EditText) edit_dia.findViewById(R.id.edit_child_price);

        edit_child_item_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        edit_child_sub_item.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        edit_child_item_name.setText(child_item);
        edit_child_sub_item.setText(sub_c);
        if(p.equals("0")){
            edit_child_price.setText("");
        }else{
            edit_child_price.setText(p);
        }


        edit_dia.findViewById(R.id.submit_edited_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(edit_child_price.length()>11){
                    utils.Toast(mContext.getResources().getString(R.string.price_less_11));
                }*/
                GetCatogoryTask(cid, edit_child_item_name.getText().toString(), edit_child_sub_item.getText().toString(),
                        edit_child_price.getText().toString(), g_pos, c_pos);
            }
        });

        edit_dia.findViewById(R.id.close_dia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_dia != null && edit_dia.isShowing()) {
                    edit_dia.cancel();
                }
            }
        });
    }

    public void AddDiaDialog(final int menu_id, final int group_pos, final int child_pos, String dis_perc) {

        add_dis_dia = new Dialog(mContext, R.style.MyDialog);
        add_dis_dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        add_dis_dia.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        add_dis_dia.setContentView(R.layout.dialog_add_discount);
        add_dis_dia.setCancelable(false);
        add_dis_dia.show();

        final EditText dis_edttxt = (EditText) add_dis_dia.findViewById(R.id.dis_edttxt);

        if (!dis_perc.equals("")) {
            dis_edttxt.setText(dis_perc);
        }

        add_dis_dia.findViewById(R.id.submit_dis_price).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (utils.isNetConnected()) {
                    if (dis_edttxt.getText().toString().equals("")) {
                        utils.Toast(mContext.getResources().getString(R.string.enterdiscount), null);
                    } else if (Double.parseDouble(dis_edttxt.getText().toString()) > 100) {
                        utils.Toast(mContext.getResources().getString(R.string.max_discount_price), null);
                    } else {
                        AddDiscountTask(menu_id, dis_edttxt.getText().toString(), group_pos, child_pos);
                    }
                } else {
                    utils.Toast(mContext.getResources().getString(R.string.check_internet), null);

                }
            }
        });

        add_dis_dia.findViewById(R.id.close_dia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (add_dis_dia != null && add_dis_dia.isShowing()) {
                    add_dis_dia.cancel();
                }
            }
        });
    }

    public void GetCatogoryTask(int cid, String subcat_name, String subcat_type, String price, final int gpos, final int cpos) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.EDIT_MYMEALSITEM);
            jo.put("name", subcat_name);
            jo.put("title", subcat_type);
            jo.put("price", price);
            jo.put("id", cid);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                if (edit_dia.isShowing()) {
                                    edit_dia.dismiss();
                                }
                                utils.Toast(mContext.getResources().getString(R.string.cat_edti_success), null);
                                groupList.get(gpos).getItems().get(cpos).setProName(j.getString("name"));
                                groupList.get(gpos).getItems().get(cpos).setProType(j.getString("title"));
                                groupList.get(gpos).getItems().get(cpos).setPrice(j.getString("price"));
                                groupList.get(gpos).getItems().get(cpos).setPrice_disprice(j.getString("discount_price"));
                                notifyDataSetChanged();
                                utils.hideKeyboard(mContext);
                            } else {

                            }
                        } catch (JSONException jsex) {
                            jsex.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException jexc) {
            jexc.printStackTrace();
        }
    }

    public void DeleteCat(final int sub_id, final int pos, final int groupPosition) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder
                .setTitle(mContext.getResources().getString(R.string.deletecat_msg))
                .setMessage(mContext.getResources().getString(R.string.confirm_delete_msg))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        DeleteSubCat(sub_id, pos, groupPosition);
                       /* MyMealsChild myMealsChild = groupList.get(groupPosition).getItems().get(pos);
                        childdata.remove(myMealsChild);
                        notifyDataSetChanged();*/
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        builder.setCancelable(true);
                    }
                })
                .show();
    }

    public void DeleteSubCat(int catid, final int pos, final int groupPosition) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.DELETEITEM);
            jo.put(Constant.CAT_ID, catid);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                MyMealsChild childText = groupList.get(groupPosition).getItems().get(pos);
                                Log.e(TAG, "onClick =====> " + childText.toString());
                                groupList.get(groupPosition).getItems().remove(childText);
                                notifyDataSetChanged();
                                utils.Toast(mContext.getResources().getString(R.string.cat_delete_success), null);
                            } else {

                            }
                        } catch (JSONException jsex) {
                            jsex.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void AddDiscountTask(int menu_id, final String dis_p, final int group_pos, final int child_pos) {
        // http://infograins.com/INFO01/yalla/restaurant_api.php?action=Add_discount&
        // menu_id=52&discount_percentage=10
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ADD_DISCOUNT);
            jo.put(Constant.MENUID, menu_id);
            jo.put(Constant.DIS_PERCENT, dis_p);

            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.hideKeyboard(mContext);

                                if (add_dis_dia.isShowing()) {
                                    add_dis_dia.dismiss();
                                }
                                groupList.get(group_pos).getItems().get(child_pos).setPrice_disprice(j.getString(Constant.DIS_PRICE));
                                groupList.get(group_pos).getItems().get(child_pos).setStatus(j.getString(Constant.DIS_STATUS));
                                groupList.get(group_pos).getItems().get(child_pos).setPrice_discount(j.getString(Constant.DIS_PERCENT));
                                notifyDataSetChanged();
                            } else {
                                utils.Toast(j.getString("msg"), null);
                            }
                        } catch (JSONException jsex) {
                            jsex.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException jexc) {
            jexc.printStackTrace();
        }

    }

    public void RemoveDiscountTask(int menu_id, final int group_pos, final int child_pos) {
        // http://infograins.com/INFO01/yalla/restaurant_api.php?action=Add_discount&
        // menu_id=52&discount_percentage=10
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ADD_DISCOUNT);
            jo.put(Constant.MENUID, menu_id);
            jo.put(Constant.DIS_PRICE, "0");

            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.hideKeyboard(mContext);
                                groupList.get(group_pos).getItems().get(child_pos).setStatus(j.getString(Constant.DIS_STATUS));
                                groupList.get(group_pos).getItems().get(child_pos).setPrice_disprice(j.getString(Constant.DIS_PRICE));
                                notifyDataSetChanged();
                            } else {
                                utils.Toast(j.getString("msg"), null);
                            }
                        } catch (JSONException jse) {
                            jse.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException jexc) {
            jexc.printStackTrace();
        }

    }

    public class EditItem implements View.OnClickListener {
        int group_pos, child_pos, id;

        EditItem(int id, int group_pos, int childPosition) {
            this.group_pos = group_pos;
            this.child_pos = childPosition;
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            DeleteSubCat(id, child_pos, group_pos);

        }
    }

    public class RemoveOffer implements View.OnClickListener {
        int id, group_pos, child_pos;

        RemoveOffer(int id, int group_pos, int child_pos) {
            this.id = id;
            this.group_pos = group_pos;
            this.child_pos = child_pos;
        }

        @Override
        public void onClick(View v) {
            RemoveDiscountTask(id, group_pos, child_pos);
        }
    }

}