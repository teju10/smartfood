package com.yallaseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.model.NotificationDO;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vivek on 6/27/2017.
 */

public class NotificationAdapter extends ArrayAdapter<NotificationDO> {

    private ArrayList<NotificationDO> listOrder;
    private Context con;

    public NotificationAdapter(@NonNull Context con, @LayoutRes int resource, @NonNull ArrayList<NotificationDO> list) {
        super(con, resource, list);
        this.con = con;
        listOrder = list;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {
        return listOrder.size();
    }

    @Override
    public NotificationDO getItem(int position) {
        return listOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) con
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(
                    R.layout.row_notification, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textTime = (TextView) convertView.findViewById(R.id.noti_time);
            viewHolder.textTitle = (TextView) convertView.findViewById(R.id.noti_title);
            viewHolder.textMsg = (TextView) convertView.findViewById(R.id.noti_msg);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            final NotificationDO foodDo = listOrder.get(position);
            viewHolder.textTitle.setText(foodDo.getTitle());
            viewHolder.textMsg.setText(foodDo.getMsg());
            viewHolder.textTime.setText(foodDo.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    class ViewHolder {
        TextView textTitle;
        TextView textMsg;
        TextView textTime;
    }
}
