package com.yallaseller.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.activity.OrderActivity;
import com.yallaseller.model.MyOrderDO;
import com.yallaseller.model.SingleDO;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vivek on 6/27/2017.
 */

public class OrderAdapter extends BaseAdapter {

    private static String TAG = OrderAdapter.class.getName();
    public int counter;
    ResponseTask rt;
    Utils utils;
    Dialog dia_send_deli_time,dia_reason_cancel;
    String Time_Str = "";
    CountDownTimer countDownTimer;
    private ArrayList<MyOrderDO> listOrder;
    private Context con;

    public OrderAdapter(Context con, ArrayList<MyOrderDO> list) {
        this.con = con;
        listOrder = list;
        utils = new Utils(con);
    }

    @Override
    public int getCount() {
        return listOrder.size();

    }

    @Override
    public Object getItem(int position) {
        return listOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) con
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(
                    R.layout.row_order, parent, false);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {

            final MyOrderDO foodDo = listOrder.get(position);
            Picasso.with(con).load(foodDo.getStrCustomerImage()).into(viewHolder.order_cust_img);
            viewHolder.cust_phone_txt.setMovementMethod(LinkMovementMethod.getInstance());

            viewHolder.textOrderId.setText(foodDo.getOrderID());
            viewHolder.txtCustName.setText(foodDo.getStrCustomerName());
            viewHolder.order_deli_time.setText(foodDo.getDelivery_time());
            viewHolder.txtCustCall.setText(foodDo.getStrCustomerCall());
            viewHolder.txtCustMeal.setText(foodDo.getStrCustomerMeal());

            viewHolder.txtCust_tprice.setText(foodDo.getStrCustomerTotalPrice() + " JD");

            viewHolder.textDriverName.setText(foodDo.getStrDriverName());
            viewHolder.textDriverCall.setText(foodDo.getStrDriverCall());
            viewHolder.textCarModel.setText(foodDo.getStrCarModel());
            viewHolder.textCarColor.setText(foodDo.getStrCarColor());

            if (foodDo.getStrStatus().equals("1")) {
                viewHolder.layoutTrack.setVisibility(View.GONE);
                viewHolder.driver_detail_ll.setVisibility(View.VISIBLE);
            } else {
                viewHolder.layoutTrack.setVisibility(View.VISIBLE);
                viewHolder.driver_detail_ll.setVisibility(View.GONE);
            }

            if (foodDo.getStrStatus().equals("0")) {
                if (foodDo.getStrDriverStatus().equals("0")) {
                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_yellow));
                    viewHolder.accpt_reject_ll.setVisibility(View.VISIBLE);
                    viewHolder.layoutTrack.setVisibility(View.GONE);
                    viewHolder.req_driver_now.setVisibility(View.GONE);

                    viewHolder.textStatus.setText(con.getResources().getString(R.string.awaiting_approve));

                    setTimer(viewHolder.timer_autoreject, viewHolder.accpt_reject_ll, foodDo.getStrCustomerReqDate());

                } else if (foodDo.getStrDriverStatus().equals("1")) {

                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.layoutTrack.setVisibility(View.GONE);
                    viewHolder.req_driver_now.setVisibility(View.VISIBLE);
                    viewHolder.layout_cancel_req.setVisibility(View.GONE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_yellow));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_preparing));

                } else if (foodDo.getStrDriverStatus().equals("3")) {
                    viewHolder.layoutTrack.setVisibility(View.GONE);
                    viewHolder.req_driver_now.setVisibility(View.GONE);
                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.layout_cancel_req.setVisibility(View.VISIBLE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_yellow));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_preparing));

                } else if (foodDo.getStrDriverStatus().equals("4")) {
                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.driver_detail_ll.setVisibility(View.VISIBLE);
                    viewHolder.layoutTrack.setVisibility(View.VISIBLE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_orange));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_on_the_way_pick));
                } else if (foodDo.getStrDriverStatus().equals("6")) {
                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.driver_detail_ll.setVisibility(View.VISIBLE);
                    viewHolder.layoutTrack.setVisibility(View.GONE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_orange));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_pickup));
                }
            } else if (foodDo.getStrStatus().equals("1")) {
                if (foodDo.getStrDriverStatus().equals("5")) {
                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.layoutTrack.setVisibility(View.GONE);
                    viewHolder.driver_detail_ll.setVisibility(View.GONE);
                    viewHolder.layout_cancel_req.setVisibility(View.GONE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_red));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_rejectbyrest));
                } else if (foodDo.getStrDriverStatus().equals("7")) {

                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.layoutTrack.setVisibility(View.GONE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_green));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.completed));
                } else if (foodDo.getStrDriverStatus().equals("2")) {
                    viewHolder.layoutTrack.setVisibility(View.GONE);
                    viewHolder.req_driver_now.setVisibility(View.GONE);
                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);
                    viewHolder.driver_detail_ll.setVisibility(View.GONE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_red));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_rejectbyrest));

                } else if (foodDo.getStrDriverStatus().equals("8")) {
                    viewHolder.layoutTrack.setVisibility(View.GONE);
                    viewHolder.req_driver_now.setVisibility(View.GONE);
                    viewHolder.accpt_reject_ll.setVisibility(View.GONE);

                    viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_red));
                    viewHolder.textStatus.setText(con.getResources().getString(R.string.status_rejectbycust));

                }
            }

            /*} else if (foodDo.getStrDriverStatus().equals("4")) {
                viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.round_red));
                viewHolder.textStatus.setText(con.getResources().getString(R.string.reject));
            } else if (foodDo.getStrDriverStatus().equals("6")) {
                viewHolder.textStatus.setBackground(con.getResources().getDrawable(R.drawable.edt_green));
                viewHolder.textStatus.setText(con.getResources().getString(R.string.completed));
            }*/

            viewHolder.cus_call_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + foodDo.getStrCustomerCall()));
                    if (ActivityCompat.checkSelfPermission(con, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    con.startActivity(callIntent);
                }
            });

            viewHolder.driver_call_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + foodDo.getStrDriverCall()));
                    if (ActivityCompat.checkSelfPermission(con, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    con.startActivity(callIntent);
                }
            });

            viewHolder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AcceptReject_OrderTask(foodDo.getOrder_id(), "0", viewHolder.accpt_reject_ll,"");
                }
            });

            viewHolder.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogCancelReq(foodDo.getOrder_id(),viewHolder.accpt_reject_ll);
                    //AcceptReject_OrderTask(foodDo.getOrder_id(), "1", viewHolder.accpt_reject_ll,);
                }
            });

            viewHolder.req_driver_now.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RequestDriverNow(foodDo.getOrder_id(), viewHolder.req_driver_now);
                }
            });
            viewHolder.layout_cancel_req.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cancel_OrderTask(foodDo.getOrder_id(), viewHolder.accpt_reject_ll, viewHolder.layout_cancel_req);
                }
            });

            viewHolder.layoutTrack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(con, SellerDrawer.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    con.startActivity(intent);
                    ((Activity) con).finish();
                }
            });
            viewHolder.layout_cancel_req.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void DialogCancelReq(final String oid, final LinearLayout ll) {

        dia_reason_cancel = new Dialog(con, R.style.MyDialog);
        dia_reason_cancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_reason_cancel.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_reason_cancel.setContentView(R.layout.dialog_cancel_request_reason);
        dia_reason_cancel.setCancelable(false);
        dia_reason_cancel.show();

        final EditText edt_txt = ((EditText) dia_reason_cancel.findViewById(R.id.edt_txt));

        dia_reason_cancel.findViewById(R.id.btn_sbmt_cancelreq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edt_txt.getText().toString().equals("")) {
                    AcceptReject_OrderTask(oid,  "1", ll,edt_txt.getText().toString());
                } else {
                    utils.Toast(con.getResources().getString(R.string.fill_reason), null);
                }
            }
        });



    }

    private void setTimer(final TextView tv, final LinearLayout ll, String date) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("Jordon"));
        String currentDate = df.format(Calendar.getInstance().getTime());
        Log.e(TAG, "timerMyOrder: " + currentDate);

        try {
            Date d1 = df.parse(currentDate);
            Date d2 = df.parse(date);
            long diff = d1.getTime() - d2.getTime();
            long diffMinutes = diff / (60 * 1000) % 60;
            Log.e(TAG, "setTimer: " + diffMinutes);

            if (diffMinutes >= 0 && diffMinutes < 10) {
                String str_time = con.getResources().getString(R.string.timeout_until_order) + " " + String.valueOf(10 - diffMinutes) + ":00";
                tv.setText(str_time);
                final long ab = (11 * 60 * 1000 - diffMinutes * 60 * 1000);
                Log.e(TAG, "setTimer: " + str_time);

                countDownTimer = new CountDownTimer(ab, 1000) {
                    @Override
                    public void onTick(long l) {
                        Log.e(TAG, "onTick: " + l);
                        long millis = l;
                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                        String str_time = con.getResources().getString(R.string.timeout_until_order) + " " + (l / 60000) + ":00";
                        tv.setText(con.getResources().getString(R.string.timeout_until_order)+" "+hms);
                    }

                    @Override
                    public void onFinish() {
                        countDownTimer.cancel();
                        ll.setVisibility(View.GONE);

                    }
                }.start();

            } else {
                ll.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DialogSendPreparedTime(final String oid) {

        dia_send_deli_time = new Dialog(con, R.style.MyDialog);
        dia_send_deli_time.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_send_deli_time.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_send_deli_time.setContentView(R.layout.dialog_time_prepare);
        dia_send_deli_time.setCancelable(false);
        dia_send_deli_time.show();
        RecyclerView time_lst = (RecyclerView) dia_send_deli_time.findViewById(R.id.time_lst);
        ArrayList<SingleDO> timelst = new ArrayList<>();

        time_lst.setHasFixedSize(true);
        time_lst.setLayoutManager(new LinearLayoutManager(con));

        for (int i = 1; i < 31; i++) {
            timelst.add(new SingleDO(i, false));
        }

        Single_txt_Adap adapter = new Single_txt_Adap(con, timelst);
        time_lst.setAdapter(adapter);

        dia_send_deli_time.findViewById(R.id.dia_confirm_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendTimeTask(oid);
            }
        });
    }

    public void SendTimeTask(String oid) {
        // http://infograins.com/INFO01/yalla/restaurant_api.php?action=TimeToPrepare&
        // order_id=5a20017b337d9&time=20&user_id=11
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, "TimeToPrepare");
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ORDER_ID, oid);
            jo.put("time", Time_Str);
            utils.startProgress();
            rt = new ResponseTask(con, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(con.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.Toast(j.getString("msg"), null);
                                dia_send_deli_time.dismiss();
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void AcceptReject_OrderTask(final String oid, final String status, final LinearLayout ll,String reason) {

//infograins.com/INFO01/yalla/restaurant_api.php?action=AcceptRejectOrder&
        // user_id=11&order_id=5a20017b337d9&verify_status=1&reject_why=driver%20not%20available%20for%20this%20location
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ACCPET_REJECT_ORDER);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ORDER_ID, oid);
            jo.put(Constant.STATUS, status);
            if (status.equals("0")) {
                jo.put(Constant.REJECTWHY, "");
            } else {
                jo.put(Constant.REJECTWHY, reason);
            }

            utils.startProgress();
            rt = new ResponseTask(con, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(con.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                if (status.equals("0")) {

                                    DialogSendPreparedTime(oid);
                                }else if(status.equals("1")){
                                    if (dia_reason_cancel.isShowing()) {
                                        dia_reason_cancel.dismiss();
                                    }
                                }
                                utils.Toast(jobj.getString("msg"), null);
                                ll.setVisibility(View.GONE);

                                ((OrderActivity)con).callAccount();
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void Cancel_OrderTask(final String oid, final LinearLayout ll, final Button can_req) {

//infograins.com/INFO01/yalla/restaurant_api.php?action=AcceptRejectOrder&
        // user_id=11&order_id=5a20017b337d9&verify_status=1&reject_why=driver%20not%20available%20for%20this%20location
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.CANCEL_ORDER);
            jo.put(Constant.ORDER_ID, oid);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));


            utils.startProgress();
            rt = new ResponseTask(con, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(con.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                utils.Toast(jobj.getString("msg"), null);
                                ll.setVisibility(View.VISIBLE);
                                can_req.setVisibility(View.GONE);
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void RequestDriverNow(String oid, final Button ll) {

        //  http://infograins.com/INFO01/yalla/restaurant_api.php?action=Request_driver&
        // order_id=17376&user_id=11
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.REQ_DRIVER_NOW);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ORDER_ID, oid);

            utils.startProgress();
            rt = new ResponseTask(con, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(con.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                utils.Toast(jobj.getString("msg"), null);
                                ll.setVisibility(View.GONE);
                            }
                        } catch (Exception je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public class Single_txt_Adap extends RecyclerView.Adapter<Single_txt_Adap.ViewHolder> {

        Context context;
        ArrayList<SingleDO> mlist;
        int selectedPosition = -1;
        ResponseTask rt;
        Utils utils;

        public Single_txt_Adap(Context con, ArrayList<SingleDO> mlist) {
            this.context = con;
            this.mlist = mlist;

            utils = new Utils(context);
        }


        @Override
        public Single_txt_Adap.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new Single_txt_Adap.ViewHolder(LayoutInflater.from(context).inflate(
                    R.layout.text_single_add, parent, false));
        }

        @Override
        public void onBindViewHolder(final Single_txt_Adap.ViewHolder holder, final int position) {

            holder.card.setText(String.valueOf(mlist.get(position).getTime()));

            holder.ll.setTag(position);

            if (selectedPosition == position) {
                holder.card.setBackground(context.getResources().getDrawable(R.drawable.circle_white_boarder));
                holder.min_tv.setVisibility(View.VISIBLE);
            } else {
                holder.card.setBackground(context.getResources().getDrawable(R.drawable.draw_trans));
                holder.min_tv.setVisibility(View.GONE);
            }

            holder.ll.setOnClickListener(new Single_txt_Adap.OnItemClick(position));

        }


        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public int getItemCount() {
            return mlist.size();
        }


        public class OnItemClick implements View.OnClickListener {
            int pos;

            public OnItemClick(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                selectedPosition = (Integer) v.getTag();
                notifyDataSetChanged();
                Time_Str = String.valueOf(mlist.get(pos).getTime());

            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView card, min_tv;
            RelativeLayout ll;

            public ViewHolder(View v) {
                super(v);
                card = (TextView) v.findViewById(R.id.card);
                min_tv = (TextView) v.findViewById(R.id.min_tv);
                ll = (RelativeLayout) v.findViewById(R.id.ll);
            }
        }

    }

    class ViewHolder {
        TextView textStatus, textCarColor, textCarModel, textDriverCall, textOrderId, textDriverName,
                txtCustName, txtCustCall, txtCustMeal, timer_autoreject,
                txtCust_tprice, cust_phone_txt,order_deli_time;
        CircleImageView imageView, order_cust_img;
        LinearLayout layoutTrack, driver_detail_ll, accpt_reject_ll;
        Button accept, reject, req_driver_now, layout_cancel_req;
        ImageView cus_call_icon, driver_call_icon;

        public ViewHolder(View b) {
            imageView = (CircleImageView) b.findViewById(R.id.order_driver_img);
            order_cust_img = (CircleImageView) b.findViewById(R.id.order_cust_img);
            textOrderId = (TextView) b.findViewById(R.id.order_id);
            textDriverName = (TextView) b.findViewById(R.id.order_driver_name);
            order_deli_time = (TextView) b.findViewById(R.id.order_deli_time);
            txtCustName = (TextView) b.findViewById(R.id.order_cust_name);
            txtCustCall = (TextView) b.findViewById(R.id.order_cust_phone);
            txtCust_tprice = (TextView) b.findViewById(R.id.order_total_price);
            txtCustMeal = (TextView) b.findViewById(R.id.order_meal);
            textDriverCall = (TextView) b.findViewById(R.id.order_phone);
            textCarModel = (TextView) b.findViewById(R.id.order_car_model);
            timer_autoreject = (TextView) b.findViewById(R.id.timer_autoreject);
            cust_phone_txt = (TextView) b.findViewById(R.id.cust_phone_txt);
            textCarColor = (TextView) b.findViewById(R.id.order_car_color);
            textStatus = (TextView) b.findViewById(R.id.order_driver_status);
            layoutTrack = (LinearLayout) b.findViewById(R.id.layout_track);
            accpt_reject_ll = (LinearLayout) b.findViewById(R.id.accpt_reject_ll);
            driver_detail_ll = (LinearLayout) b.findViewById(R.id.driver_detail_ll);
            reject = (Button) b.findViewById(R.id.reject);
            accept = (Button) b.findViewById(R.id.accept);
            req_driver_now = (Button) b.findViewById(R.id.req_driver_now);
            layout_cancel_req = (Button) b.findViewById(R.id.layout_cancel_req);
            driver_call_icon = (ImageView) b.findViewById(R.id.call_icon_driver);
            cus_call_icon = (ImageView) b.findViewById(R.id.cus_call_icon);
        }
    }

}
