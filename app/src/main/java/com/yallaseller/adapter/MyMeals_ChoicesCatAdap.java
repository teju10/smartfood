package com.yallaseller.adapter;

import android.app.Dialog;
import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.model.MyMealsChild;
import com.yallaseller.model.MyMealsGroup;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by and-04 on 29/11/17.
 */

public class MyMeals_ChoicesCatAdap extends BaseExpandableListAdapter {

    private static final String TAG = MyMealsAdapter.class.getSimpleName();
    String cat_id, cat_name;
    private Context mContext;
    private List<MyMealsGroup> groupList;
    private List<MyMealsChild> childdata;
    private ResponseTask rt;
    private Utils utils;
    private Dialog addchoice_subcat, edit_dia;
    private EditText edit_child_item_name, edit_child_price;

    public MyMeals_ChoicesCatAdap(Context context, List<MyMealsGroup> groupList, List<MyMealsChild> childdata) {
        this.mContext = context;
        this.groupList = groupList;
        this.childdata = childdata;
        utils = new Utils(mContext);
    }


    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = 0;
        if (groupList.get(groupPosition).getItems() != null)
            size = groupList.get(groupPosition).getItems().size();
        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupList.get(groupPosition).getItems().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.mealschoices_header_item, null);
        }

        ((TextView) convertView.findViewById(R.id.lblListHeader)).setText(groupList.get(groupPosition).getHeadName());

        ((ImageView) convertView.findViewById(R.id.add_choices)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddCatgoryDialog(groupList.get(groupPosition).getId(),
                        groupList.get(groupPosition).getHeadName());
            }
        });

        ((ImageView) convertView.findViewById(R.id.down))
                .setBackgroundResource(isExpanded ? R.drawable.expand_up : R.drawable.expand_down);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final MyMealsChild childText = (MyMealsChild) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.my_meals_child_itemlay, null);
        }

        final TextView my_meal_choicestype = (TextView) convertView.findViewById(R.id.my_meal_choicestype);
        final TextView my_meal_price = (TextView) convertView.findViewById(R.id.my_meal_price);

        my_meal_price.setText(childText.getPrice() + " JD");

        my_meal_choicestype.setText(childText.getProName());

        ((Button) convertView.findViewById(R.id.btn_edt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditCatgoryDialog(childText.getId(), childText.getProName(), childText.getPrice(),
                        groupPosition,childPosition);
            }
        });

        ((ImageView) convertView.findViewById(R.id.delete_cat_item)).setOnClickListener(new DeleteItem(childText.getId(), groupPosition, childPosition));

        /*if(childPosition==getChildrenCount(groupPosition)-1){
        if (convertView == null) {
                convertView = inflater.inflate(R.layout.child_footer,null);
                ImageView txtFooter = (ImageView) convertView.findViewById(R.id.txtFooter);
                Glide.with(mContext).load(mContext.getResources().getDrawable(R.drawable.ic_plus)).into(txtFooter);
        }*/

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void AddCatgoryDialog(final int cid, String headname) {

        addchoice_subcat = new Dialog(mContext, R.style.MyDialog);
        addchoice_subcat.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addchoice_subcat.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        addchoice_subcat.setContentView(R.layout.dialog_add_mealschoices_subcat);
        addchoice_subcat.setCancelable(false);
        addchoice_subcat.show();

        TextView child_item_name = (TextView) addchoice_subcat.findViewById(R.id.child_item_name);
        final EditText child_sub_item = (EditText) addchoice_subcat.findViewById(R.id.child_sub_item);
        final EditText child_price = (EditText) addchoice_subcat.findViewById(R.id.child_price);

        child_sub_item.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        child_item_name.setText(headname);

        addchoice_subcat.findViewById(R.id.submit_cat_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (child_sub_item.getText().toString().equals("")) {
                    utils.Toast(mContext.getResources().getString(R.string.sub_item_msg), null);
                } else {
                    SendSubChoiceCat(cid, child_sub_item.getText().toString(),
                            child_price.getText().toString());
                }
            }
        });

        addchoice_subcat.findViewById(R.id.close_dia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addchoice_subcat != null && addchoice_subcat.isShowing()) {
                    addchoice_subcat.cancel();
                }
            }
        });

    }

    public void EditCatgoryDialog(final int cid, String childname, String price,final int group_pos, final int child_pos) {

        edit_dia = new Dialog(mContext, R.style.MyDialog);
        edit_dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        edit_dia.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        edit_dia.setContentView(R.layout.dialog_editmeals_choicechild);
        edit_dia.setCancelable(false);
        edit_dia.show();

        edit_child_item_name = (EditText) edit_dia.findViewById(R.id.edit_child_item_name);
        edit_child_price = (EditText) edit_dia.findViewById(R.id.edit_child_price);
        edit_child_item_name.setText(childname);

        edit_child_price.setText(price);

        edit_dia.findViewById(R.id.submit_edited_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_child_item_name.getText().toString().equals("")) {
                    utils.Toast(mContext.getResources().getString(R.string.sub_item_msg), null);
                } else if (edit_child_price.getText().toString().equals("")) {
                    utils.Toast(mContext.getResources().getString(R.string.price_item_msg), null);
                } else {
                    EditSubChoices(cid, edit_child_item_name.getText().toString(), edit_child_price.getText().toString(),
                            group_pos,child_pos);
                }
            }
        });

        edit_dia.findViewById(R.id.close_dia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_dia != null && edit_dia.isShowing()) {
                    edit_dia.cancel();
                }
            }
        });
    }

    public void SendSubChoiceCat(int cid, String subcat_type, String price) {
        //http://infograins.com/INFO01/yalla/restaurant_api.php?action=AddExtratoSubItem&
        // user_id=11&subitem_id=52&add_optional=choice%20of%20drink&price=20
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ADDCATOFCHOICES);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.SUBITEMID, cid);
            jo.put("add_optional", subcat_type);
            jo.put("price", price);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.hideKeyboard(mContext);

                                if (addchoice_subcat.isShowing()) {
                                    addchoice_subcat.dismiss();
                                }
                                utils.Toast(mContext.getResources().getString(R.string.cat_send), null);
                                notifyDataSetChanged();
                            } else {

                            }
                        } catch (JSONException jsex) {
                            jsex.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException jexc) {
            jexc.printStackTrace();
        }
    }

    public void EditSubChoices(int cid, String subcat_name, String price,final int g_pos,final int c_pos) {
        //http://infograins.com/INFO01/yalla/restaurant_api.php?action=Edit_SubitemExtra&
        // subitem_id=2&add_optional=lassi&price=50&item_id=2
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.EDIT_SUBMEALSCHOICES);
            jo.put("item_id", cid);
            jo.put("add_optional", subcat_name);
            jo.put("price", price);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                utils.hideKeyboard(mContext);

                                if (edit_dia.isShowing()) {
                                    edit_dia.dismiss();
                                }
                                utils.Toast(mContext.getResources().getString(R.string.cat_edti_success), null);
                                groupList.get(g_pos).getItems().get(c_pos).setProName(j.getString("add_optional"));
                                groupList.get(g_pos).getItems().get(c_pos).setPrice(j.getString("price"));
                                notifyDataSetChanged();
                            } else {

                            }
                        } catch (JSONException jsex) {
                            jsex.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException jexc) {
            jexc.printStackTrace();
        }
    }

    public void DeleteSubCat(int catid, final int pos, final int groupPosition) {
        //http://infograins.com/INFO01/yalla/restaurant_api.php?action=delete_extra_subitem&id=2
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.DELETECHOICESSUBCAT);
            jo.put(Constant.CAT_ID, catid);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                MyMealsChild childText = groupList.get(groupPosition).getItems().get(pos);
                                Log.e(TAG, "onClick =====> " + childText.toString());
                                groupList.get(groupPosition).getItems().remove(childText);
                                notifyDataSetChanged();
                                utils.Toast(mContext.getResources().getString(R.string.cat_delete_success), null);
                            } else {

                            }
                        } catch (JSONException jsex) {
                            jsex.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public class DeleteItem implements View.OnClickListener {
        int group_pos, child_pos, id;

        DeleteItem(int id, int group_pos, int childPosition) {
            this.group_pos = group_pos;
            this.child_pos = childPosition;
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            DeleteSubCat(id, child_pos, group_pos);
          /*  MyMealsChild childText = groupList.get(group_pos).getItems().get(child_pos);
            groupList.get(group_pos).getItems().remove(child_pos);
            notifyDataSetChanged();*/

        }
    }

}
