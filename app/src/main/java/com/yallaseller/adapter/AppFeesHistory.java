package com.yallaseller.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 5/12/17.
 */

public class AppFeesHistory extends RecyclerView.Adapter<AppFeesHistory.ViewHolder> {

    Context mContext;
    ArrayList<JSONObject> mlist;
    LayoutInflater li;

    public AppFeesHistory(Context c, ArrayList<JSONObject> jobj) {
        this.mContext = c;
        this.mlist = jobj;
//        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_mywallet_appfees_item, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

            JSONObject object = mlist.get(position);
            holder.mw_cusname.setText(object.getString("first_name"));
            holder.mw_drivername.setText(object.getString("driver_name"));
            holder.mw_time.setText(object.getString("time"));
            holder.mw_order_no.setText(object.getString("order_id"));
            holder.mw_totalprice.setText(object.getString("total_price"));
            holder.mw_appfee.setText(object.getString("app_fees")+" JD");

            String cusdate = Utility.ChangeDateFormat("yyyy-MM-dd", "dd-M-yyyy", object.getString("date"));
            String driver_dte = Utility.ChangeDateFormat("yyyy-MM-dd", "dd-M-yyyy", object.getString("driver_time"));

            holder.mw_date.setText(cusdate);
            holder.mw_delivry_date.setText(driver_dte);

        } catch (Exception je) {
            je.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mw_cusname, mw_date, mw_time, mw_drivername, mw_delivry_date, mw_order_no, mw_totalprice, mw_appfee;

        public ViewHolder(View b) {
            super(b);
            mw_cusname = (TextView) b.findViewById(R.id.mw_cusname);
            mw_date = (TextView) b.findViewById(R.id.mw_date);
            mw_time = (TextView) b.findViewById(R.id.mw_time);
            mw_drivername = (TextView) b.findViewById(R.id.mw_drivername);
            mw_delivry_date = (TextView) b.findViewById(R.id.mw_delivry_date);
            mw_order_no = (TextView) b.findViewById(R.id.mw_order_no);
            mw_totalprice = (TextView) b.findViewById(R.id.mw_totalprice);
            mw_appfee = (TextView) b.findViewById(R.id.mw_appfee);

        }
    }
}
