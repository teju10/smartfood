package com.yallaseller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yallaseller.R;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 2/12/17.
 */

public class RateReviewAdap extends BaseAdapter {

    Context mContext;
    ArrayList<JSONObject> mlist;
    LayoutInflater li;

    public RateReviewAdap(Context con, ArrayList<JSONObject> list) {
        this.mContext = con;
        this.mlist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder v;

        if (convertView == null) {
            convertView = li.inflate(R.layout.list_rate_review_item, null);
            v = new ViewHolder(convertView);
            convertView.setTag(v);
        } else {
            v = (ViewHolder) convertView.getTag();
        }
        try {
            JSONObject object = mlist.get(position);
            v.review_cusname.setText(object.getString("fullname"));
//            v.item_review_date.setText(object.getString("dose_frequancy"));
            v.item_review_rate.setText(object.getString("rate"));
            v.item_review.setText(object.getString("review"));

//            v.dos_item_unit_item.setText(object.getString("time_unit"));

//            convertView.setOnClickListener(new ItemClick(position));
            float rate = Float.parseFloat(object.getString("rate"));
            if (rate >= 0 && rate < 0.5) {
                v.star1_rat.setImageResource(R.drawable.star_orange_empty);
                v.star2_rat.setImageResource(R.drawable.star_orange_empty);
                v.star3_rat.setImageResource(R.drawable.star_orange_empty);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);

            } else if (rate >= 0.5 && rate < 1) {
                v.star1_rat.setImageResource(R.drawable.star_half_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange_empty);
                v.star3_rat.setImageResource(R.drawable.star_orange_empty);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);

            } else if (rate >= 1 && rate < 1.5) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange_empty);
                v.star3_rat.setImageResource(R.drawable.star_orange_empty);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 1.5 && rate < 2) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_half_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange_empty);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 2 && rate < 2.5) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange_empty);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 2.5 && rate < 3) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_half_orange);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 3 && rate < 3.5) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange);
                v.star4_rat.setImageResource(R.drawable.star_orange_empty);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 3.5 && rate < 4) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange);
                v.star4_rat.setImageResource(R.drawable.star_half_orange);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 4 && rate < 4.5) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange);
                v.star4_rat.setImageResource(R.drawable.star_orange);
                v.star5_rat.setImageResource(R.drawable.star_orange_empty);
            } else if (rate >= 4.5 && rate < 5) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange);
                v.star4_rat.setImageResource(R.drawable.star_orange);
                v.star5_rat.setImageResource(R.drawable.star_half_orange);
            } else if (rate == 5) {
                v.star1_rat.setImageResource(R.drawable.star_orange);
                v.star2_rat.setImageResource(R.drawable.star_orange);
                v.star3_rat.setImageResource(R.drawable.star_orange);
                v.star4_rat.setImageResource(R.drawable.star_orange);
                v.star5_rat.setImageResource(R.drawable.star_orange);
            }

        } catch (Exception j) {
            j.printStackTrace();
        }
        return convertView;
    }


    public class ViewHolder {
        TextView review_cusname, item_review_date, item_review_rate, item_review;
        ImageView star1_rat, star2_rat, star3_rat, star4_rat, star5_rat;

        public ViewHolder(View b) {
            review_cusname = (TextView) b.findViewById(R.id.item_review_cusname);
            item_review_date = (TextView) b.findViewById(R.id.item_review_date);
            item_review_rate = (TextView) b.findViewById(R.id.item_review_rate);
            item_review = (TextView) b.findViewById(R.id.item_review);

            star1_rat = (ImageView) b.findViewById(R.id.star1_rat);
            star2_rat = (ImageView) b.findViewById(R.id.star2_rat);
            star3_rat = (ImageView) b.findViewById(R.id.star3_rat);
            star4_rat = (ImageView) b.findViewById(R.id.star4_rat);
            star5_rat = (ImageView) b.findViewById(R.id.star5_rat);
        }
    }

}
