package com.yallaseller.utility;

public class Constant {

    public static String PREF_ID = "userId";
    public static String PREF_NAME = "name";
    public static String PREF_STORE = "store";
    public static String PREF_BRANCH = "branch";
    public static String PREF_EMAIL = "email";
    public static String PREF_CITY = "city";
    public static String PREF_PHONE = "phone";
    public static String PREF_IMAGE = "image";
    public static String PREF_COUNTRY = "country";
    public static String PREF_NOTI_COUNT = "notiCount";
    public static String PREF_SAVE_ADD = "savedadd";

    /*-----------------APIS------------------*/

    public static String GETCATORYLIST = "list_category";
    public static String GETSINGLEITEM = "GetItemById";
    public static String ADDMYMEALSITEM = "AddMenuItems";
    public static String EDIT_MYMEALSITEM = "update_item";
    public static String EDIT_SUBMEALSCHOICES= "Edit_SubitemExtra";
    public static String ADDCATOFCHOICES = "AddExtratoSubItem";
    public static String DELETEITEM = "delete_item";
    public static String DELETECHOICESSUBCAT = "delete_extra_subitem";
    public static  String ADDMENU_IMAGE="AddMenuImage";
    public static  String GETRESTAURANTWALLET="GetRestaurantEarning";
    public static final String GETMYMEALS = "GetMyMeals";
    public static final String ADDCHOICES = "AddSubItem";
    public static final String GETCHOICESLIST = "GetChoicesById";
    public static final String GETCUSTOMERDETAIL= "push_order";
    public static final String ADD_DISCOUNT = "Update_discount";
    public static final String GETREVIEWRATE = "GetReviewByRestaurantId";
    public static final String APPFEES = "MyWallet_AppFees";
    public static final String ACCPET_REJECT_ORDER = "AcceptRejectOrder";
    public static final String CANCEL_ORDER = "RestaurantCancleOrder";
    public static final String REQ_DRIVER_NOW = "Request_driver";
    public static final String COUNT = "MsgCount";


    public static String isLangSet = "isLangSet";
    public static String LangLocale = "localeLang";

    public static String PREF_CURRENT_LAT = "curLatitude";
    public static String PREF_CURRENT_LONG = "curLongitude";

    public static String PREF_SAVED_LAT = "savedLatitude";
    public static String PREF_SAVED_LONG = "savedLongitude";
    public static final String TOTAL_SAV= "TOTAL_SAV";
    public static final String LANG = "LANG";
    public static final String MYMEALSOBJ = "MYMEALSOBJ";

    public static final String ACTION = "action";
    public static final String SERVER_MSG = "msg";
    public static final String USER_LOGOUT = "User_Logout";
    public static final String USERID = "user_id";
    public static final String OBJECT = "object";
    public static final String SPLASHCK = "SPLASHCK";

    /*----------------SERVERS KEYS---------------------*/
    public static final String CAT_ID = "id";
    public static final String CATAGORYNAME = "item_category";
    public static final String CAT_TYPE = "item_name";
    public static final String CAT_SUBTYPE = "item_title";

    public static final String PRICE = "item_price";

    public static final String LOGOUT = "Logout";

    public static final String MENUID = "menu_id";
    public static final String MENUNAME = "menu_name";
    public static final String MENUSUBNAME = "menu_sub_name";
    public static final String DIS_PRICE = "menu_discount_price";
    public static final String DIS_PERCENT= "discount_percentage";
    public static final String DIS_STATUS= "discount_status";

    public static final String MENUPRICE = "menu_price";
    public static final String ITEMID = "item_id";
    public static final String MENUTITLE = "menu_title";
    public static final String SUBITEMNAME = "subitem_name";
    public static final String SUBITEMID= "subitem_id";
    public static final String FCMID= "token";
    public static final String NOTITYPE= "notitype";
    public static final String ORDER_ID= "order_id";
    public static final String DRIVER_ID= "driver_id";
    public static final String NEWUSER= "new_user";
    public static final String STATUS= "verify_status";
    public static final String REJECTWHY= "reject_why";

    public static final String API_KEY= "AIzaSyBkGMcMpSHGW4Ic3V0kR0-WxIXCRSGPZOo";


}
