package com.yallaseller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;
import com.yallaseller.activity.AboutusActivity;
import com.yallaseller.activity.LoginActivity;
import com.yallaseller.activity.My_RestaurantActivity;
import com.yallaseller.activity.MyaccountActivity;
import com.yallaseller.activity.NotificationListActivity;
import com.yallaseller.activity.OrderActivity;
import com.yallaseller.activity.SupportActivity;
import com.yallaseller.fragment.HomeFragment;
import com.yallaseller.notificationBadge.NotificationBadge;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class SellerDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleApiClient.ConnectionCallbacks {

    public static final String mBroadcastString = "mynotification";
    private static final String TAG = SellerDrawer.class.getSimpleName();
    public static double fusedLatitude = 0.0;
    public static double fusedLongitude = 0.0;
    public static String isopenHome;
    LinearLayout linearHome;
    Context mContext;
    String OrderID = "", Jobtype, U_TYPE = "", DriverId = "";
    Bundle bun;
    ResponseTask responseTask;
    private Utils utils;
    private GoogleApiClient locationGoogleAdiClient;
    private LocationRequest mLocationRequest;
    private android.support.v4.app.FragmentTransaction fragmentTransaction;
    private RelativeLayout layoutToolbar;
    private Toolbar toolbar;

    String count = "";
    private ImageView imgNoti;
    private DrawerLayout drawer;
   // private TextView textCounter;
    NotificationBadge textCounter;
    private IntentFilter mIntentFilter;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastString)) {
                count = Utility.getSharedPreferences(context, Constant.COUNT);
                System.out.println("GETCOUNT------>>>" + count);
                invalidateOptionsMenu();
                SetBadgeCount();
            }
        }
    };

    public static double getFusedLatitude() {
        return fusedLatitude;
    }

    public void setFusedLatitude(double lat) {
        fusedLatitude = lat;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.seller_drawer, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public static double getFusedLongitude() {
        return fusedLongitude;
    }

    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constant.LANG));

        setContentView(R.layout.activity_seller_drawer);

        mContext = this;
        utils = new Utils(this);

        try {
            bun = new Bundle();
            bun = getIntent().getExtras();
            OrderID = bun.getString(Constant.ORDER_ID);
            Jobtype = bun.getString(Constant.NOTITYPE);

            if (Jobtype.equals("0")) {
                U_TYPE = bun.getString(Constant.NEWUSER);
            } else if (Jobtype.equals("5")) {
                DriverId = bun.getString(Constant.DRIVER_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        layoutToolbar = (RelativeLayout) findViewById(R.id.toolbar_rel);
        imgNoti = (ImageView) findViewById(R.id.img_noti);
        textCounter = (NotificationBadge) findViewById(R.id.noti_counter);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
//        toggle.setHomeAsUpIndicator(R.drawable.menu); //set your own
        toggle.syncState();

        ImageView menuToggle = (ImageView) findViewById(R.id.toogle_menu);

        isopenHome = "Hiiii";
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastString);
        count = Utility.getSharedPreferences(mContext, Constant.COUNT);
        try {
            if (Utility.getSharedPreferences(mContext, Constant.COUNT).equals("")) {
                textCounter.setText("0");
            } else {
                textCounter.setText(count);
            }
        } catch (Exception e) {
            textCounter.setText("0");
        }

        menuToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });


        imgNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SellerDrawer.this, NotificationListActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();
            }
        });

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//        View headerLayout = navigationView.getHeaderView(0);

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.head_img_store);
        TextView textStoreName = (TextView) findViewById(R.id.head_tv_store_name);
        TextView textLogout = (TextView) findViewById(R.id.head_store_logout);
        Picasso.with(this).load(utils.getPreference(Constant.PREF_IMAGE)).into(circleImageView);
//        textStoreName.setText(utils.getPreference(Constant.PREF_STORE));


        textLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogoutMethod();
            }
        });

        final TextView textEng = (TextView) findViewById(R.id.header_tv_eng);
        final TextView textArabic = (TextView) findViewById(R.id.header_tv_arabic);

        if (utils.getPreference(Constant.isLangSet).equals("ar")) {
            textArabic.setTextColor(getResources().getColor(R.color.colorPrimary));
            textArabic.setBackground(getResources().getDrawable(R.drawable.round_white));
        } else {
            textEng.setTextColor(getResources().getColor(R.color.colorPrimary));
            textEng.setBackground(getResources().getDrawable(R.drawable.round_white));
        }

        textEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String languageToLoad = "en";
                Utility.setIntegerSharedPreference(mContext, Constant.LANG, 1);

                Resources res = getResources();
                Configuration conf = res.getConfiguration();
                DisplayMetrics dm = res.getDisplayMetrics();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    conf.setLocale(new Locale(languageToLoad));
                    createConfigurationContext(conf);
                    res.updateConfiguration(conf, dm);
                } else {
                    conf.locale = new Locale(languageToLoad);
                    getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
                }
                utils.setPreference(Constant.isLangSet, languageToLoad);
                textEng.setTextColor(getResources().getColor(R.color.colorPrimary));
                textEng.setBackground(getResources().getDrawable(R.drawable.round_white));
                textArabic.setTextColor(getResources().getColor(R.color.colorAccent));
                textArabic.setBackground(null);

                startActivity(new Intent(mContext, SellerDrawer.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();

//                onBackPressed();
                linearHome.performClick();

            }
        });

        textArabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String languageToLoad = "ar";
                Utility.setIntegerSharedPreference(mContext, Constant.LANG, 2);
                Resources res = getResources();
                Configuration conf = res.getConfiguration();
                DisplayMetrics dm = res.getDisplayMetrics();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    conf.setLocale(new Locale(languageToLoad));
                    createConfigurationContext(conf);
                    res.updateConfiguration(conf, dm);
                } else {
                    conf.locale = new Locale(languageToLoad);
                    getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
                }
                utils.setPreference(Constant.isLangSet, languageToLoad);
                textArabic.setTextColor(getResources().getColor(R.color.colorPrimary));
                textArabic.setBackground(getResources().getDrawable(R.drawable.round_white));
                textEng.setTextColor(getResources().getColor(R.color.colorAccent));
                textEng.setBackground(null);

                startActivity(new Intent(mContext, SellerDrawer.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();

//                onBackPressed();
                linearHome.performClick();
            }
        });

        final TextView textAbout = (TextView) findViewById(R.id.header_tv_about);
        final TextView textSupport = (TextView) findViewById(R.id.header_tv_support);
        LinearLayout layoutAccount = (LinearLayout) findViewById(R.id.layout_myaccount);
        LinearLayout layoutOrder = (LinearLayout) findViewById(R.id.layout_order);
        LinearLayout layout_my_resturant = (LinearLayout) findViewById(R.id.layout_my_resturant);
        linearHome = (LinearLayout) findViewById(R.id.header_home);

        layoutOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SellerDrawer.this, OrderActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();
            }
        });

        layout_my_resturant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SellerDrawer.this, My_RestaurantActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();
            }
        });

        layoutAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SellerDrawer.this, MyaccountActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();
            }
        });

        textAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(SellerDrawer.this, AboutusActivity.class));
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    finish();
//                    layoutToolbar.setVisibility(View.GONE);
//                    toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//                    toolbar.setVisibility(View.GONE);
//                    toolbar.setTitle("");
//                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                    // Handle navigation view item clicks here.
//                    Fragment fragment = null;
//                    fragment = new AboutFragment();
//                    if (fragment != null) {
//                        fragmentTransaction.replace(R.id.content_main, fragment);
//                        fragmentTransaction.commit();
//                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                        drawer.closeDrawer(GravityCompat.START);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        textSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(SellerDrawer.this, SupportActivity.class));
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    finish();
//                    layoutToolbar.setVisibility(View.GONE);
//                    toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//                    toolbar.setVisibility(View.GONE);
//                    toolbar.setTitle("");
//                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                    // Handle navigation view item clicks here.
//                    Fragment fragment = null;
//                    fragment = new SupportFragment();
//                    if (fragment != null) {
//                        fragmentTransaction.replace(R.id.content_main, fragment);
//                        fragmentTransaction.commit();
//                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                        drawer.closeDrawer(GravityCompat.START);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        linearHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    layoutToolbar.setVisibility(View.VISIBLE);
                    toolbar.setBackground(null);
                    toolbar.setVisibility(View.VISIBLE);
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    // Handle navigation view item clicks here.
                    Bundle b = new Bundle();
                    Fragment fragment = null;
                   /* fragment = new HomeFragment();
                    b.putString(Constant.ORDER_ID, OrderID);
                    b.putString(Constant.NOTITYPE, Jobtype);
                    fragment.setArguments(b);*/
                    if (fragment != null) {
                        fragmentTransaction.replace(R.id.content_main, fragment);
                        fragmentTransaction.commit();
                        drawer.closeDrawer(GravityCompat.START);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.content_main, new HomeFragment());
//        ft.commit();

        Fragment fragment = new HomeFragment();
        Bundle b = new Bundle();

       /* utils.setPreference(Constant.ORDER_ID, OrderID);
        utils.setPreference(Constant.NOTITYPE, Jobtype);

        try {
            if (Jobtype.equals("0")) {
                utils.setPreference(Constant.NEWUSER, U_TYPE);
            } else {
                utils.setPreference(Constant.DRIVER_ID, DriverId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
        try {
            fragment = new HomeFragment();
            b.putString(Constant.ORDER_ID, OrderID);
            b.putString(Constant.NOTITYPE, Jobtype);

            if (Jobtype.equals("0")) {
                b.putString(Constant.NEWUSER, U_TYPE);
            } else if (Jobtype.equals("5")) {
                b.putString(Constant.DRIVER_ID, DriverId);
            }

            fragment.setArguments(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment != null) {
            fragmentTransaction.replace(R.id.content_main, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
            try {
                layoutToolbar.setVisibility(View.VISIBLE);
                toolbar.setBackground(null);
                toolbar.setVisibility(View.VISIBLE);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                // Handle navigation view item clicks here.
                Fragment fragment = null;
                fragment = new HomeFragment();
                if (fragment != null) {
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
//        fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//        Fragment fragment = null;
//        if (id == R.id.nav_home) {
//            fragment = new HomeFragment();
//        } else if (id == R.id.nav_account) {
//            // Handle the camera action
//        } else if (id == R.id.nav_order) {
//
//        } else if (id == R.id.nav_about) {
//            fragment = new AboutFragment();
//        } else if (id == R.id.nav_support) {
//            fragment = new SupportFragment();
//        }
//
//        if (fragment != null) {
//            fragmentTransaction.replace(R.id.content_main, fragment);
//            fragmentTransaction.commit();
//            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//            drawer.closeDrawer(GravityCompat.START);
//            return true;
//        } else {
//            Log.e("MainActivity", "Error in creating fragment");
//        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (utils.checkPlayServices()) {
            startFusedLocation();
            registerRequestUpdate(this);
        }
        registerReceiver(mReceiver, mIntentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopFusedLocation();
    }

    public void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(locationGoogleAdiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!isGoogleApiClientConnected()) {
                        locationGoogleAdiClient.connect();
                    }
                    registerRequestUpdate(listener);
                }
            }
        }, 1000);
    }

    public boolean isGoogleApiClientConnected() {
        return locationGoogleAdiClient != null && locationGoogleAdiClient.isConnected();
    }

    public void startFusedLocation() {
        if (locationGoogleAdiClient == null) {
            locationGoogleAdiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {

                        }
                    }).build();
            locationGoogleAdiClient.connect();
        } else {
            locationGoogleAdiClient.connect();
        }
    }

    public void stopFusedLocation() {
        if (locationGoogleAdiClient != null) {
            locationGoogleAdiClient.disconnect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());
        //System.out.println(location.getLatitude() + ":: location ::" + location.getLongitude());
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + locationGoogleAdiClient.isConnected());

        if (ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(locationGoogleAdiClient);
            // Note that this can be NULL if last location isn't already known.
           /* if (mCurrentLocation != null) {
//                lati = mCurrentLocation.getLatitude();
//                longii = mCurrentLocation.getLongitude();
                Log.d("DEBUG", "current location: " + mCurrentLocation.toString());
                if (Utility.isConnectingToInternet(mContext)) {
                    UpdateDriverLocation(lati, longii);
                } else {
                    Utility.showPromptDlgForError(mContext, mContext.getResources().getString(R.string.connection_error), mContext.getResources().getString(R.string.error_internet));
                }
            } else {
                startLocationUpdates();
            }*/
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void LogoutMethod() {
        //http://infograins.com/INFO01/yalla/restaurant_api.php?action=Logout&user_id=11
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constant.ACTION, Constant.LOGOUT);
            jsonObject.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            utils.startProgress();
            responseTask = new ResponseTask(mContext, jsonObject);

            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.server_fail));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equals("1")) {
                                Utility.clearsharedpreference(mContext);
                                utils.setPreference(Constant.PREF_ID, "");
                                utils.setIntPreference(Constant.LANG, 0);

                                Intent intent = new Intent(SellerDrawer.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString(Constant.SERVER_MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        isopenHome = null;
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    public void SetBadgeCount(){
        try {
            if (Utility.getSharedPreferences(mContext, Constant.COUNT).equals("")) {
                textCounter.setText("0");
            } else {
                textCounter.setText(count);
            }
        } catch (Exception e) {
            textCounter.setText("0");
        }
    }
}
