package com.yallaseller.serverinteraction;

import android.content.Context;
import android.os.AsyncTask;


import com.yallaseller.utility.Constant_Urls;
import com.yallaseller.utility.Utility;
import com.yallaseller.utility.Constant;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by Nilesh Kansal on 12-Jul-16.
 */
public class ResponseTask extends AsyncTask<String, String, String> {

    String result = "", serverURL;
    JSONObject param;
    ResponseListener mListener;

    public ResponseTask(Context appcontext, JSONObject param1) {
        if (Utility.getIngerSharedPreferences(appcontext, Constant.LANG) == 0) {
            serverURL = Constant_Urls.SERVER_URL;
        } else {
            serverURL = Constant_Urls.SERVER_URL;
        }
        param = param1;
    }

    public static String postParamsAndfindJSON(String url, JSONObject params) {
        try {
            URL myurl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) myurl.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            System.out.println("params" + params);
            writer.write(getParams(params));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream()));
                StringBuffer sb = new StringBuffer("");
                String line = "";
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                in.close();
                return sb.toString();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static String getParams(JSONObject parameter) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = parameter.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = parameter.get(key);
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
       /* StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = parameter.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Map.Entry<String, String> param = iterator.next();
            bodyBuilder.append(URLEncoder.encode(param.getKey(), "UTF-8")).append('=')
                    .append(URLEncoder.encode(param.getValue(), "UTF-8"));
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }*/
        return result.toString();
        //   return bodyBuilder.toString();
    }

    public static String getDataFromUrl(String url) {
        String result = null;
        try {
            URL myurl = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) myurl
                    .openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.connect();
            InputStream is = urlConnection.getInputStream();
            if (is != null) {
                StringBuilder sb = new StringBuilder();
                String line;
                try {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(is));
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    reader.close();
                } finally {
                    is.close();
                }
                result = sb.toString();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String url = "";
        try {
            url = serverURL + getParams(param);
            System.out.println("URL IS==========>>>>>>  " + url);
            String result = getDataFromUrl(url);
            return result;
        } catch (Exception e) {

        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("result" + result);
        try {
            returnResult(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListener(ResponseListener listener) {
        mListener = listener;
    }

    private void returnResult(String result) {

        if (mListener != null) {
            mListener.onGetPickSuccess(result);
        }
    }
}