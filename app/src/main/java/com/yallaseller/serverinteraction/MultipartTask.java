package com.yallaseller.serverinteraction;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

/**
 * Created by Nilesh Kansal on 05-Aug-16.
 */
public class MultipartTask extends AsyncTask<String, String, String> {

    String result = "", action = "", TAG = "", method = "", fileName = "", fileParam = "";
    Context mContext;
    JSONObject params;
    Activity activity;
    MultipartResponseListener mListener;

    public MultipartTask(Activity activity, JSONObject parameters, String URL_ACTION, String FileParam, String FileName,
                         String TAG, String METHOD) {
        this.activity = activity;
        this.params = parameters;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            if (method.equalsIgnoreCase("file")) {
                MultipartUtility mu;
                mu = new MultipartUtility("action=" + action, "UTF-8");

                if (fileName != null && !fileName.equals("")) {
                    mu.addFilePart(fileParam, fileName);
                }
                result = mu.finish();
            } else {
                MultipartUtility mu;
                mu = new MultipartUtility("http://infograins.com/INFO01/yalla/restaurant_api.php?"+"action=" + action, "UTF-8");
                Iterator<String> keys = params.keys();
                while (keys.hasNext()) {
                    try {
                        String id = String.valueOf(keys.next());
                        mu.addFormField(id, "" + params.get(id));
                        System.out.println(id + " : " + params.get(id));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (fileName != null && !fileName.equals("")) {
                    mu.addFilePart(fileParam, fileName);
                }
                result = mu.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e(TAG, "result ===== " + s);
        try {
            returnResult(s);
        } catch (Exception e) {
            e.printStackTrace();
            returnResult(null);
        }
    }

    public void setListener(MultipartResponseListener listener) {
        mListener = listener;
    }

    private void returnResult(String result) {
        if (mListener != null) {
            mListener.onPickSuccess(result);
        }
    }

    public class MultipartUtility {
        private static final String LINE_FEED = "\r\n";
        private final String boundary;
        private HttpURLConnection httpConn;
        private String charset;
        private OutputStream outputStream;
        private PrintWriter writer;

        public MultipartUtility(String requestURL, String charset) throws IOException {

            Log.e("MultipartUtility", requestURL);
            this.charset = charset;

            // creates a unique boundary based on time stamp
            boundary = "===" + System.currentTimeMillis() + "===";

            URL url = new URL(requestURL);
            httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setDoOutput(true);    // indicates POST method
            httpConn.setDoInput(true);
            httpConn.setConnectTimeout(20000);
            httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            outputStream = httpConn.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
        }

        public void addFormField(String name, String value) {
            Log.e(TAG, "addFormField ====== " + name + " = " + value);
            writer.append("--" + boundary).append(LINE_FEED);
            writer.append("Content-Disposition: form-data; name=\"" + name + "\"").append(LINE_FEED);
            writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
            writer.append(LINE_FEED);
            writer.append(value).append(LINE_FEED);
            writer.flush();
        }


        public void addFilePart(String fieldName, String filename) throws IOException {
            String fileName = filename;
            Log.e(TAG, "addFilePart ====== " + fieldName + " = " + filename);
            writer.append("--" + boundary).append(LINE_FEED);
            writer.append("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"")
                    .append(LINE_FEED);
            writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName)).append(LINE_FEED);
            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
            writer.append(LINE_FEED);
            writer.flush();
            FileInputStream inputStream = new FileInputStream(filename);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
            inputStream.close();
            writer.append(LINE_FEED);
            writer.flush();
        }

        public String finish() throws IOException {
            StringBuilder result = new StringBuilder();
            String response = null;
            Log.e("finish()", "finish()");
            writer.append(LINE_FEED).flush();
            writer.append("--" + boundary + "--").append(LINE_FEED);
            writer.close();

            // checks server's status code first
            int status = httpConn.getResponseCode();

            System.out.println("response from multipart class......................"+status);

            if (status == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                    Log.e("response", result.toString());
                }
                response = result.toString();
                reader.close();
                httpConn.disconnect();
            } else {
                Log.e("HTTP-ERROR", httpConn.toString());
                throw new IOException("Server returned non-OK status: " + status);
            }
            return response;
        }
    }
}