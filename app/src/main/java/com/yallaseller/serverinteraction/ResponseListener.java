package com.yallaseller.serverinteraction;

/**
 * Created by LakhanPatidar on 01-Aug-16.
 */
public interface ResponseListener {
    void onGetPickSuccess(String result);
}
