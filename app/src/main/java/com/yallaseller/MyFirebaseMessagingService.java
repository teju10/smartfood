package com.yallaseller;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.yallaseller.activity.OrderActivity;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;

import java.util.Map;

/**
 * Created by vivek on 1/16/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
//    Utils utils;

    private void updateMyActivity(Context context) {
        Intent intent = new Intent("notification");
        intent.putExtra("message", "true");
        context.sendBroadcast(intent);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        utils = new Utils(getApplication());
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> notification = remoteMessage.getData();
            Bundle bundle = new Bundle();
            for (Map.Entry<String, String> entry : notification.entrySet()) {
                bundle.putString(entry.getKey(), entry.getValue());
            }

            try {
               /* Utils utils = new Utils(this, "");
                if (!utils.getPreference(Constant.PREF_ID).equals("")) {
                    int count = utils.getIntPreference(Constant.PREF_NOTI_COUNT);
                    count++;
                    utils.setIntPreference(Constant.PREF_NOTI_COUNT, count);
                    sendNotification(remoteMessage.getData().get("message").toString(), "0");
                }*/
                if (SellerDrawer.isopenHome != null) {
                        Utility.setSharedPreference(getApplicationContext(), Constant.COUNT, (notification.get("MsgCount")));
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(SellerDrawer.mBroadcastString);
                        sendBroadcast(broadcastIntent);
                        System.out.println("mBroadcastString--->>>" + Utility.getSharedPreferences(getApplicationContext(), Constant.COUNT));

                }
               // Utility.setSharedPreference(getApplicationContext(), Constant.COUNT, (notification.get("MsgCount")));
                if (notification.get("notitype").contains("0")) {
                    sendNotification(notification.get("notification"), bundle);
                } else if (notification.get("notitype").contains("2")) {
                    sendNotification(notification.get("notification"), bundle);
                } else if (notification.get("notitype").contains("3")) {
                    sendNotification(notification.get("notification"), bundle);
                }else if (notification.get("notitype").contains("4")) {
                    sendNotification(notification.get("notification"), bundle);
                }else if (notification.get("notitype").contains("6")) {
                    sendNotification(notification.get("notification"), bundle);
                } else if (notification.get("notitype").contains("8")) {
                    sendNotification(notification.get("notification"), bundle);
                }else if (notification.get("notitype").contains("9")) {
                    sendNotification(notification.get("notification"), bundle);
                } else if (notification.get("notitype").contains("5")) {
                    sendNotification(notification.get("notification"), bundle);

                   /* Bundle b = new Bundle();
                    b.putString(Constant.NOTITYPE, notification.get("notitype"));
                    b.putString(Constant.ORDER_ID, notification.get("order_id"));
                    b.putString(Constant.DRIVER_ID, notification.get("driver_id"));

                    Intent i = new Intent(getApplicationContext(), SellerDrawer.class);
                    i.putExtras(b);
                    i.setFlags(i.FLAG_ACTIVITY_NEW_TASK);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, i,
                            PendingIntent.FLAG_ONE_SHOT);
                    try {
                        pendingIntent.send();
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }*/
                }
                if (remoteMessage.getNotification() != null) {
                    Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNotification(String messageBody, Bundle flag) {

        Intent i = new Intent();

        if (flag.getString(Constant.NOTITYPE).contains("0")) {
            flag.putString(Constant.NOTITYPE, flag.getString(Constant.NOTITYPE));
            flag.putString(Constant.ORDER_ID, flag.getString("order_id"));
            flag.putString(Constant.NEWUSER, flag.getString("new_user"));
            i = new Intent(this, SellerDrawer.class);
        } else if (flag.getString(Constant.NOTITYPE).contains("2")) {
            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
        } else if (flag.getString(Constant.NOTITYPE).contains("3")) {
            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
        }else if (flag.getString(Constant.NOTITYPE).contains("4")) {
            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
        } else if (flag.getString(Constant.NOTITYPE).contains("6")) {
//            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
        }else if (flag.getString(Constant.NOTITYPE).contains("8")) {
            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
        }else if (flag.getString(Constant.NOTITYPE).contains("9")) {
//            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
        }else if (flag.getString(Constant.NOTITYPE).contains("5")) {
            flag.putString(Constant.ORDER_ID, flag.getString("order_id"));
            flag.putString(Constant.NOTITYPE, flag.getString("notitype"));
            flag.putString(Constant.DRIVER_ID, flag.getString("driver_id"));
            i = new Intent(this, SellerDrawer.class);
        } else {
            i = new Intent(this, SellerDrawer.class);
        }
        //FLAG_ACTIVITY_NEW_TASK
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtras(flag);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }

}
