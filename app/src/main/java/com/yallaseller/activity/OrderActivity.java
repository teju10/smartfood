package com.yallaseller.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.adapter.OrderAdapter;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.MyOrderDO;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by vivek on 6/27/2017.
 */

public class OrderActivity extends AppCompatActivity {

    Context mContext;
    Toolbar tb;
    private Utils utils;
    private TextView textOntheWay, textCompleted;
    private TextView textTitleOnWay, textTitleComplete;
    private ListView listOrder;
    private ArrayList<MyOrderDO> listOrders;
    private OrderAdapter orderAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout layoutOnTheWay, layoutonComplete;
    private boolean flag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders);
        mContext = this;
        tb = (Toolbar) findViewById(R.id.res_toolbar);

        setSupportActionBar(tb);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((TextView) tb.findViewById(R.id.toolbar_title)).setText(mContext.getResources().getString(R.string.smart_f_rest));
        tb.setNavigationIcon(R.drawable.ic_drawer_back_lft);

        init();
        RunTimePermission();
        flag = true;

        layoutOnTheWay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = true;
                checkFlag();
                callAccount();
            }
        });

        layoutonComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = false;
                checkFlag();
                callAccount();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (utils.isNetConnected()) {
                    callAccount();
                }
            }
        });

        if (utils.isNetConnected()) {
            callAccount();
        }
    }

    private void checkFlag() {
        int white = getResources().getColor(R.color.colorAccent);
        int orange = getResources().getColor(R.color.colorPrimary);
        Drawable lay_bg_Orange = getResources().getDrawable(R.drawable.edt_orange);
        Drawable lay_bg_White = getResources().getDrawable(R.drawable.white_orange);
        Drawable roundOrange = getResources().getDrawable(R.drawable.round_orange);
        Drawable roundWhite = getResources().getDrawable(R.drawable.round_white);
        if (flag) {
            layoutonComplete.setBackgroundDrawable(lay_bg_White);
            textCompleted.setBackgroundDrawable(roundOrange);
            textCompleted.setTextColor(white);
            textTitleComplete.setTextColor(orange);

            layoutOnTheWay.setBackgroundDrawable(lay_bg_Orange);
            textOntheWay.setBackgroundDrawable(roundWhite);
            textOntheWay.setTextColor(orange);
            textTitleOnWay.setTextColor(white);
        } else {
            layoutOnTheWay.setBackgroundDrawable(lay_bg_White);
            textOntheWay.setBackgroundDrawable(roundOrange);
            textOntheWay.setTextColor(white);
            textTitleOnWay.setTextColor(orange);

            layoutonComplete.setBackgroundDrawable(lay_bg_Orange);
            textCompleted.setBackgroundDrawable(roundWhite);
            textCompleted.setTextColor(orange);
            textTitleComplete.setTextColor(white);
        }
    }

    private void init() {

        utils = new Utils(this);
        listOrder = (ListView) findViewById(R.id.order_list);
        textOntheWay = (TextView) findViewById(R.id.order_ontheway);
        textCompleted = (TextView) findViewById(R.id.order_compeleted);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.order_swipe);
        layoutOnTheWay = (LinearLayout) findViewById(R.id.layout_ontheway);
        layoutonComplete = (LinearLayout) findViewById(R.id.layout_oncompleted);
        textTitleOnWay = (TextView) findViewById(R.id.text_oreder_ontheway);
        textTitleComplete = (TextView) findViewById(R.id.text_order_omplete);
    }

    public void callAccount() {
        utils.startProgress();
        String userId = utils.getPreference(Constant.PREF_ID);
        ApiUtils.getAPIService().requestOrders(userId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                swipeRefreshLayout.setRefreshing(false);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    System.out.println("Response===>" + response);

                    if (success.equals("1")) {

                        String strOrderComplete = response.body().getStrOrderComplete();
                        String strOrderInComplete = response.body().getStrOrderInComplete();

                        textCompleted.setText(strOrderComplete);
                        textOntheWay.setText(strOrderInComplete);
                        listOrders = new ArrayList<>();
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        System.out.println("POSTDATA====" + listOfPostData.size());
                        MyOrderDO myOrderDO;
                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo registrationInfo = listOfPostData.get(i);
                            myOrderDO = new MyOrderDO();

                            String isComplete = registrationInfo.getIsOrderComplete();
//keyisorder_complete=1-->Completed......
                            //keyisorder_complete=0-->InCompleted


                            if (flag == false && isComplete.equals("1")) {
                                myOrderDO.setOrder_id(registrationInfo.getOrderid());
                                myOrderDO.setOrderID(registrationInfo.getOrderId());
                                myOrderDO.setStrDriverName(registrationInfo.getName());
                                myOrderDO.setStrCustomerName(registrationInfo.getCustomername());
                                myOrderDO.setDelivery_time(registrationInfo.getDelivery_time());
                                myOrderDO.setStrCustomerCall(registrationInfo.getCustomercall());
                                myOrderDO.setStrCustomerMeal(registrationInfo.getCustomermeal());
                                myOrderDO.setStrCustomerImage(registrationInfo.getCustomerimg());
                                myOrderDO.setStrCustomerTotalPrice(registrationInfo.getCustomertotalprice());

                                myOrderDO.setStrDriverCall(registrationInfo.getPhone());
                                myOrderDO.setStrCarModel(registrationInfo.getStrCarModel());
                                myOrderDO.setStrCarColor(registrationInfo.getStrCarColor());
                                myOrderDO.setStrStatus(registrationInfo.getIsOrderComplete());
                                myOrderDO.setStrDriverStatus(registrationInfo.getStrDriverStatus());
                                myOrderDO.setStrDriverImage(registrationInfo.getImageLink());
                                listOrders.add(myOrderDO);
                            }
                            if (flag && isComplete.equals("0")) {
                                myOrderDO.setOrder_id(registrationInfo.getOrderid());
                                myOrderDO.setOrderID(registrationInfo.getOrderId());
                                myOrderDO.setStrDriverName(registrationInfo.getName());
                                myOrderDO.setDelivery_time(registrationInfo.getDelivery_time());

                                myOrderDO.setStrCustomerReqDate(registrationInfo.getStrCustomerReqDate());

                                myOrderDO.setStrCustomerName(registrationInfo.getCustomername());
                                myOrderDO.setStrCustomerCall(registrationInfo.getCustomercall());
                                myOrderDO.setStrCustomerMeal(registrationInfo.getCustomermeal());
                                myOrderDO.setStrCustomerImage(registrationInfo.getCustomerimg());
                                myOrderDO.setStrCustomerTotalPrice(registrationInfo.getCustomertotalprice());

                                myOrderDO.setStrDriverCall(registrationInfo.getPhone());
                                myOrderDO.setStrCarModel(registrationInfo.getStrCarModel());
                                myOrderDO.setStrCarColor(registrationInfo.getStrCarColor());
                                myOrderDO.setStrStatus(registrationInfo.getIsOrderComplete());
                                myOrderDO.setStrDriverImage(registrationInfo.getImageLink());
                                myOrderDO.setStrDriverStatus(registrationInfo.getStrDriverStatus());
                                listOrders.add(myOrderDO);
                            }
                        }

                        orderAdapter = new OrderAdapter(OrderActivity.this, listOrders);
                        listOrder.setAdapter(orderAdapter);
                    } else if (response.body().getSuccess().equals("0")) {
                        utils.Toast(response.body().getMessage(), null);
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    swipeRefreshLayout.setRefreshing(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, SellerDrawer.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    public void RunTimePermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(OrderActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            } else {
                requestCameraPermission();
            }
        }
    }

    private void requestCameraPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(OrderActivity.this, CALL_PHONE)) {
        }
        ActivityCompat.requestPermissions(OrderActivity.this, new String[]{CALL_PHONE}, 1);
    }
}
