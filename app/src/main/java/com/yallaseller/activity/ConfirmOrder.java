package com.yallaseller.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.api.KeyAbstract;
import com.yallaseller.fragment.HomeFragment;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vivek on 6/8/2017.
 */

public class ConfirmOrder extends Activity {

    private LinearLayout layout_add_details_driver_one, layout_add_details_driver_two, layout_add_details_driver_three;
    private LinearLayout layoutMain1, layoutMain2, layoutMain3;
    private LinearLayout layoutEditOne, layoutEditTwo, layoutEditThree;

    private RelativeLayout add_driver_one, add_driver_two, add_driver_three;
    private LinearLayout bottom_edit_driver1, bottom_edit_driver2, bottom_edit_driver3;
    private TextView textCurAddDriver1;
    private Utils utils;

    private TextView dv1TvCusName, dv1TvCusNumb, dv1TvCusStreetAdd, dv1TvCusDestAdd, dv1TvCusFloor, dv1TvCusBul;
    private TextView dv2TvCusName, dv2TvCusNumb, dv2TvCusStreetAdd, dv2TvCusDestAdd, dv2TvCusFloor, dv2TvCusBul;
    private TextView dv3TvCusName, dv3TvCusNumb, dv3TvCusStreetAdd, dv3TvCusDestAdd, dv3TvCusFloor, dv3TvCusBul;

    private TextView textCurAdd1, textCurAdd2, textCurAdd3;
    private String strdv1CusName, strdv1CusNumb, strdv1CusStreetAdd, strdv1CusDestAdd, strdv1CusFloor, strdv1CusBul, strdv1Notes, strdv1Apt;
    private String strdv2CusName, strdv2CusNumb, strdv2CusStreetAdd, strdv2CusDestAdd, strdv2CusFloor, strdv2CusBul, strdv2Notes, strdv2Apt;
    private String strdv3CusName, strdv3CusNumb, strdv3CusStreetAdd, strdv3CusDestAdd, strdv3CusFloor, strdv3CusBul, strdv3Notes, strdv3Apt;
    private int driverClicked = 0;//0=1,1=2,2=3
    private int isEditClick = 0;//1=1,2=2,3=3
    private JSONObject jsonOrderOne, jsonOrderTwo, jsonOrderThree;
    private String userId;
    private Button btnOrderNow;

    private TextView textPlus, textMinus, textnumber;
    private int menuCount = 1;

    private TextView textOptionalOne, textOptionalTwo, textOptionalThree;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        init();
        jsonOrderOne = jsonOrderTwo = jsonOrderThree = new JSONObject();

        if (HomeFragment.isOrdrededOne == false) {
            add_driver_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    driverClicked = 0;
                    dialogDriverDetails();
                }
            });

            layoutEditOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isEditClick = 1;
                    driverClicked = 0;
                    dialogDriverDetails();
                }
            });
        } else {
            add_driver_one.setVisibility(View.GONE);
            layoutEditOne.setVisibility(View.GONE);
        }


        if (HomeFragment.isOrdrededTwo == false) {
            add_driver_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    driverClicked = 1;
                    dialogDriverDetails();
                }
            });


            layoutEditTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isEditClick = 2;
                    driverClicked = 1;
                    dialogDriverDetails();
                }
            });

        } else {
            add_driver_two.setVisibility(View.GONE);
            layoutEditTwo.setVisibility(View.GONE);
        }


        if (HomeFragment.isOrdrededThree == false) {
            add_driver_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    driverClicked = 2;
                    dialogDriverDetails();
                }
            });

            layoutEditThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isEditClick = 3;
                    driverClicked = 2;
                    dialogDriverDetails();
                }
            });
        } else {
            add_driver_three.setVisibility(View.GONE);
            layoutEditThree.setVisibility(View.GONE);
        }


        btnOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (utils.isNetConnected()) {
                    callOrder();
                } else {
                    utils.Toast(getResources().getString(R.string.please_enter_conf_pass_label), null);
                }
            }
        });

        textPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCalc(true);
            }
        });

        textMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCalc(false);
            }
        });
    }

    private void menuCalc(boolean isPLus) {
        menuCount = Integer.parseInt(textnumber.getText().toString());
        if (isPLus) {
            if (menuCount != 3) {
                menuCount++;
                textnumber.setText(String.valueOf(menuCount));
            }
        } else {
            if (menuCount != 1) {
                menuCount--;
                textnumber.setText(String.valueOf(menuCount));
            }
        }

        if (menuCount == 2) {
            layoutMain2.setVisibility(View.VISIBLE);
            layoutMain3.setVisibility(View.GONE);
        }

        if (menuCount == 3) {
            layoutMain2.setVisibility(View.VISIBLE);
            layoutMain3.setVisibility(View.VISIBLE);
        }

        if (menuCount == 1) {
            layoutMain1.setVisibility(View.VISIBLE);
            layoutMain2.setVisibility(View.GONE);
            layoutMain3.setVisibility(View.GONE);
        }

    }

    private void callOrder() {
        utils.startProgress();
        String jsonString = "";
        JSONObject jsonObject = new JSONObject();
        try {
            if (layoutMain1.getVisibility() == View.VISIBLE && HomeFragment.isOrdrededOne == false) {
                jsonObject.put("KeyOrder1", jsonOrderOne);
            }

            if (layoutMain2.getVisibility() == View.VISIBLE && HomeFragment.isOrdrededTwo == false) {
                jsonObject.put("KeyOrder2", jsonOrderTwo);
            }

            if (layoutMain3.getVisibility() == View.VISIBLE && HomeFragment.isOrdrededThree == false) {
                jsonObject.put("KeyOrder3", jsonOrderThree);
            }

        } catch (Exception e) {

        }
        String country = utils.getPreference(Constant.PREF_COUNTRY);
        String timeZone = utils.timeZoneId();
        ApiUtils.getAPIService().requestOrderAPI(userId, jsonObject, String.valueOf(SellerDrawer.fusedLatitude), String.valueOf(SellerDrawer.fusedLongitude), country, timeZone).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    System.out.println("SUCCESS===>RESPONSE"+response);
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        HomeFragment.counter = 0;
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        try {
                            String order1 = registrationInfo.getStrKeyOrder1();
                            if (order1.equals("0")) {
                                utils.Toast(getResources().getString(R.string.order1_denied), null);
                            } else {
                                HomeFragment.isOrdrededOne = true;
                                HomeFragment.isOrdrededOneGO = true;
                            }
                        } catch (Exception e) {
                            HomeFragment.isOrdrededOne = false;
                            HomeFragment.isOrdrededOneGO = false;
                        }

                        try {
                            String order2 = registrationInfo.getStrKeyOrder2();
                            if (order2.equals("0")) {
                                utils.Toast(getResources().getString(R.string.order2_denied), null);
                            } else {
                                HomeFragment.isOrdrededTwo = true;
                                HomeFragment.isOrdrededTwoGO = true;
                            }
                        } catch (Exception e) {
                            HomeFragment.isOrdrededTwo = false;
                            HomeFragment.isOrdrededTwoGO = false;
                        }

                        try {
                            String order3 = registrationInfo.getStrKeyOrder3();
                            if (order3.equals("0")) {
                                utils.Toast(getResources().getString(R.string.order3_denied), null);
                            } else {
                                HomeFragment.isOrdrededThree = true;
                                HomeFragment.isOrdrededThreeGO = true;
                            }
                        } catch (Exception e) {
                            HomeFragment.isOrdrededThree = false;
                            HomeFragment.isOrdrededThreeGO = false;
                        }
                        finish();
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void init() {
        utils = new Utils(this);
        add_driver_one = (RelativeLayout) findViewById(R.id.add_driver_one);
        layout_add_details_driver_one = (LinearLayout) findViewById(R.id.layout_driver_one_details);
        bottom_edit_driver1 = (LinearLayout) findViewById(R.id.bottom_edit_driver1);

        add_driver_two = (RelativeLayout) findViewById(R.id.add_driver_two);
        layout_add_details_driver_two = (LinearLayout) findViewById(R.id.layout_driver_two_details);
        bottom_edit_driver2 = (LinearLayout) findViewById(R.id.bottom_edit_driver2);

        add_driver_three = (RelativeLayout) findViewById(R.id.add_driver_three);
        layout_add_details_driver_three = (LinearLayout) findViewById(R.id.layout_driver_three_details);
        bottom_edit_driver3 = (LinearLayout) findViewById(R.id.bottom_edit_driver3);

        dv1TvCusName = (TextView) findViewById(R.id.dialog_driver1_name);
        dv1TvCusNumb = (TextView) findViewById(R.id.dialog_driver1_number);
        dv1TvCusStreetAdd = (TextView) findViewById(R.id.dialog_driver1_streetadd);
        dv1TvCusDestAdd = (TextView) findViewById(R.id.dialog_driver1_dest_add);
        dv1TvCusFloor = (TextView) findViewById(R.id.dialog_driver1_floor);
        dv1TvCusBul = (TextView) findViewById(R.id.dialog_driver1_buildno);

        dv2TvCusName = (TextView) findViewById(R.id.dialog_driver2_name);
        dv2TvCusNumb = (TextView) findViewById(R.id.dialog_driver2_number);
        dv2TvCusStreetAdd = (TextView) findViewById(R.id.dialog_driver2_streetadd);
        dv2TvCusDestAdd = (TextView) findViewById(R.id.dialog_driver2_dest_add);
        dv2TvCusFloor = (TextView) findViewById(R.id.dialog_driver2_floor);
        dv2TvCusBul = (TextView) findViewById(R.id.dialog_driver2_buildno);

        dv3TvCusName = (TextView) findViewById(R.id.dialog_driver3_name);
        dv3TvCusNumb = (TextView) findViewById(R.id.dialog_driver3_number);
        dv3TvCusStreetAdd = (TextView) findViewById(R.id.dialog_driver3_streetadd);
        dv3TvCusDestAdd = (TextView) findViewById(R.id.dialog_driver3_dest_add);
        dv3TvCusFloor = (TextView) findViewById(R.id.dialog_driver3_floor);
        dv3TvCusBul = (TextView) findViewById(R.id.dialog_driver3_buildno);

        textCurAdd1 = (TextView) findViewById(R.id.map_cur_tv_add_drive_1);
        textCurAdd2 = (TextView) findViewById(R.id.map_cur_tv_add_drive_2);
        textCurAdd3 = (TextView) findViewById(R.id.map_cur_tv_add_drive_3);


        textOptionalOne = (TextView) findViewById(R.id.confirm_optinal_one);
        textOptionalTwo = (TextView) findViewById(R.id.confirm_optinal_two);
        textOptionalThree = (TextView) findViewById(R.id.confirm_optinal_three);

        btnOrderNow = (Button) findViewById(R.id.btn_confirm_order);
        userId = utils.getPreference(Constant.PREF_ID);

        textCurAdd1.setText(HomeFragment.strCurAdd);
        textCurAdd2.setText(HomeFragment.strCurAdd);
        textCurAdd3.setText(HomeFragment.strCurAdd);

        textPlus = (TextView) findViewById(R.id.confirm_tv_plus_menu);
        textMinus = (TextView) findViewById(R.id.confirm_tv_minus_menu);
        textnumber = (TextView) findViewById(R.id.confirm_tv_number_menu);

        layoutMain1 = (LinearLayout) findViewById(R.id.mainlayoutdriverone);
        layoutMain2 = (LinearLayout) findViewById(R.id.mainlayoutdrivertwo);
        layoutMain3 = (LinearLayout) findViewById(R.id.mainlayoutdriverthree);

        layoutEditOne = (LinearLayout) findViewById(R.id.bottom_edit_driver1);
        layoutEditTwo = (LinearLayout) findViewById(R.id.bottom_edit_driver2);
        layoutEditThree = (LinearLayout) findViewById(R.id.bottom_edit_driver3);
    }

    private void dialogDriverDetails() {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_add_delivery_detail);
            dialog.setCancelable(true);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.show();

            final EditText edtCustName = (EditText) dialog.findViewById(R.id.edt_dailog_cust_name);
            final EditText edtCustNum = (EditText) dialog.findViewById(R.id.edt_dailog_cust_num);
            final EditText edtCustDelAdd = (EditText) dialog.findViewById(R.id.edt_dailog_cust_delivery_add);
            final EditText edtCustStreetAdd = (EditText) dialog.findViewById(R.id.edt_dailog_cust_street_add);
            final EditText edtCustBuilName = (EditText) dialog.findViewById(R.id.edt_dailog_cust_building);
            final EditText edtCustFloor = (EditText) dialog.findViewById(R.id.edt_dailog_cust_floor);
            final EditText edtCustAprt = (EditText) dialog.findViewById(R.id.edt_dailog_cust_aprtmnt);
            final EditText edtCustNotes = (EditText) dialog.findViewById(R.id.edt_dailog_cust_notes);
            final TextView tvdriver = (TextView) dialog.findViewById(R.id.dialog_tv_driver);
            if (driverClicked == 0) {
                tvdriver.setText("Driver 1");
                if (isEditClick == 1) {
                    try {
                        isEditClick = 0;
                        edtCustName.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvCusName));
                        edtCustNum.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvCusNumb));
                        edtCustDelAdd.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvCusDestAdd));
                        edtCustStreetAdd.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvCusStreetAdd));
                        edtCustBuilName.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvCusBul));
                        edtCustFloor.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvCusFloor));
                        edtCustAprt.setText(jsonOrderOne.getString(KeyAbstract.KEY_strApt));
                        edtCustNotes.setText(jsonOrderOne.getString(KeyAbstract.KEY_strdvNotes));
                    } catch (Exception e) {

                    }
                }
            } else if (driverClicked == 1) {
                tvdriver.setText("Driver 2");
                if (isEditClick == 2) {
                    try {
                        isEditClick = 0;
                        edtCustName.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvCusName));
                        edtCustNum.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvCusNumb));
                        edtCustDelAdd.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvCusDestAdd));
                        edtCustStreetAdd.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvCusStreetAdd));
                        edtCustBuilName.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvCusBul));
                        edtCustFloor.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvCusFloor));
                        edtCustAprt.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strApt));
                        edtCustNotes.setText(jsonOrderTwo.getString(KeyAbstract.KEY_strdvNotes));
                    } catch (Exception e) {

                    }
                }
            } else {
                tvdriver.setText("Driver 3");
                if (isEditClick == 3) {
                    try {
                        isEditClick = 0;
                        edtCustName.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvCusName));
                        edtCustNum.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvCusNumb));
                        edtCustDelAdd.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvCusDestAdd));
                        edtCustStreetAdd.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvCusStreetAdd));
                        edtCustBuilName.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvCusBul));
                        edtCustFloor.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvCusFloor));
                        edtCustAprt.setText(jsonOrderThree.getString(KeyAbstract.KEY_strApt));
                        edtCustNotes.setText(jsonOrderThree.getString(KeyAbstract.KEY_strdvNotes));
                    } catch (Exception e) {

                    }
                }
            }

            Button btnSaveCustDetail = (Button) dialog.findViewById(R.id.edt_dailog_cust_btn_save);

            btnSaveCustDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String strCustName = edtCustName.getText().toString();
                    String strCustNum = edtCustNum.getText().toString();
                    String strCustDelAdd = edtCustDelAdd.getText().toString();
                    String strCustStreetAdd = edtCustStreetAdd.getText().toString();
                    String strCustBuilName = edtCustBuilName.getText().toString();
                    String strCustFloor = edtCustFloor.getText().toString();
                    String strCustAprt = edtCustAprt.getText().toString();
                    String strCustNotes = edtCustNotes.getText().toString();

                    if (driverClicked == 0) {
                        strdv1CusName = strCustName;
                        strdv1CusNumb = strCustNum;
                        strdv1CusStreetAdd = strCustStreetAdd;
                        strdv1CusDestAdd = strCustDelAdd;
                        strdv1CusFloor = strCustFloor;
                        strdv1CusBul = strCustBuilName;
                        strdv1Notes = strCustNotes;
                        strdv1Apt = strCustAprt;

                        layout_add_details_driver_one.setVisibility(View.VISIBLE);
                        add_driver_one.setVisibility(View.GONE);
                        bottom_edit_driver1.setVisibility(View.VISIBLE);
                        textOptionalOne.setVisibility(View.GONE);
                        dv1TvCusName.setText(strCustName);
                        dv1TvCusNumb.setText(strCustNum);
                        dv1TvCusStreetAdd.setText(strCustStreetAdd);
                        dv1TvCusDestAdd.setText(strCustDelAdd);
                        dv1TvCusFloor.setText(strCustFloor);
                        dv1TvCusBul.setText(strCustBuilName);

                        try {
                            jsonOrderOne.put(KeyAbstract.KEY_strdvCusName, strdv1CusName);
                            jsonOrderOne.put(KeyAbstract.KEY_strdvCusNumb, strdv1CusNumb);
                            jsonOrderOne.put(KeyAbstract.KEY_strdvCusStreetAdd, strdv1CusStreetAdd);
                            jsonOrderOne.put(KeyAbstract.KEY_strdvCusDestAdd, strdv1CusDestAdd);
                            jsonOrderOne.put(KeyAbstract.KEY_strdvCusFloor, strdv1CusFloor);
                            jsonOrderOne.put(KeyAbstract.KEY_strdvCusBul, strdv1CusBul);
                            jsonOrderOne.put(KeyAbstract.KEY_strdvNotes, strdv1Notes);
                            jsonOrderOne.put(KeyAbstract.KEY_strApt, strdv1Apt);
                        } catch (Exception e) {

                        }


                        dialog.dismiss();
                    } else if (driverClicked == 1) {
                        strdv2CusName = strCustName;
                        strdv2CusNumb = strCustNum;
                        strdv2CusStreetAdd = strCustStreetAdd;
                        strdv2CusDestAdd = strCustDelAdd;
                        strdv2CusFloor = strCustFloor;
                        strdv2CusBul = strCustBuilName;
                        strdv2Notes = strCustNotes;
                        strdv2Apt = strCustAprt;

                        add_driver_two.setVisibility(View.GONE);
                        layout_add_details_driver_two.setVisibility(View.VISIBLE);
                        bottom_edit_driver2.setVisibility(View.VISIBLE);
                        textOptionalTwo.setVisibility(View.GONE);
                        dv2TvCusName.setText(strCustName);
                        dv2TvCusNumb.setText(strCustNum);
                        dv2TvCusStreetAdd.setText(strCustStreetAdd);
                        dv2TvCusDestAdd.setText(strCustDelAdd);
                        dv2TvCusFloor.setText(strCustFloor);
                        dv2TvCusBul.setText(strCustBuilName);

                        try {
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvCusName, strdv2CusName);
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvCusNumb, strdv2CusNumb);
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvCusStreetAdd, strdv2CusStreetAdd);
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvCusDestAdd, strdv2CusDestAdd);
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvCusFloor, strdv2CusFloor);
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvCusBul, strdv2CusBul);
                            jsonOrderTwo.put(KeyAbstract.KEY_strdvNotes, strdv2Notes);
                            jsonOrderTwo.put(KeyAbstract.KEY_strApt, strdv2Apt);
                        } catch (Exception e) {

                        }
                        dialog.dismiss();
                    } else if (driverClicked == 2) {
                        strdv3CusName = strCustName;
                        strdv3CusNumb = strCustNum;
                        strdv3CusStreetAdd = strCustStreetAdd;
                        strdv3CusDestAdd = strCustDelAdd;
                        strdv3CusFloor = strCustFloor;
                        strdv3CusBul = strCustBuilName;
                        strdv3Notes = strCustNotes;
                        strdv3Apt = strCustAprt;

                        add_driver_three.setVisibility(View.GONE);
                        layout_add_details_driver_three.setVisibility(View.VISIBLE);
                        bottom_edit_driver3.setVisibility(View.VISIBLE);
                        textOptionalThree.setVisibility(View.GONE);

                        dv3TvCusName.setText(strCustName);
                        dv3TvCusNumb.setText(strCustNum);
                        dv3TvCusStreetAdd.setText(strCustStreetAdd);
                        dv3TvCusDestAdd.setText(strCustDelAdd);
                        dv3TvCusFloor.setText(strCustFloor);
                        dv3TvCusBul.setText(strCustBuilName);

                        try {
                            jsonOrderThree.put(KeyAbstract.KEY_strdvCusName, strdv3CusName);
                            jsonOrderThree.put(KeyAbstract.KEY_strdvCusNumb, strdv3CusNumb);
                            jsonOrderThree.put(KeyAbstract.KEY_strdvCusStreetAdd, strdv3CusStreetAdd);
                            jsonOrderThree.put(KeyAbstract.KEY_strdvCusDestAdd, strdv3CusDestAdd);
                            jsonOrderThree.put(KeyAbstract.KEY_strdvCusFloor, strdv3CusFloor);
                            jsonOrderThree.put(KeyAbstract.KEY_strdvCusBul, strdv3CusBul);
                            jsonOrderThree.put(KeyAbstract.KEY_strdvNotes, strdv3Notes);
                            jsonOrderThree.put(KeyAbstract.KEY_strApt, strdv3Apt);
                        } catch (Exception e) {

                        }
                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
