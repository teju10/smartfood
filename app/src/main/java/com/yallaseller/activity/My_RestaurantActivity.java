package com.yallaseller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.adapter.ViewPagerAdapter;
import com.yallaseller.fragment.MyMealsFragment;
import com.yallaseller.fragment.MyReviewFragment;
import com.yallaseller.fragment.MyWalletFragment;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONObject;

/**
 * Created by and-04 on 21/11/17.
 */

public class My_RestaurantActivity extends AppCompatActivity {

    Context mContext;
    Utils utils;
    ResponseTask rt;
    Toolbar tb;
    ImageView star1_rat, star2_rat, star3_rat, star4_rat, star5_rat;
    JSONObject joo = new JSONObject();
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constant.LANG));

        setContentView(R.layout.activity_my_restutant);
        mContext = this;
        utils = new Utils(this);
        tb = (Toolbar) findViewById(R.id.res_toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        tb.setNavigationIcon(R.drawable.ic_drawer_back_lft);


        b = new Bundle();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        FIND();

    }

    public void FIND() {
        star1_rat = (ImageView) findViewById(R.id.star1_rat);
        star2_rat = (ImageView) findViewById(R.id.star2_rat);
        star3_rat = (ImageView) findViewById(R.id.star3_rat);
        star4_rat = (ImageView) findViewById(R.id.star4_rat);
        star5_rat = (ImageView) findViewById(R.id.star5_rat);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        MyReviewFragment menuFragment = new MyReviewFragment();
        MyMealsFragment reviewFragment = new MyMealsFragment();
        b.putString(Constant.MYMEALSOBJ, joo.toString());
        reviewFragment.setArguments(b);
        MyWalletFragment infoFragment = new MyWalletFragment();
        adapter.addFragment(menuFragment, getString(R.string.myreview));
        adapter.addFragment(reviewFragment, getString(R.string.mymeals));
        adapter.addFragment(infoFragment, getString(R.string.mywallet));
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void Ratting(float rate) {
        if (rate >= 0 && rate < 0.5) {
            star1_rat.setImageResource(R.drawable.star_orange_empty);
            star2_rat.setImageResource(R.drawable.star_orange_empty);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);

        } else if (rate >= 0.5 && rate < 1) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange_empty);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);

        } else if (rate >= 1 && rate < 1.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange_empty);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 1.5 && rate < 2) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_half_orange);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 2 && rate < 2.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange_empty);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 2.5 && rate < 3) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_half_orange);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 3 && rate < 3.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange_empty);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 3.5 && rate < 4) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_half_orange);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 4 && rate < 4.5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange);
            star5_rat.setImageResource(R.drawable.star_orange_empty);
        } else if (rate >= 4.5 && rate < 5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange);
            star5_rat.setImageResource(R.drawable.star_half_orange);
        } else if (rate == 5) {
            star1_rat.setImageResource(R.drawable.star_orange);
            star2_rat.setImageResource(R.drawable.star_orange);
            star3_rat.setImageResource(R.drawable.star_orange);
            star4_rat.setImageResource(R.drawable.star_orange);
            star5_rat.setImageResource(R.drawable.star_orange);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, SellerDrawer.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }
}


