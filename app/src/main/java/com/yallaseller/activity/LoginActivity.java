package com.yallaseller.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yallaseller.R.id.login_edt_phone;

public class LoginActivity extends AppCompatActivity {

    private static final int LOCATION_PERMISSION_CODE = 300;
    Context mContext;
    private EditText edtPhone, edtPassword;
    private Button btnSubmit, btnSupport, btnAboutus;
    private TextView textForgot;
    private Utils utils;
    private Dialog dialogLanguage;
    private String strPhone, strPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        setViews();

        if (Utility.getSharedPreferences(mContext,Constant.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is===>Login  " + token);
            Utility.setSharedPreference(mContext, Constant.FCMID,token);
        } else {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is===>Login  " + token);
            Utility.setSharedPreference(mContext, Constant.FCMID,token);

        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (utils.isNetConnected()) {
                    strPhone = edtPhone.getText().toString();
                    strPassword = edtPassword.getText().toString();

                    if (strPhone.equals("") || strPassword.equals("")) {
                        utils.Toast(getResources().getString(R.string.strFillDetail), null);
                    } else {
                        callLogin();
                    }
                } else {
                    utils.Toast(getResources().getString(R.string.strConnection), null);
                }
            }
        });

        textForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

        btnSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent support = new Intent(LoginActivity.this, SupportActivity.class);
                startActivity(support);
            }
        });

        findViewById(R.id.login_btn_lang).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent support = new Intent(LoginActivity.this, LanguageScreenActivity.class);
                startActivity(support);
                finish();
            }
        });

        btnAboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent support = new Intent(LoginActivity.this, AboutusActivity.class);
                startActivity(support);
            }
        });

    }

    private void callLogin() {
        utils.startProgress();
        ApiUtils.getAPIService().requestLoginAPI(strPhone, strPassword, "android", Utility.getSharedPreferences(mContext,Constant.FCMID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        String id = registrationInfo.getId();
                        String name = registrationInfo.getName();
                        String storeName = registrationInfo.getStrStoreName();
                        String branchName = registrationInfo.getStrStoreName();
                        String email = registrationInfo.getEmail();
                        String imageLink = registrationInfo.getImageLink();
                        String mobile = registrationInfo.getPhone();
                        String country = registrationInfo.getCountry();

                        utils.setPreference(Constant.PREF_ID, id);
                        utils.setPreference(Constant.PREF_NAME, name);
                        utils.setPreference(Constant.PREF_STORE, storeName);
                        utils.setPreference(Constant.PREF_BRANCH, branchName);
                        utils.setPreference(Constant.PREF_EMAIL, email);
                        utils.setPreference(Constant.PREF_IMAGE, imageLink);
                        utils.setPreference(Constant.PREF_PHONE, mobile);
                        utils.setPreference(Constant.PREF_COUNTRY, country);
                        utils.Toast(msg, null);

                        if (Utility.getIngerSharedPreferences(mContext, Constant.LANG) == 1) {
                            Utility.setIntegerSharedPreference(mContext, Constant.LANG, 1);
                            utils.setPreference(Constant.isLangSet, "en");

                        } else {
                            Utility.setIntegerSharedPreference(mContext, Constant.LANG, 2);
                            utils.setPreference(Constant.isLangSet, "ar");
                        }
                        Intent intent = new Intent(LoginActivity.this, SellerDrawer.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void permissionLocation() {
        int result = ContextCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (result == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == LOCATION_PERMISSION_CODE) {
            //Checking location permissions
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionLocation();
            } else {
            }
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            permissionLocation();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.please_loc_label))
                .setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void setViews() {
        utils = new Utils(this);
        edtPhone = (EditText) findViewById(login_edt_phone);
        edtPassword = (EditText) findViewById(R.id.login_edt_password);
        btnSubmit = (Button) findViewById(R.id.login_btn_submit);
        textForgot = (TextView) findViewById(R.id.login_text_forgot);
        btnSupport = (Button) findViewById(R.id.login_btn_support);
        btnAboutus = (Button) findViewById(R.id.login_btn_aboutus);
    }

}
