package com.yallaseller.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.adapter.NotificationAdapter;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.NotificationDO;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vivek on 7/13/2017.
 */

public class NotificationListActivity extends AppCompatActivity {

    private static final int SWIPE_DURATION = 250;
    private static final int MOVE_DURATION = 150;
    BackgroundContainer mBackgroundContainer;
    boolean mSwiping = false;
    boolean mItemPressed = false;
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>();
    private SwipeMenuListView listviewNoti;
    private Utils utils;
    Toolbar tb;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<NotificationDO> listNotification;
    private NotificationAdapter notificationAdapter;
    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        mContext = this;
        tb = (Toolbar) findViewById(R.id.res_toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        tb.setNavigationIcon(R.drawable.ic_drawer_back_lft);
        ((TextView) findViewById(R.id.toolbar_title)).setText(mContext.getResources().getString(R.string.notification));

        setViews();

        if (utils.isNetConnected()) {
            callNotification();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (utils.isNetConnected()) {
                    callNotification();
                } else {
                    utils.Toast(getResources().getString(R.string.strConnection), null);
                }
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
//                deleteItem.setBackground(new ColorDrawable(Color.rgb(252,
//                        134, 0x25)));
                deleteItem.setBackground(new ColorDrawable(Color.GRAY));
                deleteItem.setWidth(120);
                deleteItem.setIcon(android.R.drawable.ic_delete);
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        listviewNoti.setMenuCreator(creator);
        listviewNoti.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        listviewNoti.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                utils.startProgress();
                callDelteNotification(listNotification.get(position).getStrId());
                // false : close the menu; true : not close the menu
                return false;
            }
        });

    }

    private void setViews() {
        utils = new Utils(this);
        listviewNoti = (SwipeMenuListView) findViewById(R.id.notification_list);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.notification_swipe);
        mBackgroundContainer = (BackgroundContainer) findViewById(R.id.listViewBackground);
        Utility.setSharedPreference(mContext,Constant.COUNT,"");
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), SellerDrawer.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    private void callNotification() {
        utils.startProgress();
        String userId = utils.getPreference(Constant.PREF_ID);
        ApiUtils.getAPIService().requestNotificationList(userId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                swipeRefreshLayout.setRefreshing(false);
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        Utility.setSharedPreference(mContext,Constant.COUNT,"0");

                        listNotification = new ArrayList<>();
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        NotificationDO notiDO;
                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo callDO = listOfPostData.get(i);
                            notiDO = new NotificationDO();
                            notiDO.setStrId(callDO.getId());
                            notiDO.setTitle(callDO.getNotiTitle());
                            notiDO.setMsg(callDO.getNotiMsg());
                            notiDO.setTime(callDO.getNotiTIme());
                            listNotification.add(notiDO);
                        }

                        notificationAdapter = new NotificationAdapter(NotificationListActivity.this, 0, listNotification);
                        listviewNoti.setAdapter(notificationAdapter);
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    swipeRefreshLayout.setRefreshing(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void callDelteNotification(String keyId) {
        ApiUtils.getAPIService().requestDelteNotificationList(keyId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        callNotification();
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    swipeRefreshLayout.setRefreshing(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
