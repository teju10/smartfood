package com.yallaseller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;

import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import java.util.Locale;

/**
 * Created by vivek on 6/19/2017.
 */

public class LanguageScreenActivity extends AppCompatActivity {

    Context mContext;
    private Utils utils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_language);
        mContext = this;
        final LinearLayout layoutAr = (LinearLayout) findViewById(R.id.dialog_layout_ar);
        final LinearLayout layoutEn = (LinearLayout) findViewById(R.id.dialog_layout_eng);
        utils = new Utils(this);

        layoutAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String languageToLoad = "ar";
                Utility.setIntegerSharedPreference(mContext, Constant.LANG, 2);
                Resources res = getResources();
                Configuration conf = res.getConfiguration();
                DisplayMetrics dm = res.getDisplayMetrics();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    conf.setLocale(new Locale(languageToLoad));
                    createConfigurationContext(conf);
                    res.updateConfiguration(conf, dm);
                } else {
                    conf.locale = new Locale(languageToLoad);
                    getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
                }

                utils.setPreference(Constant.isLangSet, languageToLoad);
                if (utils.getPreference(Constant.PREF_ID).equals("")) {
                    startActivity(new Intent(LanguageScreenActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(LanguageScreenActivity.this, SellerDrawer.class));
                }
                finish();
            }
        });

        layoutEn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String languageToLoad = "en";
                Utility.setIntegerSharedPreference(mContext, Constant.LANG, 1);

                Resources res = getResources();
                Configuration conf = res.getConfiguration();
                DisplayMetrics dm = res.getDisplayMetrics();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    conf.setLocale(new Locale(languageToLoad));
                    createConfigurationContext(conf);
                    res.updateConfiguration(conf, dm);
                } else {
                    conf.locale = new Locale(languageToLoad);
                    getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
                }

                utils.setPreference(Constant.isLangSet, languageToLoad);

                if (utils.getPreference(Constant.PREF_ID).equals("")) {
                    startActivity(new Intent(LanguageScreenActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(LanguageScreenActivity.this, SellerDrawer.class));
                }
                finish();
            }
        });
    }


}
