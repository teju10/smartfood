package com.yallaseller.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonParser;
import com.yallaseller.R;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupportActivity extends AppCompatActivity {

    private EditText edtName, edtEmail, edtNumber, edtComment;
    private TextView textSubmit;
    private Utils utils;
    private JsonParser jsonParser;
    private Spinner spinner;
    private ArrayList<String> countryList;
    private String strName, strEmail, strNumber, strComment, strCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        setViews();

        findViewById(R.id.support_btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        textSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (utils.isNetConnected()) {
                    strName = edtName.getText().toString();
                    strEmail = edtEmail.getText().toString();
                    strNumber = edtNumber.getText().toString();
                    strComment = edtComment.getText().toString();
                    strCountry = spinner.getSelectedItem().toString();

                    if (strName.equals("") || strEmail.equals("") || strNumber.equals("")) {
                        utils.Toast(getResources().getString(R.string.please_enter_label), null);
                    } else {
                        callSupport();
                    }
                } else {
                    utils.Toast(getResources().getString(R.string.strConnection), null);
                }
            }
        });

    }

    private void setViews() {
        utils = new Utils(this);
        jsonParser = new JsonParser();
        countryList = new ArrayList<>();

        edtName = (EditText) findViewById(R.id.support_edt_name);
        edtEmail = (EditText) findViewById(R.id.support_edt_mail);
        edtNumber = (EditText) findViewById(R.id.support_edt_number);
        edtComment = (EditText) findViewById(R.id.support_comment);
        textSubmit = (TextView) findViewById(R.id.support_submit);
        spinner = (Spinner) findViewById(R.id.support_spinner_country);

        callGetCountry();
    }

    private void callGetCountry() {
        utils.startProgress();
        ApiUtils.getAPIService().requestGetCountry().enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                                                listOfPostData = response.body().getPostdata();

                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo registrationInfo = listOfPostData.get(i);
                            String country = registrationInfo.getCountry();
                            countryList.add(country);
                        }
                        ArrayAdapter aa = new ArrayAdapter(SupportActivity.this, android.R.layout.simple_spinner_item, countryList);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        spinner.setAdapter(aa);
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void callSupport() {
        utils.startProgress();
        ApiUtils.getAPIService().requestSupportLink(strName, strEmail, strNumber, strCountry, strComment).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                       onBackPressed();
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }
}
