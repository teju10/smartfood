package com.yallaseller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.yallaseller.R;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vivek on 6/2/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {

    private Utils utils;
    private EditText edtForgot;
    private Button btnSubmit;
    private String strEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        init();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (utils.isNetConnected()) {
                    strEmail = edtForgot.getText().toString();
                    if (!strEmail.equals("")) {
                        callForgot();
                    } else {
                        utils.Toast(getResources().getString(R.string.please_enter_email_label), null);
                    }
                } else {
                    utils.Toast(getResources().getString(R.string.strConnection), null);
                }
            }
        });

    }

    private void init() {
        utils = new Utils(this);
        edtForgot = (EditText) findViewById(R.id.forgot_edt_email);
        btnSubmit = (Button) findViewById(R.id.forgot_btn_submit);
    }

    private void callForgot() {
        utils.startProgress();
        ApiUtils.getAPIService().requestForgotAPI(strEmail).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }


}
