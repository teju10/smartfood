package com.yallaseller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by vivek on 3/23/2017.
 */

public class DestinationActivity extends AppCompatActivity {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    private static final String API_KEY = "&key=AIzaSyBkGMcMpSHGW4Ic3V0kR0-WxIXCRSGPZOo";
    private static final int SWIPE_DURATION = 250;
    private static final int MOVE_DURATION = 150;
    public static String LOCATION = "?location=";
    public static String strDestinationLocation = "";
    static String filter = "";
    private static String searchItem = "";
    StableArrayAdapter mAdapter;
    ListView mListView;
    BackgroundContainer mBackgroundContainer;
    boolean mSwiping = false;
    Context mContext;
    boolean mItemPressed = false;
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>();
    private Button btnLocation;
    private AutoCompleteTextView autoCompDestination;
    private Utils utils;
    private String savedLat, savedLong;
    private LatLng savedLatLong;
    private Button btnBack, btnCurLoc;
    private ArrayList<String> arrSavedLoc;

    private String saveLocString;

/*
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {

        float mDownX;
        private int mSwipeSlop = -1;

        @Override
        public boolean onTouch(final View v, MotionEvent event) {
            if (mSwipeSlop < 0) {
                mSwipeSlop = ViewConfiguration.get(DestinationActivity.this).
                        getScaledTouchSlop();
            }
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (mItemPressed) {
                        return false;
                    }
                    mItemPressed = true;
                    mDownX = event.getX();
                    break;
                case MotionEvent.ACTION_CANCEL:
                    v.setAlpha(1);
                    v.setTranslationX(0);
                    mItemPressed = false;
                    break;
                case MotionEvent.ACTION_MOVE: {
                    float x = event.getX() + v.getTranslationX();
                    float deltaX = x - mDownX;
                    float deltaXAbs = Math.abs(deltaX);
                    if (!mSwiping) {
                        if (deltaXAbs > mSwipeSlop) {
                            mSwiping = true;
                            mListView.requestDisallowInterceptTouchEvent(true);
                            mBackgroundContainer.showBackground(v.getTop(), v.getHeight());
                        }
                    }
                    if (mSwiping) {
                        v.setTranslationX((x - mDownX));
                        v.setAlpha(1 - deltaXAbs / v.getWidth());
                    }
                }
                break;
                case MotionEvent.ACTION_UP: {
                    // User let go - figure out whether to animate the view out, or back into place
                    if (mSwiping) {
                        float x = event.getX() + v.getTranslationX();
                        float deltaX = x - mDownX;
                        float deltaXAbs = Math.abs(deltaX);
                        float fractionCovered;
                        float endX;
                        float endAlpha;
                        final boolean remove;
                        if (deltaXAbs > v.getWidth() / 4) {
                            // Greater than a quarter of the width - animate it out
                            fractionCovered = deltaXAbs / v.getWidth();
                            endX = deltaX < 0 ? -v.getWidth() : v.getWidth();
                            endAlpha = 0;
                            remove = true;
                        } else {
                            // Not far enough - animate it back
                            fractionCovered = 1 - (deltaXAbs / v.getWidth());
                            endX = 0;
                            endAlpha = 1;
                            remove = false;
                        }
                        long duration = (int) ((1 - fractionCovered) * SWIPE_DURATION);
                        mListView.setEnabled(false);
                        v.animate().setDuration(duration).
                                alpha(endAlpha).translationX(endX).
                                withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Restore animated values
                                        v.setAlpha(1);
                                        v.setTranslationX(0);
                                        if (remove) {
                                            animateRemoval(mListView, v);
                                        } else {
                                            mBackgroundContainer.hideBackground();
                                            mSwiping = false;
                                            mListView.setEnabled(true);
                                        }
                                    }
                                });
                    }
                }
                mItemPressed = false;
                break;
                default:
                    return false;
            }
            return true;
        }
    };
*/

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        filter = input;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + "input=" + input + API_KEY);
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return resultList;
        } catch (IOException e) {
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    private void animateRemoval(final ListView listview, View viewToRemove) {
        int firstVisiblePosition = listview.getFirstVisiblePosition();
        for (int i = 0; i < listview.getChildCount(); ++i) {
            View child = listview.getChildAt(i);
            if (child != viewToRemove) {
                int position = firstVisiblePosition + i;
                long itemId = mAdapter.getItemId(position);
                mItemIdTopMap.put(itemId, child.getTop());
            }
        }
        // Delete the item from the adapter
        int position = mListView.getPositionForView(viewToRemove);
        mAdapter.remove(mAdapter.getItem(position));

        final ViewTreeObserver observer = listview.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                observer.removeOnPreDrawListener(this);
                boolean firstAnimation = true;
                int firstVisiblePosition = listview.getFirstVisiblePosition();
                for (int i = 0; i < listview.getChildCount(); ++i) {
                    final View child = listview.getChildAt(i);
                    int position = firstVisiblePosition + i;
                    long itemId = mAdapter.getItemId(position);
                    Integer startTop = mItemIdTopMap.get(itemId);
                    int top = child.getTop();
                    if (startTop != null) {
                        if (startTop != top) {
                            int delta = startTop - top;
                            child.setTranslationY(delta);
                            child.animate().setDuration(MOVE_DURATION).translationY(0);
                            if (firstAnimation) {
                                child.animate().withEndAction(new Runnable() {
                                    public void run() {
                                        mBackgroundContainer.hideBackground();
                                        mSwiping = false;
                                        mListView.setEnabled(true);
                                    }
                                });
                                firstAnimation = false;
                            }
                        }
                    } else {
                        // Animate new views along with the others. The catch is that they did not
                        // exist in the start state, so we must calculate their starting position
                        // based on neighboring views.
                        int childHeight = child.getHeight() + listview.getDividerHeight();
                        startTop = top + (i > 0 ? childHeight : -childHeight);
                        int delta = startTop - top;
                        child.setTranslationY(delta);
                        child.animate().setDuration(MOVE_DURATION).translationY(0);
                        if (firstAnimation) {
                            child.animate().withEndAction(new Runnable() {
                                public void run() {
                                    mBackgroundContainer.hideBackground();
                                    mSwiping = false;
                                    mListView.setEnabled(true);
                                }
                            });
                            firstAnimation = false;
                        }
                    }
                }
                mItemIdTopMap.clear();
                return true;
            }
        });
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        mContext = this;

        autoCompDestination = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        mListView = (ListView) findViewById(R.id.listview);

        btnLocation = (Button) findViewById(R.id.dest_btn_location);
        btnBack = (Button) findViewById(R.id.dest_btn_back);
        btnCurLoc = (Button) findViewById(R.id.btnCurLoc);
        utils = new Utils(this);
        arrSavedLoc = new ArrayList<>();
        savedLatLong = new LatLng(0.0, 0.0);

        saveLocString = utils.getPreference(Constant.PREF_SAVE_ADD);
        String[] splitedString = saveLocString.split("\\|");
        if (splitedString.length > 0) {
            for (int i = 0; i < splitedString.length; i++) {
                String str = splitedString[i];
                if (!str.equals("")) arrSavedLoc.add(str);
            }
        }

        autoCompDestination.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));

        autoCompDestination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                strDestinationLocation = (String) adapterView.getItemAtPosition(i);
                if (!saveLocString.contains(strDestinationLocation)) {
                    saveLocString = saveLocString + "|" + strDestinationLocation;
                    utils.setPreference(Constant.PREF_SAVE_ADD, saveLocString);
                }
                checkLoc();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.setPreference(Constant.PREF_SAVED_LAT, String.valueOf(savedLatLong.latitude));
                utils.setPreference(Constant.PREF_SAVED_LONG, String.valueOf(savedLatLong.longitude));
                startActivity(new Intent(mContext, SellerDrawer.class));
                finish();
            }
        });

        btnCurLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.setPreference(Constant.PREF_SAVED_LAT, "");
                utils.setPreference(Constant.PREF_SAVED_LONG, "");
                startActivity(new Intent(mContext,SellerDrawer.class));
                finish();
            }
        });

        mBackgroundContainer = (BackgroundContainer) findViewById(R.id.listViewBackground);
        mBackgroundContainer.setFocusable(false);
        final ArrayList<String> cheeseList = new ArrayList<String>();
        for (int i = 0; i < arrSavedLoc.size(); ++i) {
            cheeseList.add(arrSavedLoc.get(i));
        }
        mAdapter = new StableArrayAdapter(this, R.layout.opaque_text_view, cheeseList/*,
                mTouchListener*/);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                strDestinationLocation = arrSavedLoc.get(position);
                autoCompDestination.setText(strDestinationLocation);
                checkLoc();
            }
        });
    }

    private void checkLoc() {
        utils.startProgress();
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    utils.dismissProgress();
                    if (savedLatLong.latitude == 0.0) {
                        savedLatLong = getLocationFromAddress(strDestinationLocation);
                        checkLoc();
                    } else {
                        utils.dismissProgress();
                        btnLocation.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    utils.Toast(getApplicationContext().getResources().getString(R.string.no_address_found),null);
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;
        try {
            address = coder.getFromLocationName(strAddress, 1);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater
                        .inflate(R.layout.list_item, parent, false);
            }
            TextView lblName = (TextView) view
                    .findViewById(R.id.text_search_destination);
            try {
                searchItem = resultList.get(position).toString();
                int startPos = searchItem.toLowerCase(Locale.US).indexOf(filter.toLowerCase());
                int endPos = startPos + filter.length();

                if (startPos != -1)
                {
                    Spannable spannable = new SpannableString(searchItem);
                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{R.color.colorPrimary});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                    spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    lblName.setText(spannable);
                } else
                    lblName.setText(searchItem);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return view;
        }

        @Override
        public int getCount() {
            if (resultList != null) {
                return resultList.size();
            } else {
                return 0;
            }
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index).toString();
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    try {
                        if (constraint != null) {
                            resultList = autocomplete(constraint.toString());
                            filterResults.values = resultList;
                            filterResults.count = resultList.size();
                        }
                    } catch (Exception e) {

                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}
