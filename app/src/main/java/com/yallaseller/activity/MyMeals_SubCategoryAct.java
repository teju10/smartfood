package com.yallaseller.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.soundcloud.android.crop.Crop;
import com.yallaseller.BuildConfig;
import com.yallaseller.R;
import com.yallaseller.adapter.MyMeals_ChoicesCatAdap;
import com.yallaseller.customwidget.CircleImageView;
import com.yallaseller.model.MyMealsChild;
import com.yallaseller.model.MyMealsGroup;
import com.yallaseller.serverinteraction.MultipartResponseListener;
import com.yallaseller.serverinteraction.MultipartTask;
import com.yallaseller.serverinteraction.ResponseListener;
import com.yallaseller.serverinteraction.ResponseTask;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.RuntimePermissionsActivity;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by and-04 on 24/11/17.
 */

public class MyMeals_SubCategoryAct extends RuntimePermissionsActivity implements View.OnClickListener {
    public static final String TAG = MyMeals_SubCategoryAct.class.getSimpleName();
    private static final int RC_LOCATION_CONTACTS_PERM = 1;
    Context mContext;
    ResponseTask rt;
    Utils utils;
    Dialog addsubcat_dia;
    String meal_id = "";
    String meal_name = "";
    TextView mealname, mealdes, mealprie;
    ImageView edit_img;
    CircleImageView mealimage;
    String imageurl = "";
    String PicturePath = "";
    MultipartTask multipartTask;
    ExpandableListView subcat_list;
    List<MyMealsGroup> groupList = new ArrayList<>();
    List<MyMealsChild> childList;
    boolean CLICK = false;
    ImageView add_maincat;
    MyMeals_ChoicesCatAdap adap;
    Toolbar tb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constant.LANG));

        setContentView(R.layout.act_mymeals_addsubcat);
        mContext = this;
        utils = new Utils(mContext);

        tb = (Toolbar) findViewById(R.id.res_toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        ((Toolbar) findViewById(R.id.res_toolbar)).setNavigationIcon(R.drawable.ic_drawer_back_lft);

        ((TextView) tb.findViewById(R.id.toolbar_title)).setText(mContext.getResources().getString(R.string.mymeals));


        Intent in = getIntent();
        meal_id = in.getStringExtra(Constant.MENUID);
        meal_name = in.getStringExtra(Constant.MENUNAME);
        bind();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == RC_LOCATION_CONTACTS_PERM) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(MyMeals_SubCategoryAct.this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(MyMeals_SubCategoryAct.this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(MyMeals_SubCategoryAct.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    requestCameraPermission();
                }
            } else {
                selectImage();
            }
        }
    }

    public void bind() {
        add_maincat = (ImageView) findViewById(R.id.add_maincat);
        subcat_list = (ExpandableListView) findViewById(R.id.subcat_list);
        mealname = (TextView) findViewById(R.id.meal_name);
        mealdes = (TextView) findViewById(R.id.meal_des);
        mealprie = (TextView) findViewById(R.id.meal_price);
        mealimage = (CircleImageView) findViewById(R.id.img_menu);
        edit_img = (ImageView) findViewById(R.id.edit_img);

       /* subcat_list.setGroupIndicator(null);
        subcat_list.setChildIndicator(null);
        subcat_list.setChildDivider(getResources().getDrawable(R.color.grey_400));
        subcat_list.setDivider(getResources().getDrawable(R.color.grey_300));
        subcat_list.setDividerHeight(10);*/

        listener();

        if (utils.isNetConnected()) {
            GetChoicesTask(meal_id);
        } else {
            utils.Toast(mContext.getResources().getString(R.string.check_internet), null);
        }
//        loadMealData();
        findViewById(R.id.add_maincat).setVisibility(View.VISIBLE);
        findViewById(R.id.hide_maincat).setVisibility(View.GONE);

    }

    public void listener() {
        add_maincat.setOnClickListener(this);
        findViewById(R.id.hide_maincat).setOnClickListener(this);
        edit_img.setOnClickListener(this);
        findViewById(R.id.btn_choices).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_img:
                if (hasPermissions(mContext, Manifest.permission.CAMERA)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(MyMeals_SubCategoryAct.this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(MyMeals_SubCategoryAct.this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(MyMeals_SubCategoryAct.this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            selectImage();
                        } else {
                            requestCameraPermission();
                        }
                    } else {
                        selectImage();
                    }
                } else {
                    requestAppPermissions(new String[]{Manifest.permission.CAMERA},
                            R.string.rationale_location, RC_LOCATION_CONTACTS_PERM);
                }
                break;

            case R.id.add_maincat:
//                CLICK = true;
                findViewById(R.id.add_choices_lay).setVisibility(View.VISIBLE);
                findViewById(R.id.hide_maincat).setVisibility(View.VISIBLE);
                findViewById(R.id.add_maincat).setVisibility(View.GONE);

                break;

            case R.id.hide_maincat:
                findViewById(R.id.add_choices_lay).setVisibility(View.GONE);
                findViewById(R.id.add_maincat).setVisibility(View.VISIBLE);
                findViewById(R.id.hide_maincat).setVisibility(View.GONE);

                break;

            case R.id.btn_choices:
                if (((EditText) findViewById(R.id.edt_choices)).getText().toString().equals("")) {
                    utils.Toast(mContext.getResources().getString(R.string.fill_type_choices), null);
                } else {
                    SendCatChoices(meal_id, ((EditText) findViewById(R.id.edt_choices)).getText().toString());
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] options = {getResources().getString(R.string.from_camera),
                getResources().getString(R.string.from_gallery), getResources().getString(R.string.close)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.add_your_img));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (item == 1) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (item == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    private Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        } else {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider",
                        new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        }
        return outputFileUri;
    }

    private void beginCrop(Uri source) {
        String MYFOLDER = "yallaseller";
        File pro = new File(Utility.MakeDir(MYFOLDER, mContext), System.currentTimeMillis() + ".jpg");
        Uri destination1 = Uri.fromFile(pro);
        Crop.of(source, destination1).asSquare().withAspect(200, 200).start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            String croped_file = "" + Crop.getOutput(result).getPath();
            PicturePath = croped_file;
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);
            // Log.e(TAG, "croped_file ===========> " + croped_file);
            PicturePath = croped_file;
            Glide.with(mContext).load(PicturePath).into(mealimage);
            updateImage();

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {
            //If the user has denied the permission previously your code will come to this block...
            //Here you can explain why you need this permission...
            //Explain here why you need this permission...
        }
        //And finally ask for the permission...
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, 1);
    }

    public void updateImage() {
        if (!PicturePath.equals("")) {
            UpdateImage(PicturePath);
        }
    }

    public void UpdateImage(String image) {
    /* http://infograins.com/INFO01/yalla/restaurant_api.php?action=AddMenuImage&menu_id=50&menu_image=imgName*/
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("menu_id", meal_id);
            utils.startProgress();
            multipartTask = new MultipartTask(this, jsonObject, Constant.ADDMENU_IMAGE,
                    "menu_image", image, TAG, "post");
            multipartTask.execute();
            multipartTask.setListener(new MultipartResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    try {
                        utils.dismissProgress();
                        Log.e("MyDetail", "result ======>" + result);
                        if (result == null) {
                            utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                        } else {
                            try {
                                JSONObject jobj = new JSONObject(result);
                                if (jobj.getString("success").equals("1")) {
//                                    JSONObject joo = jobj.getJSONObject("object");
                                    utils.Toast(mContext.getResources().getString(R.string.prodct_img_update), null);
                                    Glide.with(mContext)
                                            .load(jobj.getString("product_image"))
                                            /*.listener(new RequestListener<String, GlideDrawable>() {
                                                @Override
                                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                    findViewById(R.id.prodct_pb).setVisibility(View.GONE);
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                    findViewById(R.id.prodct_pb).setVisibility(View.GONE);
                                                    return false;
                                                }
                                            })*/
                                            .into(mealimage);
                                }
                            } catch (JSONException e) {
                                Log.e("exception multipart", e.toString());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetChoicesTask(String itemid) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.GETCHOICESLIST);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ITEMID, itemid);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                JSONObject joo = jobj.getJSONObject("object");
                                groupList.clear();

                                JSONObject jfood = joo.getJSONObject("food_details");
                                mealname.setText(jfood.getString("menu_name"));
                                mealdes.setText(jfood.getString("menu_title"));
                                mealprie.setText(jfood.getString("menu_price") + " JD");
//                                mealprie.setText("1011111" + " JD");
                                Glide.with(mContext).load(jfood.getString("menu_image").trim()).into(mealimage);
                                JSONArray menuarra = joo.getJSONArray("subitems");
                                for (int i = 0; i < menuarra.length(); i++) {
                                    JSONObject c = menuarra.getJSONObject(i);
                                    childList = new ArrayList<>();
                                    JSONArray childarra = c.getJSONArray("extra");

                                    for (int j = 0; j < childarra.length(); j++) {
                                        JSONObject object = childarra.getJSONObject(j);
                                        childList.add(new MyMealsChild(object.getInt("id"), object.getString("extra_name"),
                                                object.getString("price")));
                                    }
                                    groupList.add(new MyMealsGroup(c.getInt("sub_item_id"), c.getString("sub_item_name"), childList));
                                }
                                SetAdap();
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void SendCatChoices(String itemid, String choicesname) {
        //http://infograins.com/INFO01/yalla/restaurant_api.php?action=AddSubItem&
        // user_id=11&item_id=52&subitem_name=choice%20of%20drink
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constant.ACTION, Constant.ADDCHOICES);
            jo.put(Constant.USERID, utils.getPreference(Constant.PREF_ID));
            jo.put(Constant.ITEMID, itemid);
            jo.put(Constant.SUBITEMNAME, choicesname);
            utils.startProgress();
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    utils.dismissProgress();
                    if (result == null) {
                        utils.Toast(mContext.getResources().getString(R.string.server_fail), null);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                findViewById(R.id.add_choices_lay).setVisibility(View.GONE);
                                findViewById(R.id.add_maincat).setVisibility(View.VISIBLE);
                                ((EditText) findViewById(R.id.edt_choices)).setText("");
                                GetChoicesTask(meal_id);
                            } else {
                                utils.Toast(jobj.getString("msg"), null);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void SetAdap() {
        adap = new MyMeals_ChoicesCatAdap(mContext, groupList, childList);
        subcat_list.setAdapter(adap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
