package com.yallaseller.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yallaseller.R;
import com.yallaseller.SellerDrawer;
import com.yallaseller.api.ApiUtils;
import com.yallaseller.model.RegistrationDO;
import com.yallaseller.model.RegistrationInfo;
import com.yallaseller.utility.Constant;
import com.yallaseller.utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vivek on 6/2/2017.
 */

public class MyaccountActivity extends AppCompatActivity {

    private Utils utils;
    private TextView textBusName;
    private TextView textBranch;
    private TextView textPhone;
    private TextView textEmail;
    private TextView textPassword;
    private LinearLayout layoutBack;

    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myaccount);

        mContext = this;

        init();

        callAccount();

        textPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPassword();
            }
        });
    }

    private void callAccount() {
        utils.startProgress();
        String userId = utils.getPreference(Constant.PREF_ID);
        ApiUtils.getAPIService().requestAccount(userId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        textBusName.setText(registrationInfo.getStrStoreName());
                        textBranch.setText(registrationInfo.getStrBranchName());
                        textPhone.setText(registrationInfo.getPhone());
                        textEmail.setText(registrationInfo.getEmail());

                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });
    }

    private void callPassword(String oldPass, String newPass) {
        utils.startProgress();
        String userId = utils.getPreference(Constant.PREF_ID);
        ApiUtils.getAPIService().requestPassword(userId, oldPass, newPass).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                utils.dismissProgress();
                try {
                    String success = response.body().getSuccess();
                    String msg = response.body().getMessage();
                    if (success.equals("1")) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        utils.Toast(msg, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                utils.dismissProgress();
                utils.Toast(getResources().getString(R.string.strConnection), null);
            }
        });

        layoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {

        utils = new Utils(this);
        textBusName = (TextView) findViewById(R.id.account_busName);
        textBranch = (TextView) findViewById(R.id.account_branch);
        textPhone = (TextView) findViewById(R.id.account_phone);
        textEmail = (TextView) findViewById(R.id.account_email);
        textPassword = (TextView) findViewById(R.id.account_busPassword);
        layoutBack = (LinearLayout) findViewById(R.id.layout_account_back);

        findViewById(R.id.layout_account_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, SellerDrawer.class));
        finish();
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    private void dialogPassword() {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_reset_password);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(true);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.show();

            final EditText edtOldPass = (EditText) dialog.findViewById(R.id.reset_edt_old_password);
            final EditText edtNewPass = (EditText) dialog.findViewById(R.id.reset_edt_new_password);
            final EditText edtConfPass = (EditText) dialog.findViewById(R.id.reset_edt_confirm_password);
            Button btnSave = (Button) dialog.findViewById(R.id.reset_btn_update);


            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String strOld = edtOldPass.getText().toString();
                    String strNew = edtNewPass.getText().toString();
                    String strCnf = edtConfPass.getText().toString();

                    if (strOld.equals("") || strNew.equals("") || strCnf.equals("")) {
                        utils.Toast(getResources().getString(R.string.please_enter_label), null);
                    } else if (!strNew.equals(strCnf)) {
                        utils.Toast(getResources().getString(R.string.please_enter_conf_pass_label), null);
                    } else {
                        callPassword(strOld, strNew);
                    }
                }
            });
        } catch (Exception e) {
        }
    }

}
