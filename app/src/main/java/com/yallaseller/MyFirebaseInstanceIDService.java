package com.yallaseller;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.SyncStateContract;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;


/**
 * Created by vivek on 1/16/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    public static String token;
    Utils uti;
    Context con;
    Handler mHandler;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
       /* mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                uti = new Utils(getApplicationContext());
                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                Log.e("Token", "Refreshed token: " + refreshedToken);

                sendRegistrationToServer(refreshedToken);

            }
        };
*/

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("Token", "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        Utility.setSharedPreference(getApplicationContext(), Constant.FCMID,token);
        // TODO: Implement this method to send token to your app server.
    }
}
