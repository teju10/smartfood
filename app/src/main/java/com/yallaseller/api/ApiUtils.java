package com.yallaseller.api;

/**
 * Created by vivek on 3/6/2017.
 */

public class ApiUtils {

    public static final String BASE_URL = "https://infograins.com/INFO01/yalla/Sellers/";

//    public static final String BASE_URL1 = "https://infograins.com/INFO01/yalla/Sellers/";

    private ApiUtils() {
    }

    public static RequestAPI getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(RequestAPI.class);
    }

  /*  public static RequestAPI getAPIService1() {
        return RetrofitClient.getClient(BASE_URL1).create(RequestAPI.class);
    }*/
}
