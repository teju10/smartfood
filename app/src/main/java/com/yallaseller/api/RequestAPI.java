package com.yallaseller.api;


import com.yallaseller.model.RegistrationDO;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import static com.yallaseller.api.KeyAbstract.CANCEL_ORDER;
import static com.yallaseller.api.KeyAbstract.CONFIRM_ORDER_LINK;
import static com.yallaseller.api.KeyAbstract.DELETE_NOTIFICATION_LIST;
import static com.yallaseller.api.KeyAbstract.FORGOT_LINK;
import static com.yallaseller.api.KeyAbstract.GET_COUNTRY_LINK;
import static com.yallaseller.api.KeyAbstract.KEY_COMMENT;
import static com.yallaseller.api.KeyAbstract.KEY_COUNTRY;
import static com.yallaseller.api.KeyAbstract.KEY_DEGREE;
import static com.yallaseller.api.KeyAbstract.KEY_DEVICE_ID;
import static com.yallaseller.api.KeyAbstract.KEY_DEVICE_TYPE;
import static com.yallaseller.api.KeyAbstract.KEY_EMAIL;
import static com.yallaseller.api.KeyAbstract.KEY_LATIYUDE;
import static com.yallaseller.api.KeyAbstract.KEY_LONGITUDE;
import static com.yallaseller.api.KeyAbstract.KEY_NAME;
import static com.yallaseller.api.KeyAbstract.KEY_NEW_PWRD;
import static com.yallaseller.api.KeyAbstract.KEY_OLD_PWRD;
import static com.yallaseller.api.KeyAbstract.KEY_ORDER;
import static com.yallaseller.api.KeyAbstract.KEY_ORDER_ID;
import static com.yallaseller.api.KeyAbstract.KEY_PHONE;
import static com.yallaseller.api.KeyAbstract.KEY_PWRD;
import static com.yallaseller.api.KeyAbstract.KEY_RATE;
import static com.yallaseller.api.KeyAbstract.KEY_TIME_ZONE;
import static com.yallaseller.api.KeyAbstract.KEY_UID;
import static com.yallaseller.api.KeyAbstract.KEY_USERID;
import static com.yallaseller.api.KeyAbstract.LOGIN_LINK;
import static com.yallaseller.api.KeyAbstract.MY_ACCOUNT;
import static com.yallaseller.api.KeyAbstract.MY_ORDER;
import static com.yallaseller.api.KeyAbstract.NOTIFICATION_LIST;
import static com.yallaseller.api.KeyAbstract.RATING;
import static com.yallaseller.api.KeyAbstract.RESET_PASSWORD;
import static com.yallaseller.api.KeyAbstract.SUPPORT_LINK;
import static com.yallaseller.api.KeyAbstract.UPDATE_LOCATION_LINK;

/**
 * Created by vivek on 3/6/2017.
 */

public interface RequestAPI {

    //LOGIN
    @POST(LOGIN_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestLoginAPI(
            @Field(KEY_PHONE) String phone,
            @Field(KEY_PWRD) String password,
            @Field(KEY_DEVICE_TYPE) String deviceType,
            @Field(KEY_DEVICE_ID) String deviceToken);

    //LOGIN
    @POST(FORGOT_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestForgotAPI(
            @Field(KEY_EMAIL) String phone);

    //Update location
    @POST(UPDATE_LOCATION_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestLocationAPI(
            @Field(KEY_USERID) String strkeyID,
            @Field(KEY_COUNTRY) String strCountry,
            @Field(KEY_LATIYUDE) String strkeyLat,
            @Field(KEY_LONGITUDE) String strkeyLong,
            @Field(KEY_DEGREE) String strkeyDegree,
            @Field(KEY_DEVICE_ID) String strkeyId,
            @Field(KEY_DEVICE_TYPE) String strkeyType,
            @Field(KEY_TIME_ZONE) String strkeyTimeZone);

    //CONFIRM ORDER
    @POST(CONFIRM_ORDER_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestOrderAPI(
            @Field(KEY_USERID) String strkeyID,
            @Field(KEY_ORDER) JSONObject strOrder,
            @Field(KEY_LATIYUDE) String strkeyLat,
            @Field(KEY_LONGITUDE) String strkeyLong,
            @Field(KEY_COUNTRY) String strCountry,
            @Field(KEY_TIME_ZONE) String strTimeZone
    );

    //CONFIRM ORDER
    @POST(GET_COUNTRY_LINK)
//    @FormUrlEncoded
    Call<RegistrationDO> requestGetCountry(
    );

    @POST(SUPPORT_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestSupportLink(
            @Field(KEY_NAME) String strName,
            @Field(KEY_EMAIL) String strEmail,
            @Field(KEY_PHONE) String strPhone,
            @Field(KEY_COUNTRY) String strCountry,
            @Field(KEY_COMMENT) String strComment
    );


    @POST(CANCEL_ORDER)
    @FormUrlEncoded
    Call<RegistrationDO> requestCancelOrder(
            @Field(KEY_ORDER_ID) String strName
    );

    @POST(RATING)
    @FormUrlEncoded
    Call<RegistrationDO> requestRating(
            @Field(KEY_ORDER_ID) String strName,
            @Field(KEY_RATE) String strRate

    );


    @POST(MY_ACCOUNT)
    @FormUrlEncoded
    Call<RegistrationDO> requestAccount(
            @Field(KEY_USERID) String strName
    );

    @POST(MY_ORDER)
    @FormUrlEncoded
    Call<RegistrationDO> requestOrders(
            @Field(KEY_USERID) String strName
    );

    @POST(RESET_PASSWORD)
    @FormUrlEncoded
    Call<RegistrationDO> requestPassword(
            @Field(KEY_USERID) String strName,
            @Field(KEY_OLD_PWRD) String strOldPass,
            @Field(KEY_NEW_PWRD) String strNewPass
    );

    @POST(NOTIFICATION_LIST)
    @FormUrlEncoded
    Call<RegistrationDO> requestNotificationList(
            @Field(KEY_USERID) String orderID
    );

    @POST(DELETE_NOTIFICATION_LIST)
    @FormUrlEncoded
    Call<RegistrationDO> requestDelteNotificationList(
            @Field(KEY_USERID) String orderID
    );

}