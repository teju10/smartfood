package com.yallaseller.api;

/**
 * Created by vivek on 3/7/2017.
 */

public class KeyAbstract {

    /**
     * Base url's
     */
    public static final String LOGIN_LINK = "login.php";
    public static final String FORGOT_LINK = "forgot_password.php";
    public static final String UPDATE_LOCATION_LINK = "seller_location.php";
    public static final String CONFIRM_ORDER_LINK = "confirm_order.php";
    public static final String SUPPORT_LINK = "support.php";
    public static final String GET_COUNTRY_LINK = "get_country.php";
    public static final String CANCEL_ORDER = "cancel_orders.php";
    public static final String MY_ORDER = "my_orders.php";
    public static final String MY_ACCOUNT = "myaccount.php";
    public static final String RESET_PASSWORD = "reset_password.php";
    public static final String RATING = "rating.php";
    public static final String NOTIFICATION_LIST = "notification_list.php";
    public static final String DELETE_NOTIFICATION_LIST = "delete_notification.php";
    public static final String GETMENU = "GetMenuById.php";

    /**
     * json key's
     */
    public static final String KEY_SUCCESS = "success";
    public static final String KEY_SUCCESS_MSG = "message";
    public static final String KEY_POST_DATA = "post_data";
    public static final String KEY_DEVICE_TYPE = "keydevice_type";
        public static final String KEY_DEVICE_ID = "keydevice_id";
    public static final String KEY_NAME = "keyname";

    public static final String KEY_CUSTOMERNAME= "customer_name";
    public static final String KEY_CUSTOMER_MEAL= "product_subitems";
    public static final String KEY_CUSTOMER_CALL= "contact";
    public static final String KEY_CUSTOMER_TOTALPRICE= "subtotal";
    public static final String KEY_CUSTOMER_IMAGE= "customer_image";
    public static final String KEY_CUSTOMER_REQDATE= "date_time";


    public static final String KEY_STORE_NAME = "keystore_name";
    public static final String KEY_BRANCH_NAME = "keybranch_name";
    public static final String KEY_CITY = "keycity";
    public static final String KEY_LANG = "keylanguage";
    public static final String KEY_TIME_ZONE = "keyTimeZone";

    public static final String KEY_EMAIL = "keyemail";
    public static final String KEY_ORDER = "keyOrder";
    public static final String KEY_COUNTRY = "keycountry";
    public static final String KEY_PHONE = "keyphone";
    public static final String KEY_PWRD = "keypassword";

    public static final String KEY_OLD_PWRD = "keyold_password";
    public static final String KEY_NEW_PWRD = "keynew_password";

    public static final String KEY_IMAGE = "keyimage";
    public static final String KEY_OTP = "keyverify_number";
    public static final String KEY_USERID = "keyid";
    public static final String KEY_UID = "user_id";
    public static final String KEY_COMMENT = "keycomment";
    public static final String KEY_CUR_ADD = "keycurrent_address";
    public static final String KEY_ISCOMPLETE = "keyisorder_complete";

    public static final String KEY_CAR_MODEL = "keycar_model";
    public static final String KEY_CAR_COLOR = "keycar_color";
    public static final String KEY_IS_BUSY = "keyisbusy";

    public static final String KEY_LATIYUDE = "keylat";
    public static final String KEY_LONGITUDE = "keylong";
    public static final String KEY_DEGREE = "keydegree";
    public static final String KEY_DURATION = "keyduration";
    public static final String KEY_DRIVERSTATUS= "driver_status";
    public static final String KEY_DISTANCE = "keydistance";
    public static final String KEY_PRICE = "keyPrice";
    public static final String KEY_REQUEST_ID = "keyrequest_id";
    public static final String KEY_DRIVER_ID = "keydriver_id";
    public static final String KEY_ORDER_ID = "keyOrderId";
    public static final String KEY_RATE = "keyRate";
    public static final String ORDERID = "order_id";
    public static final String DELIVERYTIME = "delivery_time";


    public static final String KEY_COUNT_ORDER_COMPLETE = "keyCountComplete";
    public static final String KEY_COUNT_ORDER_ONWAY = "keyCountInComplete";


    public static final String KEY_TOTAL_RATING = "keytotal_rating";
    public static final String KEY_TOTAL_REVIEW = "keytotal_review";
    public static final String KEY_ONE_STAR = "keyone_star";
    public static final String KEY_TWO_STAR = "keytwo_star";
    public static final String KEY_THREE_STAR = "keythree_star";
    public static final String KEY_FOUR_STAR = "keyfour_star";
    public static final String KEY_FIVE_STAR = "keyfive_star";

    public static final String KEY_strdvCusName = "KeyCustName";
    public static final String KEY_strdvCusNumb = "KeyCustNumber";
    public static final String KEY_strdvCusStreetAdd = "KeyCustStreet";
    public static final String KEY_strdvCusDestAdd = "KeyCustDestin";
    public static final String KEY_strdvCusFloor = "KeyCustFloor";
    public static final String KEY_strdvCusBul = "KeyCustBulding";
    public static final String KEY_strApt = "KeyCustAppartment";
    public static final String KEY_strdvNotes = "KeyCustNote";

    public static final String KEY_ORDER1 = "KeyOrder1";
    public static final String KEY_ORDER2 = "KeyOrder2";
    public static final String KEY_ORDER3 = "KeyOrder3";

    public static final String KEY_NOTI_TIME = "keytime";
    public static final String KEY_NOTI = "keynotification";
    public static final String KEY_NOTI_MSG = "keymessage";

}
