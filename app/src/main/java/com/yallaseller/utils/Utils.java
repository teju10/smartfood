/**
 * @author Vivek Shah
 * <p>
 * This class is made as common or utility class where the all common methods and
 * also common variables are stored which can be used in whole development time.
 * <p>
 * You should declare this class  in every activity to use common things like alert and toast even ,
 * just need to pass context to constructor from there.
 */
package com.yallaseller.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.yallaseller.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils extends Application {

    // This variable store the context of Application
    public static Context applicationContext;

    // private context made because no one can use ot store in this context
    private Context context = null;
    private SharedPreferences preferences = null;
    private SharedPreferences.Editor editor = null;
    private ProgressDialog dialog;

    /*
     * Parameterized Constructor made because getting fresh context every time
     * and to make methods easy.
     */
    public Utils(Context con) {
        context = con;
        dialog = new ProgressDialog(con);
        preferences = PreferenceManager.getDefaultSharedPreferences(con);
        editor = preferences.edit();
    }

    public Utils(Context con, String str) {
        context = con;
        preferences = PreferenceManager.getDefaultSharedPreferences(con);
        editor = preferences.edit();
    }


    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String streamToString(InputStream is) throws IOException {
        String str = "";

        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                reader.close();
            } finally {
                is.close();
            }

            str = sb.toString();
        }

        return str;
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                Toast("Error to find google play service", null);
            }
            return false;
        }
        return true;
    }

    /**
     * @param message Pass message to show user
     * @return It will return long toast message whatever you pass in your
     * application
     */
    public void Toast(String message, CoordinatorLayout coordinatorLayout) {
        try {
            if (isAboveMarsh() && coordinatorLayout != null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
                snackbar.show();
            } else {
                final String onTimeMsg = message;
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, onTimeMsg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // public void showProgress() {
    // // dialog.setTitle("Please wait");
    // // dialog.setMessage("Loading...");
    //
    // }

    // public void cancelProgress() {
    //
    // }


    public boolean isAboveMarsh() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return true;
        } else {
            return false;
        }
    }

    public void startProgress() {
        try {
            // for making transparent background
            dialog.show();
//            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setMessage(null);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_progress);

            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissProgress() {
        try {
            // dialog cancel
            dialog.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param buttonName set yes no or cancel
     * @param message    message in alert box
     * @return AlertBox to use as user message
     */
    public void showAlertMessage(String buttonName, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(buttonName,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * @param emailAddress Passyour emiailaddress string to check
     * @return It will return true if email address is valid or false in case
     * email is not valid
     */
    public boolean isEmailValid(String emailAddress) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(emailAddress);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    /*
     * (?=.*\d) must contains one digit from 0-9 (?=.*[a-z]) must contains one
     * lowercase characters (?=.*[A-Z]) must contains one uppercase characters
     * (?=.*[@#$%]) must contains one special symbols in the list "@#$%" . match
     * anything with previous condition checking {6,20} length at least 6
     * characters and maximum of 12
     */
    public boolean isPasswordValid(String password) {
        // String passPattern =
        // "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,12})";
//		String passPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,100})";

        String passPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})";
        Pattern pattern = Pattern.compile(passPattern);
        Matcher matcher = pattern.matcher(password);
        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static void ChangeLang(Context appContext, int i) {
        String lang = "";
        switch (i) {
            case 0:
                lang = "en";
                break;
            case 1:
                lang = "ar";
                break;
        }
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = locale;
        appContext.getResources().updateConfiguration(config, appContext.getResources().getDisplayMetrics());
    }

    /**
     * @return It will check your Internet connection.True if any net connected.
     */
    public boolean isNetConnected() {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    /**
     * @return This method returns system current time , change format as per
     * your reuirement , Locale is also set as english so take care of
     * that also.It is HH:mm 24 hour format
     */
    public String getCurrentTime() {
        return new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(new Date());
    }

    /**
     * @return This method returns system current date in dd/MM/yyyy format
     */
    public String getCurrentDate() {
        return new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
                .format(new Date());
    }
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    /**
     * set string Preference
     */
    public void setPreference(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * get string preferences
     */
    public String getPreference(String key) {
        return preferences.getString(key, "");
    }

    /*public static void setIntegerSharedPreference(Context context, String name, int value) {
        context = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        editor.putInt(name, value);
        editor.commit();
    }*/

    public void setIntPreference(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * get string preferences
     */
    public int getIntPreference(String key) {
        return preferences.getInt(key, 0);
    }

    /**
     * Use to set boolean preference
     */
    public void setBoolPrefrences(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String ChangeDateFormat(String inputFormat,String outputFormat,String inputDate){
        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {

        }
        return outputDate;
    }

    /**
     * for timezone
     */
        public String timeZoneId() {
            return TimeZone.getDefault().getID();
        }

    /**
     * use to get boolean preference
     */
    public boolean getBoolPref(String key) {
        return preferences.getBoolean(key, false);
    }

    /**
     * @return This method kills all processes of application which is running
     * in back ground , we can also use
     * android.os.Process.killProcess(pid) to exit from application
     */
    public void killProcess(Context context) {
        int pid = 0;
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> pids = am
                .getRunningAppProcesses();
        for (int i = 0; i < pids.size(); i++) {
            ActivityManager.RunningAppProcessInfo info = pids.get(i);
            if (info.processName.equalsIgnoreCase(context.getPackageName())) {
                pid = info.pid;
            }
        }
        android.os.Process.killProcess(pid);
    }

    /**
     * @return true is tablet resolution is there
     */
    public boolean isTablet() {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    /**
     * @return IMEI NO
     */
    public String getImeiNo() {
        TelephonyManager teleManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return teleManager.getDeviceId();
    }

    public boolean isUsingWiFi() {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivity
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo.getState() == NetworkInfo.State.CONNECTED
                || wifiInfo.getState() == NetworkInfo.State.CONNECTING) {
            return true;
        }
        return false;
    }

    public Typeface normal() {
        return Typeface.createFromAsset(context.getAssets(), "text_semi_bold.ttf");
    }

    public Typeface bold() {
        return Typeface.createFromAsset(context.getAssets(), "text_bold.ttf");
    }


}
