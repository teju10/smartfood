package com.yallaseller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.yallaseller.activity.LoginActivity;
import com.yallaseller.utility.Constant;
import com.yallaseller.utility.Utility;
import com.yallaseller.utils.Utils;

/**
 * Created by vivek on 6/2/2017.
 */

public class SplashActivity extends AppCompatActivity {
    Context mCpontext;
    private Utils utils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mCpontext = this;

        try {
            utils = new Utils(this);
            /*utils.setPreference(Constant.isLangSet, "en");
            Utility.setIntegerSharedPreference(mCpontext, Constant.LANG, 0);*/

            methodForchanegeGet();

            /*if (utils.getPreference(Constant.isLangSet).equals("ar")) {
                String languageToLoad = "ar";
                Resources res = getResources();
                Configuration conf = res.getConfiguration();
                DisplayMetrics dm = res.getDisplayMetrics();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    conf.setLocale(new Locale(languageToLoad));
                    createConfigurationContext(conf);
                    res.updateConfiguration(conf, dm);
                } else {
                    conf.locale = new Locale(languageToLoad);
                    getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
                }
            } else {
                String languageToLoad = "en";
                Resources res = getResources();
                Configuration conf = res.getConfiguration();
                DisplayMetrics dm = res.getDisplayMetrics();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    conf.setLocale(new Locale(languageToLoad));
                    createConfigurationContext(conf);
                    res.updateConfiguration(conf, dm);
                } else {
                    conf.locale = new Locale(languageToLoad);
                    getResources().updateConfiguration(conf, getResources().getDisplayMetrics());
                }
            }*/
        } catch (Exception e) {

        }


        new CountDownTimer(1000, 3000) {
            @Override
            public void onTick(long l) {
                new MyFirebaseInstanceIDService().onTokenRefresh();
            }

            @Override
            public void onFinish() {
                if (utils.getPreference(Constant.PREF_ID).equals("")) {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, SellerDrawer.class);
                    startActivity(intent);
                    finish();
                }
            }
        }.start();

    }

    private void methodForchanegeGet() {
        if (Utility.getSharedPreferences(mCpontext,Constant.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(mCpontext, Constant.FCMID,token);
        }
    }
}
